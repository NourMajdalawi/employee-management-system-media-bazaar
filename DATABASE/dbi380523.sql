-- phpMyAdmin SQL Dump
-- version 4.9.3
-- https://www.phpmyadmin.net/
--
-- Host: studmysql01.fhict.local
-- Generation Time: Jun 25, 2020 at 10:59 AM
-- Server version: 5.7.26-log
-- PHP Version: 7.3.16

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `dbi380523`
--

-- --------------------------------------------------------

--
-- Table structure for table `availability`
--

CREATE TABLE `availability` (
  `id` int(11) NOT NULL,
  `employeeid` int(11) NOT NULL,
  `mondaymorning` tinyint(4) NOT NULL,
  `mondayafternoon` tinyint(4) NOT NULL,
  `mondayevening` tinyint(4) NOT NULL,
  `tuesdaymorning` tinyint(4) NOT NULL,
  `tuesdayafternoon` tinyint(4) NOT NULL,
  `tuesdayevening` tinyint(4) NOT NULL,
  `wednesdaymorning` tinyint(4) NOT NULL,
  `wednesdayafternoon` tinyint(4) NOT NULL,
  `wednesdayevening` tinyint(4) NOT NULL,
  `thursdaymorning` tinyint(4) NOT NULL,
  `thursdayafternoon` tinyint(4) NOT NULL,
  `thursdayevening` tinyint(4) NOT NULL,
  `fridaymorning` tinyint(4) NOT NULL,
  `fridayafternoon` tinyint(4) NOT NULL,
  `fridayevening` tinyint(4) NOT NULL,
  `saturdaymorning` tinyint(4) NOT NULL,
  `saturdayafternoon` tinyint(4) NOT NULL,
  `saturdayevening` tinyint(4) NOT NULL,
  `sundaymorning` tinyint(4) NOT NULL,
  `sundayafternoon` tinyint(4) NOT NULL,
  `sundayevening` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `availability`
--

INSERT INTO `availability` (`id`, `employeeid`, `mondaymorning`, `mondayafternoon`, `mondayevening`, `tuesdaymorning`, `tuesdayafternoon`, `tuesdayevening`, `wednesdaymorning`, `wednesdayafternoon`, `wednesdayevening`, `thursdaymorning`, `thursdayafternoon`, `thursdayevening`, `fridaymorning`, `fridayafternoon`, `fridayevening`, `saturdaymorning`, `saturdayafternoon`, `saturdayevening`, `sundaymorning`, `sundayafternoon`, `sundayevening`) VALUES
(4, 61, 0, 0, 0, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0),
(5, 64, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 0, 0, 0, 0, 0),
(10, 62, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(24, 65, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0),
(25, 66, 0, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1),
(26, 67, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 1, 1, 1, 0),
(27, 68, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1),
(28, 69, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0),
(29, 70, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0),
(31, 71, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0),
(32, 72, 0, 0, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1),
(33, 73, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0),
(34, 74, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1),
(35, 75, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(36, 76, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0),
(37, 77, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(38, 78, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1),
(39, 79, 1, 1, 1, 0, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(44, 80, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0),
(46, 81, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1),
(47, 82, 0, 0, 1, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 0, 1),
(48, 83, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 0, 1, 1),
(49, 84, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1),
(50, 85, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0),
(51, 86, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1),
(52, 87, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1),
(53, 88, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0),
(54, 89, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0),
(55, 90, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 0),
(56, 91, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0),
(57, 92, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0),
(58, 93, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1),
(59, 94, 1, 1, 1, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(60, 95, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0),
(61, 96, 0, 0, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1),
(62, 97, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1),
(63, 98, 0, 0, 0, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1),
(64, 99, 1, 1, 1, 1, 0, 0, 0, 1, 1, 0, 0, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0),
(65, 100, 0, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 1, 1),
(66, 101, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1),
(67, 102, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1),
(68, 103, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0),
(69, 104, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 0),
(70, 105, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 0, 0),
(71, 106, 1, 1, 1, 1, 0, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1),
(72, 107, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1),
(73, 108, 1, 1, 1, 1, 0, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 0, 1, 1),
(74, 109, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 0, 1),
(75, 110, 1, 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 1, 0, 0, 1, 1, 0, 1, 1, 0, 1),
(76, 111, 1, 1, 0, 1, 1, 1, 0, 1, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1, 1, 0),
(77, 112, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1),
(78, 113, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(79, 114, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 1, 1),
(80, 115, 1, 1, 1, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 0),
(81, 116, 1, 0, 0, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 0, 0, 1, 1, 1, 1, 1, 1),
(82, 117, 1, 1, 1, 1, 1, 1, 0, 0, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1),
(83, 118, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0),
(84, 119, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(85, 120, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 1, 1, 1, 1, 1),
(87, 122, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(88, 123, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(89, 124, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(90, 125, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 1, 1, 1),
(91, 126, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(92, 127, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(94, 128, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(96, 129, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `company`
--

CREATE TABLE `company` (
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `company`
--

INSERT INTO `company` (`name`) VALUES
('MediaBazaar');

-- --------------------------------------------------------

--
-- Table structure for table `contract`
--

CREATE TABLE `contract` (
  `id` int(11) NOT NULL,
  `employeeid` int(11) NOT NULL,
  `position` varchar(50) NOT NULL,
  `hourlysalary` double NOT NULL,
  `startdate` date NOT NULL,
  `enddate` date DEFAULT NULL,
  `FTE` double NOT NULL,
  `active` tinyint(1) NOT NULL,
  `departurereason` text,
  `departuretype` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `contract`
--

INSERT INTO `contract` (`id`, `employeeid`, `position`, `hourlysalary`, `startdate`, `enddate`, `FTE`, `active`, `departurereason`, `departuretype`) VALUES
(148, 62, '1', 10, '2020-05-07', '0001-01-01', 0.9, 1, NULL, NULL),
(154, 64, '3', 25, '2020-05-07', '0001-01-01', 1, 1, NULL, NULL),
(160, 61, '2', 15, '2020-05-14', '2020-05-14', 0.7, 1, NULL, NULL),
(166, 65, '3', 6, '2020-06-18', '0001-01-01', 0.6, 1, NULL, NULL),
(167, 66, '3', 8, '2020-06-18', '0001-01-01', 0.7, 1, NULL, NULL),
(168, 67, '3', 8, '2020-06-18', '0001-01-01', 0.6, 1, NULL, NULL),
(169, 68, '3', 14, '2020-06-18', '0001-01-01', 0.5, 1, NULL, NULL),
(170, 69, '3', 10, '2020-06-18', '0001-01-01', 1, 1, NULL, NULL),
(171, 70, '3', 29, '2020-06-18', '0001-01-01', 0.9, 1, NULL, NULL),
(172, 71, '2', 19, '2020-06-18', '0001-01-01', 0.7, 1, NULL, NULL),
(173, 72, '2', 11, '2020-06-18', '0001-01-01', 0.9, 1, NULL, NULL),
(174, 73, '2', 25, '2020-06-18', '0001-01-01', 1, 1, NULL, NULL),
(175, 74, '2', 27, '2020-06-18', '0001-01-01', 0.6, 1, NULL, NULL),
(176, 75, '2', 35, '2020-06-18', '0001-01-01', 1, 1, NULL, NULL),
(177, 76, '1', 18, '2020-06-17', '0001-01-01', 0.6, 1, NULL, NULL),
(178, 77, '1', 16, '2020-06-18', '0001-01-01', 1, 1, NULL, NULL),
(179, 78, '1', 10, '2020-06-18', '0001-01-01', 0.8, 1, NULL, NULL),
(180, 79, '1', 18, '2020-06-18', '0001-01-01', 0.8, 1, NULL, NULL),
(185, 80, '2', 11, '2020-06-18', '0001-01-01', 1, 1, NULL, NULL),
(186, 81, '3', 7, '2020-06-18', '0001-01-01', 0.6, 1, NULL, NULL),
(187, 82, '3', 20, '2020-06-18', '0001-01-01', 0.9, 1, NULL, NULL),
(188, 83, '1', 14, '2020-06-18', '0001-01-01', 1, 1, NULL, NULL),
(189, 84, '1', 12, '2020-06-19', '2020-06-22', 0.8, 0, 'Contract changed', '2'),
(190, 85, '0', 18, '2020-06-19', '0001-01-01', 0.6, 1, NULL, NULL),
(191, 86, '1', 8, '2020-06-19', '0001-01-01', 0.7, 1, NULL, NULL),
(192, 87, '1', 37, '2020-06-19', '0001-01-01', 1, 1, NULL, NULL),
(193, 88, '1', 6, '2020-06-19', '0001-01-01', 0.6, 1, NULL, NULL),
(194, 89, '1', 20, '2020-06-19', '0001-01-01', 0.8, 1, NULL, NULL),
(195, 90, '1', 12, '2020-06-19', '0001-01-01', 0.4, 1, NULL, NULL),
(196, 91, '1', 17, '2020-06-19', '0001-01-01', 1, 1, NULL, NULL),
(197, 92, '4', 14, '2020-06-19', '0001-01-01', 1, 1, NULL, NULL),
(198, 93, '4', 12, '2020-06-19', '0001-01-01', 0.9, 1, NULL, NULL),
(199, 94, '4', 25, '2020-06-19', '0001-01-01', 0.7, 1, NULL, NULL),
(200, 95, '4', 16, '2020-06-19', '2020-06-24', 0.8, 0, 'Contract changed', '2'),
(201, 96, '4', 24, '2020-06-19', '0001-01-01', 0.8, 1, NULL, NULL),
(202, 97, '4', 12, '2020-06-19', '0001-01-01', 0.7, 1, NULL, NULL),
(203, 98, '4', 20, '2020-06-19', '0001-01-01', 0.6, 1, NULL, NULL),
(204, 99, '4', 10, '2020-06-19', '0001-01-01', 0.9, 1, NULL, NULL),
(205, 100, '4', 16, '2020-06-19', '0001-01-01', 0.9, 1, NULL, NULL),
(206, 101, '4', 11, '2020-06-19', '0001-01-01', 0.6, 1, NULL, NULL),
(207, 102, '4', 25, '2020-06-19', '0001-01-01', 0.9, 1, NULL, NULL),
(208, 103, '4', 24, '2020-06-19', '0001-01-01', 0.8, 1, NULL, NULL),
(209, 104, '0', 11, '2020-06-19', '0001-01-01', 1, 1, NULL, NULL),
(210, 105, '0', 10, '2020-06-19', '0001-01-01', 0.8, 1, NULL, NULL),
(211, 106, '0', 18, '2020-06-19', '0001-01-01', 0.9, 1, NULL, NULL),
(212, 107, '2', 11, '2020-06-19', '0001-01-01', 0.3, 1, NULL, NULL),
(213, 108, '0', 9, '2020-06-19', '0001-01-01', 0.7, 1, NULL, NULL),
(214, 109, '0', 10, '2020-06-19', '0001-01-01', 1, 1, NULL, NULL),
(215, 110, '0', 11, '2020-06-19', '0001-01-01', 0.5, 1, NULL, NULL),
(216, 111, '0', 14, '2020-06-20', '0001-01-01', 1, 1, NULL, NULL),
(217, 112, '0', 12, '2020-06-20', '0001-01-01', 0.9, 1, NULL, NULL),
(218, 113, '0', 10, '2020-06-20', '0001-01-01', 1, 1, NULL, NULL),
(219, 114, '0', 100, '2020-06-20', '0001-01-01', 0.8, 1, NULL, NULL),
(220, 115, '0', 100, '2020-06-20', '0001-01-01', 1, 1, NULL, NULL),
(221, 116, '0', 12, '2020-06-20', '0001-01-01', 1, 1, NULL, NULL),
(222, 117, '0', 10, '2020-06-20', '0001-01-01', 0, 1, NULL, NULL),
(223, 118, '0', 13, '2020-06-20', '0001-01-01', 1, 1, NULL, NULL),
(224, 119, '0', 10, '2020-06-20', '0001-01-01', 1, 1, NULL, NULL),
(225, 120, '0', 11, '2020-06-20', '0001-01-01', 1, 1, NULL, NULL),
(226, 122, '0', 10, '2020-06-20', '0001-01-01', 1, 1, NULL, NULL),
(227, 123, '0', 10, '2020-06-20', '0001-01-01', 1, 1, NULL, NULL),
(228, 124, '0', 11, '2020-06-20', '0001-01-01', 1, 1, NULL, NULL),
(229, 125, '0', 11, '2020-06-20', '0001-01-01', 1, 1, NULL, NULL),
(230, 84, '1', 15, '2020-06-19', '2020-06-26', 8, 0, 'She quit ', '0'),
(231, 84, '1', 13, '2020-06-22', '0001-01-01', 0.7, 1, NULL, NULL),
(232, 126, '1', 8, '2020-06-24', '2020-06-24', 0.3, 0, 'Contract renewed', '2'),
(233, 126, '1', 8, '2020-06-24', '2020-06-28', 0.3, 0, 'She quit due to school', '0'),
(234, 95, '4', 17, '2020-06-19', '0001-01-01', 0.6, 1, NULL, NULL),
(235, 127, '0', 11, '2020-06-25', '2020-06-25', 0.6, 0, 'Contract changed', '2'),
(236, 127, '0', 15, '2020-06-25', '2020-06-25', 0.6, 0, 'She quit', '0'),
(237, 128, '0', 7, '2020-06-25', '2020-06-25', 0.5, 0, 'Contract changed', '2'),
(238, 128, '0', 10, '2020-06-25', '2020-06-25', 0.5, 0, 'He stole from the company', '1'),
(239, 129, '0', 10, '2020-06-25', '2020-06-25', 0.3, 0, 'Contract changed', '2'),
(240, 129, '0', 15, '2020-06-25', '2020-07-02', 0.3, 0, 'She stole from the shop', '1');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` int(11) NOT NULL,
  `department` text NOT NULL,
  `requiredfte` double NOT NULL,
  `position` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `department`, `requiredfte`, `position`) VALUES
(1, 'Manager', 48, 2),
(2, 'Administration', 61, 3),
(3, 'Depot worker', 84, 1),
(4, 'Security', 84, 4),
(5, 'Mobile devices', 69, 0),
(6, 'Car accessories', 40, 0),
(7, 'Home automation', 35, 0),
(8, 'Entertainments', 57, 0);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE `employee` (
  `id` int(11) NOT NULL,
  `store` varchar(50) NOT NULL,
  `username` varchar(50) NOT NULL,
  `password` text NOT NULL,
  `currentposition` varchar(50) DEFAULT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `gender` varchar(50) NOT NULL,
  `bsn` varchar(15) NOT NULL,
  `birthdate` date NOT NULL,
  `emailaddress` varchar(100) NOT NULL,
  `nationality` varchar(50) NOT NULL,
  `spokenlanguages` text NOT NULL,
  `region` varchar(50) NOT NULL,
  `zipcode` varchar(10) NOT NULL,
  `streetname` varchar(50) NOT NULL,
  `phonenumber` varchar(15) NOT NULL,
  `emergencyphonenumber` varchar(15) NOT NULL,
  `spouse` tinyint(4) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`id`, `store`, `username`, `password`, `currentposition`, `firstname`, `lastname`, `gender`, `bsn`, `birthdate`, `emailaddress`, `nationality`, `spokenlanguages`, `region`, `zipcode`, `streetname`, `phonenumber`, `emergencyphonenumber`, `spouse`) VALUES
(61, 'MediaBazaarEindhoven', 'Manager', '12345', '2', 'Jaimy', 'Stevens', 'Female', '234987546', '1996-09-04', 'jaimystevens1@hotmail.com', 'Dutch', 'Dutch, English', 'Noord-Zuid', '2970NB', 'Rob van spaendonckstraat', '0687300551', '0646251511', 0),
(62, 'MediaBazaarEindhoven', 'DepotWorker', '12345', '1', 'Henk', 'Janssen', 'Male', '126574635', '2000-03-18', 'jaimystevens3@hotmail.com', 'Dutch', 'Dutch', 'Noord-Zoud', '5654GF', 'Broekmanstraat', '0686473847', '0612365940', 0),
(64, 'MediaBazaarEindhoven', 'admin', '12345', '3', 'Emma', 'van de broek', 'Female', '544073344', '1988-06-26', 'jaimystevens3@hotmail.com', 'Dutch', 'Dutch', 'Noord-Zuid', '5654GF', 'Motelstraat', '0687358735', '0693169436', 0),
(65, 'MediaBazaarEindhoven', 'jorick.van nuland.mediabazaar', '9-MtgYV#', '3', 'Jorick', 'Van Nuland', '0', '265534782', '1999-07-15', 'jorick@gmail.com', 'Dutch', 'Dutch,English,German', 'Eindhoven', '5643NC', 'Gestelsestraat ', '0634672345', '0675847362', 0),
(66, 'MediaBazaarEindhoven', 'simone.geurtz', '123456', '3', 'Simone', 'Geurtz', '1', '453622836', '2000-09-03', 'simonegeurtz@hotmail.com', 'Dutch', 'Dutch,English,French', 'Huissen', '6453TH', 'Generaalstraat', '0647345285', '0630376322', 0),
(67, 'MediaBazaarEindhoven', 'nour.majdalawi.mediabazaar', 'O3-BPJp2', '3', 'Nour', 'Majdalawi', '1', '678293959', '2000-06-23', 'Nour@gmail.com', 'Dutch', 'Dutch,English,Arabic', 'Tilburg', '6574VK', 'Dorpstraat', '0675829235', '0675436722', 0),
(68, 'MediaBazaarEindhoven', 'sjuul.de lang.mediabazaar', 'pCa~%h3Z', '3', 'Sjuul', 'de Lang', '0', '236774566', '2000-07-05', 'sjuul@gmail.com', 'Dutch', 'Dutch,English', 'Eindhoven', '5653HV', 'Hoogstraat', '0682345198', '0613465296', 0),
(69, 'MediaBazaarEindhoven', 'henk.de jong.mediabazaar', ')ifSqoWi', '3', 'Henk', 'de jong', '0', '564728993', '2000-09-15', 'henk@gmail.com', 'Dutch', 'Dutch,English', 'Eindhoven', '6547TD', 'Reinkenstraat', '0634892654', '0689892345', 0),
(70, 'MediaBazaarEindhoven', 'gerard.janssen.mediabazaar', 'OM5RVtbH', '3', 'Gerard', 'Janssen', '0', '675899938', '1999-09-22', 'gerard@gmail.com', 'Dutch', 'Dutch,English', 'Den Bosch', '7623GT', 'Hoofdstraat', '0635661525', '0678474652', 0),
(71, 'MediaBazaarEindhoven', 'hannah.dam.mediabazaar', '2NeD~y$I', '2', 'Hannah', 'Dam', '1', '546376278', '1988-06-10', 'hannah@gmail.com', 'Dutch', 'Dutch', 'Amsterdam', '7653GU', 'Tuinlaan', '0678237656', '0698546372', 0),
(72, 'MediaBazaarEindhoven', 'debora.smits.mediabazaar', 'VU*8ICF9', '2', 'Debora', 'Smits', '1', '534682781', '1988-06-10', 'debora@gmail.com', 'Dutch', 'Dutch,English', 'Rotterdam', '1145TH', 'Moutweg', '0674566373', '0623899654', 1),
(73, 'MediaBazaarEindhoven', 'maarten.mulder.mediabazaar', 'a*C#0b3q', '2', 'Maarten', 'Mulder', '0', '238764456', '1987-11-26', 'maartenmulder@gmail.com', 'Dutch', 'Dutch,English', 'Eindhoven', '5643NC', 'Laagstraat', '0633487625', '0678887345', 1),
(74, 'MediaBazaarEindhoven', 'rick.pieters.mediabazaar', 'zD^)g-AJ', '2', 'Rick', 'Pieters', '0', '785849395', '1988-10-22', 'rickpieters@gmail.com', 'Dutch', 'Dutch,English', 'Eindhoven', '6765TH', 'Stratumsdijk', '0695467832', '0623456355', 0),
(75, 'MediaBazaarEindhoven', 'cynthia.amerongen.mediabazaar', 'giWnk1o^', '2', 'Cynthia', 'Amerongen', '1', '673654256', '1986-12-19', 'cynthiaamerongen@gmail.com', 'Dutch', 'Dutch,English,German', 'Helmond', '7654JK', 'Hoofdweg', '0646543892', '0698734674', 1),
(76, 'MediaBazaarEindhoven', 'jari.de bruin.mediabazaar', 'ky-eTj3o', '1', 'Jari', 'de Bruin', '0', '785847263', '1986-12-02', 'jaridebruin@gmail.com', 'Dutch', 'Dutch,English', 'Utrecht', '2345GN', 'Schaapskamp', '0630494758', '0624897896', 0),
(77, 'MediaBazaarEindhoven', 'ronald.hendriks.mediabazaar', 'B5%YwSd9', '1', 'Ronald', 'Hendriks', '0', '563728197', '1984-06-13', 'ronaldhendriks@gmail.com', 'Dutch', 'Dutch,English', 'Utrecht', '6553TD', 'Veldstraat', '0654635153', '0676764534', 0),
(78, 'MediaBazaarEindhoven', 'robin.koenen.mediabazaar', 'dnpjvm47', '1', 'Robin', 'Koenen', '0', '145667345', '1984-06-13', 'robinkoenen@gmail.com', 'Dutch', 'Dutch,English', 'Veldhoven', '5654NC', 'Jachtlaan', '0678475623', '0611245633', 0),
(79, 'MediaBazaarEindhoven', 'marjolein.de haas.mediabazaar', 'Z3M2UhqV', '1', 'Marjolein', 'De Haas', '1', '238846763', '1971-03-01', 'marjoleindehaas@gmail.com', 'Dutch', 'Dutch,English', 'Hengelo', '1298AD', 'Peregaard', '0611983678', '0611348984', 0),
(80, 'MediaBazaarEindhoven', 'marit.koning.mediabazaar', 'dqKABo3v', '2', 'Marit', 'Koning', '1', '389287656', '1983-11-29', 'maritkoning@gmail.com', 'Dutch', 'Dutch,English', 'Tilburg', '5023RK', 'Poststraat', '0643546294', '0698534234', 0),
(81, 'MediaBazaarEindhoven', 'joanne.zwart.mediabazaar', 'jMz4^1Fo', '3', 'Joanne', 'Zwart', '1', '673728288', '1983-11-29', 'joannezwart@gmail.com', 'Dutch', 'Dutch,English,German', 'Weesp', '4378TF', 'Kerkstraat', '0628394948', '0638498488', 0),
(82, 'MediaBazaarEindhoven', 'luca.winters.mediabazaar', '#x$5-pSs', '3', 'Luca', 'Winters', '1', '453538223', '1983-11-29', 'lucawinters@gmail.com', 'Dutch', 'Dutch,English,German', 'Waalwijk', '8873WS', 'Fruitlaan', '0644357389', '0611298839', 0),
(83, 'MediaBazaarEindhoven', 'lukas.rikken.mediabazaar', 'FR@%IkK0', '1', 'Lukas', 'Rikken', '0', '226367782', '1983-11-01', 'lukasrikken@gmail.com', 'Belgian', 'Dutch,English,German', 'Helmond', '6635RT', 'Essenpas', '0673888411', '0622378499', 0),
(84, 'MediaBazaarEindhoven', 'eva.van egmond.mediabazaar', '~BZ5Oyji', '1', 'Eva', 'van Egmond', '1', '284839220', '1969-06-19', 'evavanegmond@gmail.com', 'Dutch', 'Dutch,English', 'Bemmel', '6681GH', 'Ceres', '0648394887', '0612389283', 1),
(85, 'MediaBazaarEindhoven', 'hilde.van essen.mediabazaar', '-YcY3~J7', '0', 'Hilde', 'Van Essen', '1', '747389993', '1985-05-21', 'hildevanessen@gmail.com', 'Dutch', 'Dutch,English,French', 'Rotterdam', '1274SK', 'Lindelaan', '0628753219', '0625114328', 0),
(86, 'MediaBazaarEindhoven', 'lisette.wiersema.mediabazaar', 'c6Em6p2~', '1', 'Lisette', 'Wiersema', '1', '437282819', '1999-06-23', 'lisettewiersema@gmail.com', 'Dutch', 'Dutch', 'Soest', '3674GN', 'Wieksloot', '0632887334', '0612748593', 0),
(87, 'MediaBazaarEindhoven', 'anthonie.meinema.mediabazaar', '-izK~VAE', '1', 'Anthonie', 'Meinema', '0', '588283777', '2020-08-22', 'anthoniemeinema@gmail.com', 'Dutch', 'Dutch,English', 'Elst', '6672RG', 'Spui', '06298836473', '0634465524', 0),
(88, 'MediaBazaarEindhoven', 'sven.ledder.mediabazaar', 'R#kuTz*u', '1', 'Sven', 'Ledder', '0', '576272993', '1975-01-15', 'svenledder@gmail.com', 'Dutch', 'Dutch,English,German', 'Gendt', '7652ZP', 'het Zandvoort', '06528374654', '0698143647', 0),
(89, 'MediaBazaarEindhoven', 'tigo.heijsen.mediabazaar', 'j@ne8SjB', '1', 'Tigo', 'Heijsen', '0', '238847573', '1976-05-05', 'tigoheijsen@gmail.com', 'Dutch', 'Dutch,English', 'Oosterhout', '7887PL', 'jonagoldstraat', '0628793840', '0609172600', 0),
(90, 'MediaBazaarEindhoven', 'sanne.fransen.mediabazaar', '8x^q29V)', '1', 'Sanne', 'Fransen', '1', '832203981', '1977-02-25', 'sannefransen@gmail.com', 'Dutch', 'Dutch,English', 'Vierlingsbeek', '8645HW', 'Oeverkamp', '0628333477', '0600192893', 0),
(91, 'MediaBazaarEindhoven', 'kaylee.balk.mediabazaar', 'kPuEJ4xm', '1', 'Kaylee', 'Balk', '1', '129394087', '1977-12-20', 'kayleebalk@gmail.com', 'Dutch', 'Dutch,English,German', 'Bemmel', '6681TH', 'Floralaan', '0633748228', '0674389485', 0),
(92, 'MediaBazaarEindhoven', 'koen.jacobs.mediabazaar', 'grfZcJDN', '4', 'Koen', 'Jacobs', '0', '637282938', '1994-05-09', 'koenjacobs@gmail.com', 'Dutch', 'Dutch,English', 'Veenendaal', '2356TP', 'Nieuwestraatweg', '0625389346', '0612389546', 0),
(93, 'MediaBazaarEindhoven', 'timo.hoornweg.mediabazaar', 'Lc$JLkq*', '4', 'Timo', 'Hoornweg', '0', '253647386', '1995-04-23', 'timohoornweg@gmail.com', 'Dutch', 'Dutch,English,French', 'Veenendaal', '2384HW', 'Jansplein', '0637422834', '0618746335', 0),
(94, 'MediaBazaarEindhoven', 'robert.lubbers.mediabazaar', 'Il+y#T4s', '4', 'Robert', 'Lubbers', '0', '162748346', '1995-08-24', 'robertlubbers@gmail.com', 'Dutch', 'Dutch,English,German', 'Roosendaal', '6726GM', 'Limburglaan', '0634523678', '0610982367', 0),
(95, 'MediaBazaarEindhoven', 'moira.harteveld.mediabazaar', 'm~@~EBMO', '4', 'Moira', 'Harteveld', '1', '748293945', '1971-03-19', 'moiraharteveld@gmail.com', 'Dutch', 'Dutch,English', 'Huissen', '9284ML', 'Generaalstraat', '0637434985', '0698346254', 1),
(96, 'MediaBazaarEindhoven', 'maureen.brom.mediabazaar', 'vF+Ly@oj', '4', 'Maureen', 'Brom', '1', '283747828', '1972-06-25', 'maureenbrom@gmail.com', 'Dutch', 'Dutch', 'Tilburg', '5346TH', 'Jacobslaan', '0602847564', '0634567623', 1),
(97, 'MediaBazaarEindhoven', 'lisa.jonkers.mediabazaar', 'w5aUYEZu', '4', 'Lisa', 'Jonkers', '1', '235758372', '1973-04-17', 'lisajonkers@gmail.com', 'Belgian', 'Dutch,English,German', 'Elst', '4673PS', 'Dolemeinstraat', '0673844593', '0603048563', 1),
(98, 'MediaBazaarEindhoven', 'kim.hansema.mediabazaar', 'iitst9nX', '4', 'Kim', 'Hansema', '1', '322584736', '1974-01-12', 'kimhansema@gmail.com', 'Belgian', 'Dutch,English', 'Angeren', '6354CP', 'Dorsvlegel', '0634959938', '0605364289', 0),
(99, 'MediaBazaarEindhoven', 'stijn.melchers.mediabazaar', '#yirDKid', '4', 'Stijn', 'Melchers', '0', '234758238', '1975-06-03', 'stijnmelchers@gmail.com', 'Dutch', 'Dutch,English', 'Doornenburg', '6473TF', 'Krakkedel', '0682949483', '0612658374', 0),
(100, 'MediaBazaarEindhoven', 'tom.reversberg.mediabazaar', 'ldhA%%P%', '4', 'Tom', 'Reversberg', '0', '235748957', '1976-05-23', 'tomreversberg@gmail.com', 'Dutch', 'Dutch,English', 'Bemmel', '6681TH', 'Floralaan', '0637483987', '0643287612', 0),
(101, 'MediaBazaarEindhoven', 'eva.pere.mediabazaar', 'MKPi*2V)', '4', 'Eva', 'Pere', '1', '264593947', '1976-05-03', 'evapere@gmail.com', 'Dutch', 'Dutch,English,German', 'Gendt', '6684BP', 'Utrechtseweg', '0647346235', '0682341124', 1),
(102, 'MediaBazaarEindhoven', 'linda.bruinsma.mediabazaar', 'mYHr7D-u', '4', 'Linda', 'Bruinsma', '1', '253647382', '1977-01-09', 'lindabruinsma@gmail.com', 'Dutch', 'Dutch,English', 'Renswoude', '5436WD', 'Maarspad', '0643526454', '0619835563', 1),
(103, 'MediaBazaarEindhoven', 'jan.verhoef.mediabazaar', '5aMLij~3', '4', 'Jan', 'Verhoef', '0', '647272635', '1977-05-13', 'janverhoef@gmail.com', 'Dutch', 'Dutch,English,German', 'Maastricht', '5243GM', 'Kanaalweg', '0637585937', '0623546485', 0),
(104, 'MediaBazaarEindhoven', 'judith.appel.mediabazaar', '0iQ^kCFo', '0', 'Judith', 'Appel', '1', '234856937', '1996-02-15', 'judithappel@gmail.com', 'Dutch', 'Dutch,English', 'Hengelo', '2345TH', 'Bekkersstraat', '0628392958', '0675849284', 0),
(105, 'MediaBazaarEindhoven', 'pim.roosmalen.mediabazaar', 'viwbZZ98', '0', 'Pim', 'Roosmalen', '0', '235657485', '1995-09-27', 'pimroosmalenl@gmail.com', 'Dutch', 'Dutch', 'Nijmegen', '4564GB', 'Molenstraat', '0653245543', '0612356743', 0),
(106, 'MediaBazaarEindhoven', 'jeroen.evers.mediabazaar', '^(S5@Y&)', '0', 'Jeroen', 'Evers', '0', '234758483', '1993-11-10', 'jeroenevers@gmail.com', 'Dutch', 'Dutch,English', 'Assen', '4387PF', 'Het Meerendal', '0634827450', '0610928192', 0),
(107, 'MediaBazaarEindhoven', 'abby.hillecom.mediabazaar', '6$-5^yIZ', '2', 'Abby', 'Hillecom', '1', '237484827', '1994-08-05', 'abbyhillecom@live.nl', 'Dutch', 'Dutch', 'Valkenswaard', '3425TH', 'Donkersstraat', '0673421125', '0654123458', 0),
(108, 'MediaBazaarEindhoven', 'monique.straten.mediabazaar', '+$&uW%pF', '0', 'Monique', 'Straten', '1', '234753831', '1999-06-08', 'moniquestraten@gmail.com', 'Dutch', 'Dutch', 'Haalderen', '4665DV', 'Langestraat', '0637422811', '0618723909', 0),
(109, 'MediaBazaarEindhoven', 'karlijn.peters.mediabazaar', 'mN**k5CR', '0', 'Karlijn', 'Peters', '1', '234753838', '2068-07-11', 'karlijnpeters@gmail.com', 'Dutch', 'Dutch,English', 'Arnhem', '6453TR', 'Apeldoornseweg', '0644372891', '0617324528', 1),
(110, 'MediaBazaarEindhoven', 'pietje.puk.mediabazaar', 'Zp#Qz)PH', '0', 'Pietje', 'Puk', '0', '825491054', '1989-11-15', 'pietjepuk@hotmail.nl', 'Dutch', 'Dutch,English', 'Breda', '8175NM', 'Tramstraat', '06819261054', '06819262845', 1),
(111, 'MediaBazaarEindhoven', 'jarod.reves.mediabazaar', 'Md4y(cWv', '0', 'Jarod', 'Reves', '0', '019482758', '1997-07-17', 'jarodreves@hotmail.com', 'American', 'Dutch,English', 'Eindhoven', '5611KV', 'Don Boscostraat', '0609257251', '0681215858', 0),
(112, 'MediaBazaarEindhoven', 'theo.gieles.mediabazaar', 'GIvVj)$J', '0', 'Theo', 'Gieles', '0', '884259300', '1998-08-27', 'theogieles@gmail.com', 'Dutch', 'Dutch,English', 'Eindhoven', '5611KV', 'Don Boscostraat', '0617963238', '0613507365', 0),
(113, 'MediaBazaarEindhoven', 'hannes.kempe.mediabazaar', 'Cxz~3Y@m', '0', 'Hannes', 'Kempe', '0', '665278213', '1997-09-30', 'hanneskempe@hotmail.com', 'Dutch', 'Dutch,English,German', 'Eindhoven', '5611KV', 'Don Boscostraat', '0666527821', '0699032163', 0),
(114, 'MediaBazaarEindhoven', 'silvana.huijbregts.mediabazaar', 'RovT^P4t', '0', 'Silvana', 'Huijbregts', '1', '184946947', '2002-09-23', 'silvanahuijbregts@live.nl', 'Dutch', 'Dutch', 'Eindhoven', '5611KV', 'Don Boscostraat', '0629069326', '0667381598', 0),
(115, 'MediaBazaarEindhoven', 'yvette.hoorn.mediabazaar', 'K*MYeM!2', '0', 'Yvette', 'Hoorn', '1', '176724269', '2000-06-14', 'yvettehoorn@live.nl', 'Dutch', 'Dutch', 'Eindhoven', '5611KV', 'Don Boscostraat', '0649543837', '0634063286', 0),
(116, 'MediaBazaarEindhoven', 'jurre.juurlink.mediabazaar', 'wb!Fu7X~', '0', 'Jurre', 'Juurlink', '0', '628645368', '1998-10-29', 'jurrejuurlink@hotmail.com', 'Dutch', 'Dutch,English', 'Eindhoven', '5611KV', 'Don Boscostraat', '0686075008', '0616478130', 0),
(117, 'MediaBazaarEindhoven', 'daan.vijselaar.mediabazaar', 'rwU7ONp4', '0', 'Daan', 'Vijselaar', '0', '731303590', '1997-05-12', 'daanvijselaar@live.nl', 'Dutch', 'Dutch', 'Eindhoven', '5611KV', 'Don Boscostraat', '0664248646', '0623130972', 0),
(118, 'MediaBazaarEindhoven', 'mathijs.moonen.mediabazaar', '5~MzXjIB', '0', 'Mathijs', 'Moonen', '0', '870336687', '2000-06-21', 'mathijsmoonen@gmail.com', 'Dutch', 'Dutch,English', 'Eindhoven', '5611KV', 'Don Boscostraat', '0652413693', '0676412040', 0),
(119, 'MediaBazaarEindhoven', 'kris.kraslot.mediabazaar', '#~8uC65o', '0', 'Kris', 'Kraslot', '0', '298993121', '1975-07-18', 'kriskraslot@hotmail.com', 'Dutch', 'Dutch', 'Eindhoven', '8126KW', 'Metrostraat', '0690156600', '0602542863', 0),
(120, 'MediaBazaarEindhoven', 'michel.meiland.mediabazaar', 'v2&ruIIX', '0', 'Michel', 'Meiland', '0', '565155420', '1998-10-28', 'michelmeiland@hotmail.com', 'Dutch', 'Dutch,English', 'Eindhoven', '5611KV', 'Don Boscostraat', '0640412999', '0674365109', 0),
(122, 'MediaBazaarEindhoven', 'robin.brattinga.mediabazaar', 'Z8*I9-tF', '0', 'Robin', 'Brattinga', '0', '004462018', '1998-07-17', 'robinbrattinga@hotmail.nl', 'Dutch', 'Dutch,English', 'Eindhoven', '5611KV', 'Don Boscostraat', ' 0634785535', '0697840098', 0),
(123, 'MediaBazaarEindhoven', 'piet.paulusma.mediabazaar', 'SGfMvGQX', '0', 'Piet', 'Paulusma', '0', '049505595', '1984-06-20', 'pietpaulusma@hotmail.com', 'Dutch', 'Dutch,English', 'Noord-Holland', '5782KA', 'Wolstraat', '0604934606', '0681070317', 0),
(124, 'MediaBazaarEindhoven', 'okke.rademaker.mediabazaar', 'bb9f&5$~', '0', 'Okke', 'Rademaker', '0', '173093409', '2000-06-24', 'okkerademaker@gmail.com', 'Dutch', 'Dutch,English', 'Eindhoven', '8265KW', 'Cyclamenstraat', '0600372978', '0670543542', 0),
(125, 'MediaBazaarEindhoven', 'daan.reins.mediabazaar', '^0n3fI%4', '0', 'Daan', 'Reins', '0', '705435420', '1998-07-17', 'daanreins@gmail.com', 'Dutch', 'Dutch,English', 'Eindhoven', '8172KL', 'Faramir', '0670249483', '0683781286', 1),
(126, 'MediaBazaarEindhoven', 'moira.dam.mediabazaar', 'h^W@0mzT', '5', 'Moira', 'Dam', '1', '657473882', '1994-04-08', 'moiradam@gmail.com', 'Dutch', 'Dutch,English', 'Huissen', '6453TG', 'Generaalstraat', '0647564736', '0689234516', 0),
(127, 'MediaBazaarEindhoven', 'eva .smits.mediabazaar', 'UW6(mPp*', '5', 'Eva ', 'Smits', '1', '546372636', '1989-06-23', 'evasmits@gmail.com', 'Dutch', 'Dutch,English', 'Bemmel', '6674LH', 'Hertekamp', '0678342516', '0648929345', 0),
(128, 'MediaBazaarEindhoven', 'stef.koenen.mediabazaar', 'srNGN9Fn', '5', 'Stef', 'Koenen', '0', '747362737', '1989-06-16', 'stefkoenen@gmail.com', 'Dutch', 'Dutch,English', 'Soest', '3672KB', 'Koninginelaan', '0672837466', '0612389564', 0),
(129, 'MediaBazaarEindhoven', 'eva.dam.mediabazaar', '12XzsN#8', '5', 'Eva', 'Dam', '1', '54637364', '1999-06-16', 'Evadam@gmail.com', 'Dutch', 'Dutch,English', 'Bemmel', '6683TG', 'Pomona', '0673847375', '0612746347', 0);

-- --------------------------------------------------------

--
-- Table structure for table `product`
--

CREATE TABLE `product` (
  `barcode` int(11) NOT NULL,
  `store` varchar(50) NOT NULL,
  `productname` varchar(50) NOT NULL,
  `modelnumber` int(11) NOT NULL,
  `manufacturerbrand` text NOT NULL,
  `category` text NOT NULL,
  `normalprice` double NOT NULL,
  `promotionprice` double DEFAULT NULL,
  `stockindepot` int(11) NOT NULL,
  `stockonshelves` int(11) NOT NULL,
  `amountsold` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `product`
--

INSERT INTO `product` (`barcode`, `store`, `productname`, `modelnumber`, `manufacturerbrand`, `category`, `normalprice`, `promotionprice`, `stockindepot`, `stockonshelves`, `amountsold`) VALUES
(1, 'MediaBazaarEindhoven', 'charger samsung usb-c', 65, 'samsung', 'chargers', 20, 18, 44, 12, 34),
(2, 'MediaBazaarEindhoven', 'earphones', 3, 'apple', 'sound', 30, 26, 29, 16, 23),
(3, 'MediaBazaarEindhoven', 'keyboard', 30, 'philips', 'pc', 50, 35, 10, 20, 4),
(4, 'MediaBazaarEindhoven', 'cap', 12, 'nike', 'clothing', 10, 7, 10, 10, 1),
(5, 'MediaBazaarEindhoven', 'monitor', 12, 'philips', 'pc', 150, 130, 5, 20, 8),
(6, 'MediaBazaarEindhoven', 'galaxy s10', 10, 'samsung', 'mobile phones', 560, 500, 5, 2, 8),
(7, 'MediaBazaarEindhoven', 'iphone 6', 6, 'apple', 'mobile phones', 400, 350, 5, 7, 4),
(8, 'MediaBazaarEindhoven', 'zbook laptop', 73, 'hp', 'laptop', 800, 765, 1, 2, 4),
(9, 'MediaBazaarEindhoven', 'washing machine', 9, 'siemens', 'appliances', 1000, 950, 2, 1, 3),
(10, 'MediaBazaarEindhoven', 'charger iphone', 9, 'apple', 'chargers', 20, 17, 10, 8, 12),
(11, 'MediaBazaarEindhoven', 'airpods', 300, 'apple', 'sound', 180, 160, 30, 21, 65),
(12, 'MediaBazaarEindhoven', 'acer 2039', 2039, 'acer', 'laptops', 1200, 1100, 5, 2, 7),
(13, 'MediaBazaarEindhoven', 'vacuum cleaner', 310899, 'philips', 'appliances', 80, 70, 6, 8, 3),
(14, 'MediaBazaarEindhoven', 'steering wheel', 2784, 'hyundai', 'car accessories', 200, 180, 6, 3, 2);

-- --------------------------------------------------------

--
-- Table structure for table `restockrequest`
--

CREATE TABLE `restockrequest` (
  `id` int(11) NOT NULL,
  `store` varchar(50) NOT NULL,
  `productbarcode` int(11) NOT NULL,
  `productamount` int(11) NOT NULL,
  `floor` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `delivered` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `restockrequest`
--

INSERT INTO `restockrequest` (`id`, `store`, `productbarcode`, `productamount`, `floor`, `date`, `delivered`) VALUES
(75, 'MediaBazaarEindhoven', 2, 3, 'MobileDevices', '2020-03-22', 1),
(76, 'MediaBazaarEindhoven', 6, 8, 'MobileDevices', '2020-03-22', 0),
(77, 'MediaBazaarEindhoven', 1, 2, 'MobileDevices', '2020-03-23', 0),
(78, 'MediaBazaarEindhoven', 6, 6, 'MobileDevices', '2020-03-23', 0),
(79, 'MediaBazaarEindhoven', 7, 5, 'MobileDevices', '2020-03-26', 1),
(80, 'MediaBazaarEindhoven', 12, 2, 'MobileDevices', '2020-03-26', 1),
(81, 'MediaBazaarEindhoven', 1, 4, 'MobileDevices', '2020-06-18', 0),
(91, 'MediaBazaarEindhoven', 9, 7, 'HomeAutomation', '2020-06-18', 0),
(92, 'MediaBazaarEindhoven', 13, 5, 'HomeAutomation', '2020-06-18', 0),
(93, 'MediaBazaarEindhoven', 10, 3, 'MobileDevices', '2020-06-18', 0),
(94, 'MediaBazaarEindhoven', 3, 7, 'Entertainments', '2020-06-19', 0),
(95, 'MediaBazaarEindhoven', 3, 7, 'Entertainments', '2020-06-19', 0),
(96, 'MediaBazaarEindhoven', 2, 4, 'Mobile devices', '2020-06-19', 0),
(97, 'MediaBazaarEindhoven', 2, 4, 'Mobile devices', '2020-06-19', 0);

-- --------------------------------------------------------

--
-- Table structure for table `shift`
--

CREATE TABLE `shift` (
  `id` int(11) NOT NULL,
  `employeeid` int(11) NOT NULL,
  `date` date NOT NULL,
  `daysegment` varchar(50) NOT NULL,
  `floor` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `shift`
--

INSERT INTO `shift` (`id`, `employeeid`, `date`, `daysegment`, `floor`) VALUES
(144, 61, '2020-06-09', 'morning', 'Manager'),
(147, 62, '2020-06-10', 'afternoon', 'Depot worker'),
(149, 71, '2020-06-18', 'morning', 'Manager'),
(150, 72, '2020-06-18', 'morning', 'Manager'),
(151, 72, '2020-06-17', 'afternoon', 'Manager'),
(152, 73, '2020-06-17', 'afternoon', 'Manager'),
(153, 64, '2020-06-15', 'morning', 'Administration'),
(154, 65, '2020-06-15', 'morning', 'Administration'),
(155, 64, '2020-06-17', 'evening', 'Administration'),
(156, 66, '2020-06-17', 'evening', 'Administration'),
(182, 71, '2020-06-25', 'morning', 'Manager'),
(183, 72, '2020-06-24', 'afternoon', 'Manager'),
(184, 80, '2020-06-27', 'morning', 'Manager'),
(185, 74, '2020-06-17', 'afternoon', 'Manager'),
(186, 75, '2020-06-17', 'afternoon', 'Manager'),
(187, 75, '2020-06-19', 'afternoon', 'Manager'),
(188, 66, '2020-06-01', 'morning', 'Administration'),
(189, 66, '2020-06-03', 'evening', 'Administration'),
(190, 66, '2020-06-04', 'evening', 'Administration'),
(191, 66, '2020-06-05', 'afternoon', 'Administration'),
(192, 66, '2020-06-02', 'morning', 'Administration'),
(193, 66, '2020-06-03', 'morning', 'Administration'),
(194, 66, '2020-06-18', 'morning', 'Administration'),
(195, 66, '2020-06-08', 'morning', 'Administration'),
(196, 66, '2020-06-11', 'morning', 'Administration'),
(197, 66, '2020-06-13', 'morning', 'Administration'),
(198, 66, '2020-06-14', 'afternoon', 'Administration'),
(199, 66, '2020-06-24', 'evening', 'Administration'),
(200, 66, '2020-06-30', 'evening', 'Administration'),
(201, 66, '2020-06-22', 'evening', 'Administration'),
(202, 71, '2020-06-01', 'morning', 'Manager'),
(203, 74, '2020-06-01', 'morning', 'Manager'),
(204, 71, '2020-06-01', 'afternoon', 'Manager'),
(205, 73, '2020-06-01', 'afternoon', 'Manager'),
(206, 71, '2020-06-01', 'evening', 'Manager'),
(207, 73, '2020-06-01', 'evening', 'Manager'),
(208, 74, '2020-06-01', 'evening', 'Manager'),
(209, 61, '2020-06-02', 'morning', 'Manager'),
(210, 71, '2020-06-02', 'morning', 'Manager'),
(211, 71, '2020-06-02', 'afternoon', 'Manager'),
(212, 75, '2020-06-02', 'afternoon', 'Manager'),
(213, 80, '2020-06-02', 'afternoon', 'Manager'),
(214, 71, '2020-06-02', 'evening', 'Manager'),
(215, 72, '2020-06-02', 'evening', 'Manager'),
(216, 75, '2020-06-02', 'evening', 'Manager'),
(217, 71, '2020-06-03', 'morning', 'Manager'),
(218, 72, '2020-06-03', 'afternoon', 'Manager'),
(219, 73, '2020-06-03', 'afternoon', 'Manager'),
(220, 61, '2020-06-03', 'evening', 'Manager'),
(221, 61, '2020-06-04', 'morning', 'Manager'),
(222, 71, '2020-06-04', 'morning', 'Manager'),
(223, 61, '2020-06-04', 'afternoon', 'Manager'),
(224, 71, '2020-06-04', 'afternoon', 'Manager'),
(225, 72, '2020-06-04', 'afternoon', 'Manager'),
(226, 61, '2020-06-04', 'evening', 'Manager'),
(227, 71, '2020-06-04', 'evening', 'Manager'),
(228, 72, '2020-06-04', 'evening', 'Manager'),
(229, 61, '2020-06-05', 'morning', 'Manager'),
(230, 74, '2020-06-05', 'morning', 'Manager'),
(231, 61, '2020-06-05', 'afternoon', 'Manager'),
(232, 73, '2020-06-05', 'afternoon', 'Manager'),
(233, 74, '2020-06-05', 'afternoon', 'Manager'),
(234, 75, '2020-06-05', 'afternoon', 'Manager'),
(235, 61, '2020-06-05', 'evening', 'Manager'),
(236, 72, '2020-06-05', 'evening', 'Manager'),
(237, 71, '2020-06-06', 'morning', 'Manager'),
(238, 72, '2020-06-06', 'morning', 'Manager'),
(239, 61, '2020-06-06', 'afternoon', 'Manager'),
(240, 71, '2020-06-06', 'afternoon', 'Manager'),
(241, 72, '2020-06-06', 'afternoon', 'Manager'),
(242, 73, '2020-06-06', 'afternoon', 'Manager'),
(243, 71, '2020-06-06', 'evening', 'Manager'),
(244, 72, '2020-06-06', 'evening', 'Manager'),
(245, 73, '2020-06-06', 'evening', 'Manager'),
(246, 74, '2020-06-06', 'evening', 'Manager'),
(247, 61, '2020-06-07', 'morning', 'Manager'),
(248, 72, '2020-06-07', 'evening', 'Manager'),
(249, 61, '2020-06-07', 'afternoon', 'Manager'),
(250, 64, '2020-06-01', 'morning', 'Administration'),
(251, 65, '2020-06-01', 'morning', 'Administration'),
(252, 64, '2020-06-01', 'afternoon', 'Administration'),
(253, 65, '2020-06-01', 'afternoon', 'Administration'),
(254, 66, '2020-06-01', 'afternoon', 'Administration'),
(255, 67, '2020-06-01', 'afternoon', 'Administration'),
(256, 64, '2020-06-01', 'evening', 'Administration'),
(257, 65, '2020-06-01', 'evening', 'Administration'),
(258, 67, '2020-06-01', 'evening', 'Administration'),
(259, 68, '2020-06-01', 'evening', 'Administration'),
(260, 69, '2020-06-01', 'evening', 'Administration'),
(261, 64, '2020-06-02', 'morning', 'Administration'),
(262, 65, '2020-06-02', 'morning', 'Administration'),
(263, 67, '2020-06-02', 'morning', 'Administration'),
(264, 68, '2020-06-02', 'morning', 'Administration'),
(265, 64, '2020-06-02', 'afternoon', 'Administration'),
(266, 69, '2020-06-02', 'afternoon', 'Administration'),
(267, 64, '2020-06-02', 'evening', 'Administration'),
(268, 65, '2020-06-02', 'evening', 'Administration'),
(269, 66, '2020-06-02', 'evening', 'Administration'),
(270, 69, '2020-06-02', 'evening', 'Administration'),
(271, 65, '2020-06-03', 'morning', 'Administration'),
(272, 68, '2020-06-03', 'morning', 'Administration'),
(273, 65, '2020-06-03', 'afternoon', 'Administration'),
(274, 66, '2020-06-03', 'afternoon', 'Administration'),
(275, 67, '2020-06-03', 'afternoon', 'Administration'),
(276, 67, '2020-06-03', 'evening', 'Administration'),
(277, 68, '2020-06-03', 'evening', 'Administration'),
(278, 65, '2020-06-04', 'morning', 'Administration'),
(279, 66, '2020-06-04', 'morning', 'Administration'),
(280, 65, '2020-06-04', 'afternoon', 'Administration'),
(281, 66, '2020-06-04', 'afternoon', 'Administration'),
(282, 68, '2020-06-04', 'afternoon', 'Administration'),
(283, 65, '2020-06-04', 'evening', 'Administration'),
(284, 69, '2020-06-04', 'evening', 'Administration'),
(285, 69, '2020-06-05', 'morning', 'Administration'),
(286, 70, '2020-06-05', 'morning', 'Administration'),
(287, 65, '2020-06-05', 'afternoon', 'Administration'),
(288, 67, '2020-06-05', 'afternoon', 'Administration'),
(289, 65, '2020-06-05', 'evening', 'Administration'),
(290, 67, '2020-06-05', 'evening', 'Administration'),
(291, 65, '2020-06-06', 'morning', 'Administration'),
(292, 66, '2020-06-06', 'morning', 'Administration'),
(293, 67, '2020-06-06', 'morning', 'Administration'),
(294, 66, '2020-06-06', 'afternoon', 'Administration'),
(295, 68, '2020-06-06', 'afternoon', 'Administration'),
(296, 65, '2020-06-06', 'evening', 'Administration'),
(297, 66, '2020-06-06', 'evening', 'Administration'),
(298, 67, '2020-06-06', 'evening', 'Administration'),
(299, 68, '2020-06-06', 'evening', 'Administration'),
(300, 70, '2020-06-06', 'evening', 'Administration'),
(301, 65, '2020-06-07', 'morning', 'Administration'),
(302, 67, '2020-06-07', 'morning', 'Administration'),
(303, 65, '2020-06-07', 'afternoon', 'Administration'),
(304, 66, '2020-06-07', 'afternoon', 'Administration'),
(305, 66, '2020-06-07', 'evening', 'Administration'),
(306, 68, '2020-06-07', 'evening', 'Administration'),
(307, 62, '2020-06-01', 'morning', 'Depot worker'),
(308, 77, '2020-06-01', 'morning', 'Depot worker'),
(309, 78, '2020-06-01', 'morning', 'Depot worker'),
(310, 79, '2020-06-01', 'morning', 'Depot worker'),
(311, 84, '2020-06-01', 'morning', 'Depot worker'),
(312, 86, '2020-06-01', 'morning', 'Depot worker'),
(313, 62, '2020-06-01', 'afternoon', 'Depot worker'),
(314, 76, '2020-06-01', 'afternoon', 'Depot worker'),
(315, 77, '2020-06-01', 'afternoon', 'Depot worker'),
(316, 78, '2020-06-01', 'afternoon', 'Depot worker'),
(317, 79, '2020-06-01', 'afternoon', 'Depot worker'),
(318, 83, '2020-06-01', 'afternoon', 'Depot worker'),
(319, 62, '2020-06-01', 'evening', 'Depot worker'),
(320, 76, '2020-06-01', 'evening', 'Depot worker'),
(321, 77, '2020-06-01', 'evening', 'Depot worker'),
(322, 78, '2020-06-01', 'evening', 'Depot worker'),
(323, 62, '2020-06-02', 'morning', 'Depot worker'),
(324, 76, '2020-06-02', 'morning', 'Depot worker'),
(325, 77, '2020-06-02', 'morning', 'Depot worker'),
(326, 78, '2020-06-02', 'morning', 'Depot worker'),
(327, 83, '2020-06-02', 'morning', 'Depot worker'),
(328, 62, '2020-06-02', 'afternoon', 'Depot worker'),
(329, 76, '2020-06-02', 'afternoon', 'Depot worker'),
(330, 77, '2020-06-02', 'afternoon', 'Depot worker'),
(331, 86, '2020-06-02', 'afternoon', 'Depot worker'),
(332, 88, '2020-06-02', 'afternoon', 'Depot worker'),
(333, 62, '2020-06-02', 'evening', 'Depot worker'),
(334, 77, '2020-06-02', 'evening', 'Depot worker'),
(335, 83, '2020-06-02', 'evening', 'Depot worker'),
(336, 62, '2020-06-03', 'morning', 'Depot worker'),
(337, 76, '2020-06-03', 'morning', 'Depot worker'),
(338, 77, '2020-06-03', 'morning', 'Depot worker'),
(339, 79, '2020-06-03', 'morning', 'Depot worker'),
(340, 83, '2020-06-03', 'morning', 'Depot worker'),
(341, 84, '2020-06-03', 'morning', 'Depot worker'),
(342, 62, '2020-06-03', 'afternoon', 'Depot worker'),
(343, 76, '2020-06-03', 'afternoon', 'Depot worker'),
(344, 77, '2020-06-03', 'afternoon', 'Depot worker'),
(345, 79, '2020-06-03', 'afternoon', 'Depot worker'),
(346, 62, '2020-06-03', 'evening', 'Depot worker'),
(347, 76, '2020-06-03', 'evening', 'Depot worker'),
(348, 77, '2020-06-03', 'evening', 'Depot worker'),
(349, 78, '2020-06-03', 'evening', 'Depot worker'),
(350, 62, '2020-06-04', 'morning', 'Depot worker'),
(351, 77, '2020-06-04', 'morning', 'Depot worker'),
(352, 78, '2020-06-04', 'morning', 'Depot worker'),
(353, 83, '2020-06-04', 'morning', 'Depot worker'),
(354, 62, '2020-06-04', 'afternoon', 'Depot worker'),
(355, 77, '2020-06-04', 'afternoon', 'Depot worker'),
(356, 78, '2020-06-04', 'afternoon', 'Depot worker'),
(357, 62, '2020-06-04', 'evening', 'Depot worker'),
(358, 76, '2020-06-04', 'evening', 'Depot worker'),
(359, 62, '2020-06-05', 'morning', 'Depot worker'),
(360, 76, '2020-06-05', 'morning', 'Depot worker'),
(361, 77, '2020-06-05', 'morning', 'Depot worker'),
(362, 78, '2020-06-05', 'morning', 'Depot worker'),
(363, 62, '2020-06-05', 'afternoon', 'Depot worker'),
(364, 76, '2020-06-05', 'afternoon', 'Depot worker'),
(365, 77, '2020-06-05', 'afternoon', 'Depot worker'),
(366, 78, '2020-06-05', 'afternoon', 'Depot worker'),
(367, 62, '2020-06-05', 'evening', 'Depot worker'),
(368, 76, '2020-06-05', 'evening', 'Depot worker'),
(369, 77, '2020-06-05', 'evening', 'Depot worker'),
(370, 78, '2020-06-05', 'evening', 'Depot worker'),
(371, 62, '2020-06-06', 'morning', 'Depot worker'),
(372, 76, '2020-06-06', 'morning', 'Depot worker'),
(373, 77, '2020-06-06', 'morning', 'Depot worker'),
(374, 79, '2020-06-06', 'morning', 'Depot worker'),
(375, 83, '2020-06-06', 'morning', 'Depot worker'),
(376, 62, '2020-06-06', 'afternoon', 'Depot worker'),
(377, 76, '2020-06-06', 'afternoon', 'Depot worker'),
(378, 77, '2020-06-06', 'afternoon', 'Depot worker'),
(379, 79, '2020-06-06', 'afternoon', 'Depot worker'),
(380, 62, '2020-06-06', 'evening', 'Depot worker'),
(381, 76, '2020-06-06', 'evening', 'Depot worker'),
(382, 77, '2020-06-06', 'evening', 'Depot worker'),
(383, 62, '2020-06-07', 'morning', 'Depot worker'),
(384, 76, '2020-06-07', 'morning', 'Depot worker'),
(385, 77, '2020-06-07', 'morning', 'Depot worker'),
(386, 62, '2020-06-07', 'afternoon', 'Depot worker'),
(387, 77, '2020-06-07', 'afternoon', 'Depot worker'),
(388, 78, '2020-06-07', 'afternoon', 'Depot worker'),
(389, 62, '2020-06-07', 'evening', 'Depot worker'),
(390, 77, '2020-06-07', 'evening', 'Depot worker'),
(391, 92, '2020-06-01', 'morning', 'Security'),
(392, 94, '2020-06-01', 'morning', 'Security'),
(393, 95, '2020-06-01', 'morning', 'Security'),
(394, 97, '2020-06-01', 'morning', 'Security'),
(395, 92, '2020-06-01', 'afternoon', 'Security'),
(396, 94, '2020-06-01', 'afternoon', 'Security'),
(397, 95, '2020-06-01', 'afternoon', 'Security'),
(398, 97, '2020-06-01', 'afternoon', 'Security'),
(399, 92, '2020-06-01', 'evening', 'Security'),
(400, 94, '2020-06-01', 'evening', 'Security'),
(401, 95, '2020-06-01', 'evening', 'Security'),
(402, 96, '2020-06-01', 'evening', 'Security'),
(403, 92, '2020-06-02', 'morning', 'Security'),
(404, 93, '2020-06-02', 'morning', 'Security'),
(405, 95, '2020-06-02', 'morning', 'Security'),
(406, 96, '2020-06-02', 'morning', 'Security'),
(407, 92, '2020-06-02', 'afternoon', 'Security'),
(408, 93, '2020-06-02', 'afternoon', 'Security'),
(409, 95, '2020-06-02', 'afternoon', 'Security'),
(410, 96, '2020-06-02', 'afternoon', 'Security'),
(411, 92, '2020-06-02', 'evening', 'Security'),
(412, 93, '2020-06-02', 'evening', 'Security'),
(413, 95, '2020-06-02', 'evening', 'Security'),
(414, 97, '2020-06-02', 'evening', 'Security'),
(415, 92, '2020-06-03', 'morning', 'Security'),
(416, 93, '2020-06-03', 'morning', 'Security'),
(417, 95, '2020-06-03', 'morning', 'Security'),
(418, 97, '2020-06-03', 'morning', 'Security'),
(419, 92, '2020-06-03', 'afternoon', 'Security'),
(420, 93, '2020-06-03', 'afternoon', 'Security'),
(421, 95, '2020-06-03', 'afternoon', 'Security'),
(422, 96, '2020-06-03', 'afternoon', 'Security'),
(423, 92, '2020-06-03', 'evening', 'Security'),
(424, 93, '2020-06-03', 'evening', 'Security'),
(425, 95, '2020-06-03', 'evening', 'Security'),
(426, 96, '2020-06-03', 'evening', 'Security'),
(427, 92, '2020-06-04', 'morning', 'Security'),
(428, 93, '2020-06-04', 'morning', 'Security'),
(429, 94, '2020-06-04', 'morning', 'Security'),
(430, 95, '2020-06-04', 'morning', 'Security'),
(431, 92, '2020-06-04', 'afternoon', 'Security'),
(432, 93, '2020-06-04', 'afternoon', 'Security'),
(433, 94, '2020-06-04', 'afternoon', 'Security'),
(434, 95, '2020-06-04', 'afternoon', 'Security'),
(435, 92, '2020-06-04', 'evening', 'Security'),
(436, 93, '2020-06-04', 'evening', 'Security'),
(437, 94, '2020-06-04', 'evening', 'Security'),
(438, 95, '2020-06-04', 'evening', 'Security'),
(439, 92, '2020-06-05', 'morning', 'Security'),
(440, 93, '2020-06-05', 'morning', 'Security'),
(441, 94, '2020-06-05', 'morning', 'Security'),
(442, 95, '2020-06-05', 'morning', 'Security'),
(443, 92, '2020-06-05', 'afternoon', 'Security'),
(444, 93, '2020-06-05', 'afternoon', 'Security'),
(445, 94, '2020-06-05', 'afternoon', 'Security'),
(446, 95, '2020-06-05', 'afternoon', 'Security'),
(447, 92, '2020-06-05', 'evening', 'Security'),
(448, 94, '2020-06-05', 'evening', 'Security'),
(449, 95, '2020-06-05', 'evening', 'Security'),
(450, 96, '2020-06-05', 'evening', 'Security'),
(451, 94, '2020-06-06', 'morning', 'Security'),
(452, 95, '2020-06-06', 'morning', 'Security'),
(453, 97, '2020-06-06', 'morning', 'Security'),
(454, 99, '2020-06-06', 'morning', 'Security'),
(455, 94, '2020-06-06', 'afternoon', 'Security'),
(456, 96, '2020-06-06', 'afternoon', 'Security'),
(457, 97, '2020-06-06', 'afternoon', 'Security'),
(458, 98, '2020-06-06', 'afternoon', 'Security'),
(459, 93, '2020-06-06', 'evening', 'Security'),
(460, 94, '2020-06-06', 'evening', 'Security'),
(461, 96, '2020-06-06', 'evening', 'Security'),
(462, 97, '2020-06-06', 'evening', 'Security'),
(463, 93, '2020-06-07', 'morning', 'Security'),
(464, 94, '2020-06-07', 'morning', 'Security'),
(465, 96, '2020-06-07', 'morning', 'Security'),
(466, 97, '2020-06-07', 'morning', 'Security'),
(467, 93, '2020-06-07', 'afternoon', 'Security'),
(468, 94, '2020-06-07', 'afternoon', 'Security'),
(469, 96, '2020-06-07', 'afternoon', 'Security'),
(470, 97, '2020-06-07', 'afternoon', 'Security'),
(471, 93, '2020-06-07', 'evening', 'Security'),
(472, 94, '2020-06-07', 'evening', 'Security'),
(473, 96, '2020-06-07', 'evening', 'Security'),
(474, 97, '2020-06-07', 'evening', 'Security'),
(475, 85, '2020-06-01', 'morning', 'Mobile devices'),
(476, 104, '2020-06-01', 'morning', 'Mobile devices'),
(477, 106, '2020-06-01', 'morning', 'Mobile devices'),
(478, 85, '2020-06-01', 'afternoon', 'Mobile devices'),
(479, 104, '2020-06-01', 'afternoon', 'Mobile devices'),
(480, 106, '2020-06-01', 'afternoon', 'Mobile devices'),
(481, 108, '2020-06-01', 'afternoon', 'Mobile devices'),
(482, 85, '2020-06-01', 'evening', 'Mobile devices'),
(483, 104, '2020-06-01', 'evening', 'Mobile devices'),
(484, 105, '2020-06-01', 'evening', 'Mobile devices'),
(485, 85, '2020-06-02', 'morning', 'Mobile devices'),
(486, 104, '2020-06-02', 'morning', 'Mobile devices'),
(487, 105, '2020-06-02', 'morning', 'Mobile devices'),
(488, 105, '2020-06-02', 'afternoon', 'Mobile devices'),
(489, 109, '2020-06-02', 'afternoon', 'Mobile devices'),
(490, 111, '2020-06-02', 'afternoon', 'Mobile devices'),
(491, 85, '2020-06-02', 'evening', 'Mobile devices'),
(492, 104, '2020-06-02', 'evening', 'Mobile devices'),
(493, 105, '2020-06-02', 'evening', 'Mobile devices'),
(494, 85, '2020-06-03', 'morning', 'Mobile devices'),
(495, 104, '2020-06-03', 'morning', 'Mobile devices'),
(496, 105, '2020-06-03', 'morning', 'Mobile devices'),
(497, 106, '2020-06-03', 'morning', 'Mobile devices'),
(498, 85, '2020-06-03', 'afternoon', 'Mobile devices'),
(499, 104, '2020-06-03', 'afternoon', 'Mobile devices'),
(500, 105, '2020-06-03', 'afternoon', 'Mobile devices'),
(501, 108, '2020-06-03', 'afternoon', 'Mobile devices'),
(502, 105, '2020-06-03', 'evening', 'Mobile devices'),
(503, 109, '2020-06-03', 'evening', 'Mobile devices'),
(504, 111, '2020-06-03', 'evening', 'Mobile devices'),
(505, 112, '2020-06-03', 'evening', 'Mobile devices'),
(506, 104, '2020-06-04', 'morning', 'Mobile devices'),
(507, 106, '2020-06-04', 'morning', 'Mobile devices'),
(508, 85, '2020-06-04', 'afternoon', 'Mobile devices'),
(509, 104, '2020-06-04', 'afternoon', 'Mobile devices'),
(510, 105, '2020-06-04', 'afternoon', 'Mobile devices'),
(511, 106, '2020-06-04', 'afternoon', 'Mobile devices'),
(512, 85, '2020-06-04', 'evening', 'Mobile devices'),
(513, 104, '2020-06-04', 'evening', 'Mobile devices'),
(514, 105, '2020-06-04', 'evening', 'Mobile devices'),
(515, 106, '2020-06-04', 'evening', 'Mobile devices'),
(516, 108, '2020-06-04', 'evening', 'Mobile devices'),
(517, 85, '2020-06-05', 'morning', 'Mobile devices'),
(518, 105, '2020-06-05', 'morning', 'Mobile devices'),
(519, 104, '2020-06-05', 'afternoon', 'Mobile devices'),
(520, 109, '2020-06-05', 'afternoon', 'Mobile devices'),
(521, 111, '2020-06-05', 'afternoon', 'Mobile devices'),
(522, 104, '2020-06-05', 'evening', 'Mobile devices'),
(523, 105, '2020-06-05', 'evening', 'Mobile devices'),
(524, 106, '2020-06-05', 'evening', 'Mobile devices'),
(525, 108, '2020-06-05', 'evening', 'Mobile devices'),
(526, 85, '2020-06-06', 'morning', 'Mobile devices'),
(527, 104, '2020-06-06', 'morning', 'Mobile devices'),
(528, 105, '2020-06-06', 'morning', 'Mobile devices'),
(529, 106, '2020-06-06', 'morning', 'Mobile devices'),
(530, 85, '2020-06-06', 'afternoon', 'Mobile devices'),
(531, 104, '2020-06-06', 'afternoon', 'Mobile devices'),
(532, 105, '2020-06-06', 'afternoon', 'Mobile devices'),
(533, 106, '2020-06-06', 'afternoon', 'Mobile devices'),
(534, 85, '2020-06-06', 'evening', 'Mobile devices'),
(535, 104, '2020-06-06', 'evening', 'Mobile devices'),
(536, 105, '2020-06-06', 'evening', 'Mobile devices'),
(537, 106, '2020-06-06', 'evening', 'Mobile devices'),
(538, 85, '2020-06-07', 'morning', 'Mobile devices'),
(539, 104, '2020-06-07', 'morning', 'Mobile devices'),
(540, 104, '2020-06-07', 'afternoon', 'Mobile devices'),
(541, 106, '2020-06-07', 'afternoon', 'Mobile devices'),
(542, 106, '2020-06-07', 'evening', 'Mobile devices'),
(543, 108, '2020-06-07', 'evening', 'Mobile devices'),
(544, 85, '2020-06-01', 'morning', 'Home automation'),
(545, 104, '2020-06-01', 'morning', 'Home automation'),
(546, 85, '2020-06-01', 'afternoon', 'Home automation'),
(547, 104, '2020-06-01', 'afternoon', 'Home automation'),
(548, 106, '2020-06-01', 'afternoon', 'Home automation'),
(549, 85, '2020-06-01', 'evening', 'Home automation'),
(550, 104, '2020-06-01', 'evening', 'Home automation'),
(551, 85, '2020-06-02', 'morning', 'Home automation'),
(552, 104, '2020-06-02', 'morning', 'Home automation'),
(553, 105, '2020-06-02', 'morning', 'Home automation'),
(554, 106, '2020-06-02', 'morning', 'Home automation'),
(555, 105, '2020-06-02', 'afternoon', 'Home automation'),
(556, 109, '2020-06-02', 'afternoon', 'Home automation'),
(557, 111, '2020-06-02', 'afternoon', 'Home automation'),
(558, 112, '2020-06-02', 'afternoon', 'Home automation'),
(559, 85, '2020-06-02', 'evening', 'Home automation'),
(560, 104, '2020-06-02', 'evening', 'Home automation'),
(561, 105, '2020-06-02', 'evening', 'Home automation'),
(562, 106, '2020-06-02', 'evening', 'Home automation'),
(563, 85, '2020-06-03', 'morning', 'Home automation'),
(564, 104, '2020-06-03', 'morning', 'Home automation'),
(565, 105, '2020-06-03', 'morning', 'Home automation'),
(566, 85, '2020-06-03', 'afternoon', 'Home automation'),
(567, 104, '2020-06-03', 'afternoon', 'Home automation'),
(568, 105, '2020-06-03', 'afternoon', 'Home automation'),
(569, 105, '2020-06-03', 'evening', 'Home automation'),
(570, 109, '2020-06-03', 'evening', 'Home automation'),
(571, 111, '2020-06-03', 'evening', 'Home automation'),
(572, 104, '2020-06-04', 'morning', 'Home automation'),
(573, 106, '2020-06-04', 'morning', 'Home automation'),
(574, 85, '2020-06-04', 'afternoon', 'Home automation'),
(575, 104, '2020-06-04', 'afternoon', 'Home automation'),
(576, 85, '2020-06-04', 'evening', 'Home automation'),
(577, 104, '2020-06-04', 'evening', 'Home automation'),
(578, 85, '2020-06-05', 'morning', 'Home automation'),
(579, 105, '2020-06-05', 'morning', 'Home automation'),
(580, 108, '2020-06-05', 'morning', 'Home automation'),
(581, 109, '2020-06-05', 'morning', 'Home automation'),
(582, 104, '2020-06-05', 'afternoon', 'Home automation'),
(583, 109, '2020-06-05', 'afternoon', 'Home automation'),
(584, 111, '2020-06-05', 'afternoon', 'Home automation'),
(585, 112, '2020-06-05', 'afternoon', 'Home automation'),
(586, 104, '2020-06-05', 'evening', 'Home automation'),
(587, 105, '2020-06-05', 'evening', 'Home automation'),
(588, 106, '2020-06-05', 'evening', 'Home automation'),
(589, 108, '2020-06-05', 'evening', 'Home automation'),
(590, 85, '2020-06-06', 'morning', 'Home automation'),
(591, 104, '2020-06-06', 'morning', 'Home automation'),
(592, 105, '2020-06-06', 'morning', 'Home automation'),
(593, 85, '2020-06-06', 'afternoon', 'Home automation'),
(594, 104, '2020-06-06', 'afternoon', 'Home automation'),
(595, 105, '2020-06-06', 'afternoon', 'Home automation'),
(596, 85, '2020-06-06', 'evening', 'Home automation'),
(597, 104, '2020-06-06', 'evening', 'Home automation'),
(598, 105, '2020-06-06', 'evening', 'Home automation'),
(599, 85, '2020-06-07', 'morning', 'Home automation'),
(600, 104, '2020-06-07', 'morning', 'Home automation'),
(601, 104, '2020-06-07', 'afternoon', 'Home automation'),
(602, 106, '2020-06-07', 'afternoon', 'Home automation'),
(603, 106, '2020-06-07', 'evening', 'Home automation'),
(604, 108, '2020-06-07', 'evening', 'Home automation'),
(605, 85, '2020-06-01', 'morning', 'Entertainments'),
(606, 104, '2020-06-01', 'morning', 'Entertainments'),
(607, 85, '2020-06-01', 'afternoon', 'Entertainments'),
(608, 104, '2020-06-01', 'afternoon', 'Entertainments'),
(609, 85, '2020-06-01', 'evening', 'Entertainments'),
(610, 104, '2020-06-01', 'evening', 'Entertainments'),
(611, 85, '2020-06-02', 'morning', 'Entertainments'),
(612, 104, '2020-06-02', 'morning', 'Entertainments'),
(613, 105, '2020-06-02', 'morning', 'Entertainments'),
(614, 105, '2020-06-02', 'afternoon', 'Entertainments'),
(615, 109, '2020-06-02', 'afternoon', 'Entertainments'),
(616, 111, '2020-06-02', 'afternoon', 'Entertainments'),
(617, 85, '2020-06-02', 'evening', 'Entertainments'),
(618, 104, '2020-06-02', 'evening', 'Entertainments'),
(619, 105, '2020-06-02', 'evening', 'Entertainments'),
(620, 85, '2020-06-03', 'morning', 'Entertainments'),
(621, 104, '2020-06-03', 'morning', 'Entertainments'),
(622, 85, '2020-06-03', 'afternoon', 'Entertainments'),
(623, 104, '2020-06-03', 'afternoon', 'Entertainments'),
(624, 105, '2020-06-03', 'evening', 'Entertainments'),
(625, 109, '2020-06-03', 'evening', 'Entertainments'),
(626, 104, '2020-06-04', 'morning', 'Entertainments'),
(627, 106, '2020-06-04', 'morning', 'Entertainments'),
(628, 109, '2020-06-04', 'morning', 'Entertainments'),
(629, 85, '2020-06-04', 'afternoon', 'Entertainments'),
(630, 104, '2020-06-04', 'afternoon', 'Entertainments'),
(631, 105, '2020-06-04', 'afternoon', 'Entertainments'),
(632, 85, '2020-06-04', 'evening', 'Entertainments'),
(633, 104, '2020-06-04', 'evening', 'Entertainments'),
(634, 105, '2020-06-04', 'evening', 'Entertainments'),
(635, 85, '2020-06-05', 'morning', 'Entertainments'),
(636, 105, '2020-06-05', 'morning', 'Entertainments'),
(637, 108, '2020-06-05', 'morning', 'Entertainments'),
(638, 104, '2020-06-05', 'afternoon', 'Entertainments'),
(639, 109, '2020-06-05', 'afternoon', 'Entertainments'),
(640, 111, '2020-06-05', 'afternoon', 'Entertainments'),
(641, 104, '2020-06-05', 'evening', 'Entertainments'),
(642, 105, '2020-06-05', 'evening', 'Entertainments'),
(643, 106, '2020-06-05', 'evening', 'Entertainments'),
(644, 85, '2020-06-06', 'morning', 'Entertainments'),
(645, 104, '2020-06-06', 'morning', 'Entertainments'),
(646, 105, '2020-06-06', 'morning', 'Entertainments'),
(647, 106, '2020-06-06', 'morning', 'Entertainments'),
(648, 85, '2020-06-06', 'evening', 'Entertainments'),
(649, 104, '2020-06-06', 'evening', 'Entertainments'),
(650, 105, '2020-06-06', 'evening', 'Entertainments'),
(651, 106, '2020-06-06', 'evening', 'Entertainments'),
(652, 85, '2020-06-06', 'afternoon', 'Entertainments'),
(653, 104, '2020-06-06', 'afternoon', 'Entertainments'),
(654, 105, '2020-06-06', 'afternoon', 'Entertainments'),
(655, 106, '2020-06-06', 'afternoon', 'Entertainments'),
(656, 85, '2020-06-07', 'morning', 'Entertainments'),
(657, 104, '2020-06-07', 'morning', 'Entertainments'),
(658, 104, '2020-06-07', 'afternoon', 'Entertainments'),
(659, 106, '2020-06-07', 'afternoon', 'Entertainments'),
(660, 106, '2020-06-07', 'evening', 'Entertainments'),
(661, 108, '2020-06-07', 'evening', 'Entertainments');

-- --------------------------------------------------------

--
-- Table structure for table `stockupdate`
--

CREATE TABLE `stockupdate` (
  `id` int(11) NOT NULL,
  `productbarcode` int(11) NOT NULL,
  `updatereason` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `stockupdate`
--

INSERT INTO `stockupdate` (`id`, `productbarcode`, `updatereason`) VALUES
(3, 5, 'It fell and broke'),
(4, 5, 'New shipment'),
(5, 13, 'I want'),
(6, 1, 'New price'),
(7, 3, 'Shipment has arrived'),
(8, 12, 'Shipment has arrived'),
(9, 6, 'Shipment came in');

-- --------------------------------------------------------

--
-- Table structure for table `store`
--

CREATE TABLE `store` (
  `name` varchar(50) NOT NULL,
  `company` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `store`
--

INSERT INTO `store` (`name`, `company`) VALUES
('MediaBazaarEindhoven', 'MediaBazaar');

-- --------------------------------------------------------

--
-- Table structure for table `template`
--

CREATE TABLE `template` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `description` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `template`
--

INSERT INTO `template` (`id`, `name`, `description`) VALUES
(1, 'DefaultTemplate', 'This is the template used for normal weeks, without any holidays or special days');

-- --------------------------------------------------------

--
-- Table structure for table `templateshift`
--

CREATE TABLE `templateshift` (
  `id` int(11) NOT NULL,
  `department` text NOT NULL,
  `date` date DEFAULT NULL,
  `daysegment` text NOT NULL,
  `requiredemployees` int(11) NOT NULL,
  `templateid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `templateshift`
--

INSERT INTO `templateshift` (`id`, `department`, `date`, `daysegment`, `requiredemployees`, `templateid`) VALUES
(4, 'Manager', '2020-06-18', 'mondayMorning', 2, 1),
(5, 'Manager', '2020-06-17', 'mondayAfternoon', 2, 1),
(6, 'Manager', '2020-06-18', 'mondayEvening', 3, 1),
(7, 'Manager', '2020-06-18', 'tuesdayMorning', 2, 1),
(8, 'Manager', '2020-06-18', 'tuesdayAfternoon', 3, 1),
(9, 'Manager', '2020-06-18', 'tuesdayEvening', 3, 1),
(10, 'Manager', '2020-06-18', 'wednesdayMorning', 1, 1),
(11, 'Manager', '2020-06-18', 'wednesdayAfternoon', 2, 1),
(12, 'Manager', '2020-06-18', 'wednesdayEvening', 1, 1),
(13, 'Manager', '2020-06-18', 'thursdayMorning', 2, 1),
(14, 'Manager', '2020-06-18', 'thursdayAfternoon', 3, 1),
(15, 'Manager', '2020-06-18', 'thursdayEvening', 3, 1),
(16, 'Manager', '2020-06-18', 'fridayMorning', 2, 1),
(17, 'Manager', '2020-06-18', 'fridayAfternoon', 4, 1),
(18, 'Manager', '2020-06-18', 'fridayEvening', 2, 1),
(19, 'Manager', '2020-06-18', 'saturdayMorning', 2, 1),
(20, 'Manager', '2020-06-18', 'saturdayAfternoon', 4, 1),
(21, 'Manager', '2020-06-18', 'saturdayEvening', 4, 1),
(22, 'Manager', '2020-06-18', 'sundayMorning', 1, 1),
(23, 'Manager', '2020-06-18', 'sundayEvening', 1, 1),
(24, 'Manager', '2020-06-18', 'sundayAfternoon', 1, 1),
(25, 'Administration', '2020-06-18', 'mondayMorning', 2, 1),
(26, 'Administration', '2020-06-18', 'mondayAfternoon', 4, 1),
(28, 'Administration', '2020-06-18', 'mondayEvening', 5, 1),
(29, 'Administration', '2020-06-18', 'tuesdayMorning', 4, 1),
(30, 'Administration', '2020-06-18', 'tuesdayAfternoon', 2, 1),
(31, 'Administration', '2020-06-18', 'tuesdayEvening', 4, 1),
(32, 'Administration', '2020-06-18', 'wednesdayMorning', 2, 1),
(33, 'Administration', '2020-06-18', 'wednesdayAfternoon', 3, 1),
(34, 'Administration', '2020-06-18', 'wednesdayEvening', 2, 1),
(35, 'Administration', '2020-06-18', 'thursdayMorning', 2, 1),
(36, 'Administration', '2020-06-18', 'thursdayAfternoon', 3, 1),
(37, 'Administration', '2020-06-18', 'thursdayEvening', 2, 1),
(38, 'Administration', '2020-06-18', 'fridayMorning', 6, 1),
(39, 'Administration', '2020-06-18', 'fridayAfternoon', 2, 1),
(40, 'Administration', '2020-06-18', 'fridayEvening', 2, 1),
(41, 'Administration', '2020-06-18', 'saturdayMorning', 3, 1),
(42, 'Administration', '2020-06-18', 'saturdayAfternoon', 2, 1),
(43, 'Administration', '2020-06-18', 'saturdayEvening', 5, 1),
(44, 'Administration', '2020-06-18', 'sundayMorning', 2, 1),
(45, 'Administration', '2020-06-18', 'sundayAfternoon', 2, 1),
(46, 'Administration', '2020-06-18', 'sundayEvening', 2, 1),
(47, 'Depot worker', '2020-06-18', 'mondayMorning', 6, 1),
(48, 'Depot worker', '2020-06-18', 'mondayAfternoon', 6, 1),
(49, 'Depot worker', '2020-06-18', 'mondayEvening', 4, 1),
(50, 'Depot worker', '2020-06-18', 'tuesdayMorning', 5, 1),
(51, 'Depot worker', '2020-06-18', 'tuesdayAfternoon', 5, 1),
(52, 'Depot worker', '2020-06-18', 'tuesdayEvening', 3, 1),
(53, 'Depot worker', '2020-06-18', 'wednesdayMorning', 6, 1),
(54, 'Depot worker', '2020-06-18', 'wednesdayAfternoon', 4, 1),
(55, 'Depot worker', '2020-06-18', 'wednesdayEvening', 4, 1),
(56, 'Depot worker', '2020-06-18', 'thursdayMorning', 4, 1),
(57, 'Depot worker', '2020-06-18', 'thursdayAfternoon', 3, 1),
(59, 'Depot worker', '2020-06-18', 'thursdayEvening', 2, 1),
(60, 'Depot worker', '2020-06-18', 'fridayMorning', 4, 1),
(61, 'Depot worker', '2020-06-18', 'fridayAfternoon', 4, 1),
(62, 'Depot worker', '2020-06-18', 'fridayEvening', 4, 1),
(63, 'Depot worker', '2020-06-18', 'saturdayMorning', 5, 1),
(64, 'Depot worker', '2020-06-18', 'saturdayAfternoon', 4, 1),
(65, 'Depot worker', '2020-06-18', 'saturdayEvening', 3, 1),
(66, 'Depot worker', '2020-06-18', 'sundayMorning', 3, 1),
(67, 'Depot worker', '2020-06-18', 'sundayAfternoon', 3, 1),
(68, 'Depot worker', '2020-06-18', 'sundayEvening', 2, 1),
(69, 'Security', '2020-06-18', 'mondayMorning', 4, 1),
(70, 'Security', '2020-06-18', 'mondayAfternoon', 4, 1),
(71, 'Security', '2020-06-18', 'mondayEvening', 4, 1),
(72, 'Security', '2020-06-18', 'tuesdayMorning', 4, 1),
(73, 'Security', '2020-06-18', 'tuesdayAfternoon', 4, 1),
(74, 'Security', '2020-06-18', 'tuesdayEvening', 4, 1),
(75, 'Security', '2020-06-18', 'wednesdayMorning', 4, 1),
(76, 'Security', '2020-06-18', 'wednesdayAfternoon', 4, 1),
(77, 'Security', '2020-06-18', 'wednesdayEvening', 4, 1),
(78, 'Security', '2020-06-18', 'thursdayMorning', 4, 1),
(79, 'Security', '2020-06-18', 'thursdayAfternoon', 4, 1),
(80, 'Security', '2020-06-18', 'thursdayEvening', 4, 1),
(81, 'Security', '2020-06-18', 'fridayMorning', 4, 1),
(82, 'Security', '2020-06-18', 'fridayAfternoon', 4, 1),
(83, 'Security', '2020-06-18', 'fridayEvening', 4, 1),
(84, 'Security', '2020-06-18', 'saturdayMorning', 4, 1),
(85, 'Security', '2020-06-18', 'saturdayAfternoon', 4, 1),
(86, 'Security', '2020-06-18', 'saturdayEvening', 4, 1),
(87, 'Security', '2020-06-18', 'sundayMorning', 4, 1),
(88, 'Security', '2020-06-18', 'sundayAfternoon', 4, 1),
(89, 'Security', '2020-06-18', 'sundayEvening', 4, 1),
(90, 'Mobile devices', '2020-06-18', 'mondayMorning', 3, 1),
(91, 'Mobile devices', '2020-06-18', 'mondayAfternoon', 4, 1),
(92, 'Mobile devices', '2020-06-18', 'mondayEvening', 3, 1),
(93, 'Mobile devices', '2020-06-18', 'tuesdayMorning', 3, 1),
(94, 'Mobile devices', '2020-06-18', 'tuesdayAfternoon', 3, 1),
(95, 'Mobile devices', '2020-06-18', 'tuesdayEvening', 3, 1),
(96, 'Mobile devices', '2020-06-18', 'wednesdayMorning', 4, 1),
(97, 'Mobile devices', '2020-06-18', 'wednesdayAfternoon', 4, 1),
(98, 'Mobile devices', '2020-06-18', 'wednesdayEvening', 4, 1),
(99, 'Mobile devices', '2020-06-18', 'thursdayMorning', 2, 1),
(100, 'Mobile devices', '2020-06-18', 'thursdayAfternoon', 4, 1),
(101, 'Mobile devices', '2020-06-18', 'thursdayEvening', 5, 1),
(102, 'Mobile devices', '2020-06-18', 'fridayMorning', 2, 1),
(103, 'Mobile devices', '2020-06-18', 'fridayAfternoon', 3, 1),
(104, 'Mobile devices', '2020-06-18', 'fridayEvening', 4, 1),
(105, 'Mobile devices', '2020-06-18', 'saturdayMorning', 4, 1),
(106, 'Mobile devices', '2020-06-18', 'saturdayAfternoon', 4, 1),
(107, 'Mobile devices', '2020-06-18', 'saturdayEvening', 4, 1),
(108, 'Mobile devices', '2020-06-18', 'sundayMorning', 2, 1),
(109, 'Mobile devices', '2020-06-18', 'sundayAfternoon', 2, 1),
(110, 'Mobile devices', '2020-06-18', 'sundayEvening', 2, 1),
(111, 'Car accessories\r\n', '2020-06-18', 'mondayMorning', 2, 1),
(112, 'Car accessories\r\n', '2020-06-18', 'mondayAfternoon', 2, 1),
(113, 'Car accessories\r\n', '2020-06-18', 'mondayEvening', 2, 1),
(114, 'Car accessories\r\n', '2020-06-18', 'tuesdayMorning', 2, 1),
(115, 'Car accessories\r\n', '2020-06-18', 'tuesdayEvening', 2, 1),
(116, 'Car accessories\r\n', '2020-06-18', 'tuesdayAfternoon', 2, 1),
(117, 'Car accessories\r\n', '2020-06-18', 'wednesdayMorning', 3, 1),
(118, 'Car accessories\r\n', '2020-06-18', 'wednesdayAfternoon', 3, 1),
(119, 'Car accessories\r\n', '2020-06-18', 'wednesdayEvening', 2, 1),
(120, 'Car accessories\r\n', '2020-06-18', 'thursdayMorning', 3, 1),
(121, 'Car accessories\r\n', '2020-06-18', 'thursdayAfternoon', 3, 1),
(122, 'Car accessories\r\n', '2020-06-18', 'thursdayEvening', 2, 1),
(123, 'Car accessories\r\n', '2020-06-18', 'fridayMorning', 4, 1),
(124, 'Car accessories\r\n', '2020-06-18', 'fridayAfternoon', 4, 1),
(125, 'Car accessories\r\n', '2020-06-18', 'fridayEvening', 4, 1),
(126, 'Car accessories\r\n', '2020-06-18', 'saturdayMorning', 3, 1),
(127, 'Car accessories\r\n', '2020-06-18', 'saturdayAfternoon', 4, 1),
(128, 'Car accessories\r\n', '2020-06-18', 'saturdayEvening', 3, 1),
(129, 'Car accessories\r\n', '2020-06-18', 'sundayMorning', 2, 1),
(130, 'Car accessories\r\n', '2020-06-18', 'sundayAfternoon', 2, 1),
(131, 'Car accessories\r\n', '2020-06-18', 'sundayEvening', 2, 1),
(132, 'Home automation', '2020-06-18', 'mondayMorning', 2, 1),
(133, 'Home automation', '2020-06-18', 'mondayAfternoon', 3, 1),
(134, 'Home automation', '2020-06-18', 'mondayEvening', 2, 1),
(135, 'Home automation', '2020-06-18', 'tuesdayMorning', 4, 1),
(136, 'Home automation', '2020-06-18', 'tuesdayAfternoon', 4, 1),
(137, 'Home automation', '2020-06-18', 'tuesdayEvening', 4, 1),
(138, 'Home automation', '2020-06-18', 'wednesdayMorning', 3, 1),
(139, 'Home automation', '2020-06-18', 'wednesdayAfternoon', 3, 1),
(140, 'Home automation', '2020-06-18', 'wednesdayEvening', 3, 1),
(141, 'Home automation', '2020-06-18', 'thursdayMorning', 2, 1),
(142, 'Home automation', '2020-06-18', 'thursdayAfternoon', 2, 1),
(143, 'Home automation', '2020-06-18', 'thursdayEvening', 2, 1),
(144, 'Home automation', '2020-06-18', 'fridayMorning', 4, 1),
(145, 'Home automation', '2020-06-18', 'fridayAfternoon', 4, 1),
(146, 'Home automation', '2020-06-18', 'fridayEvening', 4, 1),
(147, 'Home automation', '2020-06-18', 'saturdayMorning', 3, 1),
(148, 'Home automation', '2020-06-18', 'saturdayAfternoon', 3, 1),
(149, 'Home automation', '2020-06-18', 'saturdayEvening', 3, 1),
(150, 'Home automation', '2020-06-18', 'sundayMorning', 2, 1),
(151, 'Home automation', '2020-06-18', 'sundayAfternoon', 2, 1),
(152, 'Home automation', '2020-06-18', 'sundayEvening', 2, 1),
(153, 'Entertainments', '2020-06-18', 'mondayMorning', 2, 1),
(154, 'Entertainments', '2020-06-18', 'mondayAfternoon', 2, 1),
(155, 'Entertainments', '2020-06-18', 'mondayEvening', 2, 1),
(156, 'Entertainments', '2020-06-18', 'tuesdayMorning', 3, 1),
(157, 'Entertainments', '2020-06-18', 'tuesdayAfternoon', 3, 1),
(158, 'Entertainments', '2020-06-18', 'tuesdayEvening', 3, 1),
(159, 'Entertainments', '2020-06-18', 'wednesdayMorning', 2, 1),
(160, 'Entertainments', '2020-06-18', 'wednesdayAfternoon', 2, 1),
(161, 'Entertainments', '2020-06-18', 'wednesdayEvening', 2, 1),
(162, 'Entertainments', '2020-06-18', 'thursdayMorning', 3, 1),
(163, 'Entertainments', '2020-06-18', 'thursdayAfternoon', 3, 1),
(164, 'Entertainments', '2020-06-18', 'thursdayEvening', 3, 1),
(165, 'Entertainments', '2020-06-18', 'fridayMorning', 3, 1),
(166, 'Entertainments', '2020-06-18', 'fridayAfternoon', 3, 1),
(167, 'Entertainments', '2020-06-18', 'fridayEvening', 3, 1),
(168, 'Entertainments', '2020-06-18', 'saturdayMorning', 4, 1),
(169, 'Entertainments', '2020-06-18', 'saturdayEvening', 4, 1),
(170, 'Entertainments', '2020-06-18', 'saturdayAfternoon', 4, 1),
(171, 'Entertainments', '2020-06-18', 'sundayMorning', 2, 1),
(172, 'Entertainments', '2020-06-18', 'sundayAfternoon', 2, 1),
(173, 'Entertainments', '2020-06-18', 'sundayEvening', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `temporaryshiftholder`
--

CREATE TABLE `temporaryshiftholder` (
  `id` int(11) NOT NULL,
  `requiredemployees` int(11) DEFAULT NULL,
  `employeeid` int(11) DEFAULT NULL,
  `date` date DEFAULT NULL,
  `daysegment` varchar(50) DEFAULT NULL,
  `separateSegment` varchar(50) DEFAULT NULL,
  `floor` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `temporaryshiftholder`
--

INSERT INTO `temporaryshiftholder` (`id`, `requiredemployees`, `employeeid`, `date`, `daysegment`, `separateSegment`, `floor`) VALUES
(1, 2, 71, '2020-06-29', 'mondayMorning', 'morning', 'Manager'),
(2, 2, 74, '2020-06-29', 'mondayMorning', 'morning', 'Manager'),
(3, 2, 71, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Manager'),
(4, 2, 73, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Manager'),
(5, 3, 71, '2020-06-29', 'mondayEvening', 'evening', 'Manager'),
(6, 3, 73, '2020-06-29', 'mondayEvening', 'evening', 'Manager'),
(7, 3, 74, '2020-06-29', 'mondayEvening', 'evening', 'Manager'),
(8, 2, 71, '2020-06-30', 'tuesdayMorning', 'morning', 'Manager'),
(9, 2, 72, '2020-06-30', 'tuesdayMorning', 'morning', 'Manager'),
(10, 3, 71, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Manager'),
(11, 3, 75, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Manager'),
(12, 3, 80, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Manager'),
(13, 3, 71, '2020-06-30', 'tuesdayEvening', 'evening', 'Manager'),
(14, 3, 72, '2020-06-30', 'tuesdayEvening', 'evening', 'Manager'),
(15, 3, 75, '2020-06-30', 'tuesdayEvening', 'evening', 'Manager'),
(16, 1, 71, '2020-07-01', 'wednesdayMorning', 'morning', 'Manager'),
(17, 2, 72, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Manager'),
(18, 2, 73, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Manager'),
(19, 1, 72, '2020-07-01', 'wednesdayEvening', 'evening', 'Manager'),
(20, 2, 61, '2020-07-02', 'thursdayMorning', 'morning', 'Manager'),
(21, 2, 71, '2020-07-02', 'thursdayMorning', 'morning', 'Manager'),
(22, 3, 61, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Manager'),
(23, 3, 71, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Manager'),
(24, 3, 72, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Manager'),
(25, 3, 61, '2020-07-02', 'thursdayEvening', 'evening', 'Manager'),
(26, 3, 71, '2020-07-02', 'thursdayEvening', 'evening', 'Manager'),
(27, 3, 72, '2020-07-02', 'thursdayEvening', 'evening', 'Manager'),
(28, 2, 61, '2020-07-03', 'fridayMorning', 'morning', 'Manager'),
(29, 2, 74, '2020-07-03', 'fridayMorning', 'morning', 'Manager'),
(30, 4, 61, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Manager'),
(31, 4, 73, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Manager'),
(32, 4, 74, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Manager'),
(33, 4, 75, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Manager'),
(34, 2, 61, '2020-07-03', 'fridayEvening', 'evening', 'Manager'),
(35, 2, 72, '2020-07-03', 'fridayEvening', 'evening', 'Manager'),
(36, 2, 71, '2020-07-04', 'saturdayMorning', 'morning', 'Manager'),
(37, 2, 72, '2020-07-04', 'saturdayMorning', 'morning', 'Manager'),
(38, 4, 61, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Manager'),
(39, 4, 71, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Manager'),
(40, 4, 72, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Manager'),
(41, 4, 73, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Manager'),
(42, 4, 71, '2020-07-04', 'saturdayEvening', 'evening', 'Manager'),
(43, 4, 72, '2020-07-04', 'saturdayEvening', 'evening', 'Manager'),
(44, 4, 73, '2020-07-04', 'saturdayEvening', 'evening', 'Manager'),
(45, 4, 74, '2020-07-04', 'saturdayEvening', 'evening', 'Manager'),
(46, 1, 61, '2020-07-05', 'sundayMorning', 'morning', 'Manager'),
(47, 1, 72, '2020-07-05', 'sundayEvening', 'evening', 'Manager'),
(48, 1, 61, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Manager'),
(49, 2, 64, '2020-06-29', 'mondayMorning', 'morning', 'Administration'),
(50, 2, 65, '2020-06-29', 'mondayMorning', 'morning', 'Administration'),
(51, 4, 64, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Administration'),
(52, 4, 65, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Administration'),
(53, 4, 67, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Administration'),
(54, 4, 69, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Administration'),
(55, 5, 64, '2020-06-29', 'mondayEvening', 'evening', 'Administration'),
(56, 5, 65, '2020-06-29', 'mondayEvening', 'evening', 'Administration'),
(57, 5, 67, '2020-06-29', 'mondayEvening', 'evening', 'Administration'),
(58, 5, 68, '2020-06-29', 'mondayEvening', 'evening', 'Administration'),
(59, 5, 69, '2020-06-29', 'mondayEvening', 'evening', 'Administration'),
(60, 4, 64, '2020-06-30', 'tuesdayMorning', 'morning', 'Administration'),
(61, 4, 65, '2020-06-30', 'tuesdayMorning', 'morning', 'Administration'),
(62, 4, 67, '2020-06-30', 'tuesdayMorning', 'morning', 'Administration'),
(63, 4, 68, '2020-06-30', 'tuesdayMorning', 'morning', 'Administration'),
(64, 2, 64, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Administration'),
(65, 2, 69, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Administration'),
(66, 4, 64, '2020-06-30', 'tuesdayEvening', 'evening', 'Administration'),
(67, 4, 65, '2020-06-30', 'tuesdayEvening', 'evening', 'Administration'),
(68, 4, 69, '2020-06-30', 'tuesdayEvening', 'evening', 'Administration'),
(69, 4, 81, '2020-06-30', 'tuesdayEvening', 'evening', 'Administration'),
(70, 2, 64, '2020-07-01', 'wednesdayMorning', 'morning', 'Administration'),
(71, 2, 65, '2020-07-01', 'wednesdayMorning', 'morning', 'Administration'),
(72, 3, 64, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Administration'),
(73, 3, 65, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Administration'),
(74, 3, 67, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Administration'),
(75, 2, 64, '2020-07-01', 'wednesdayEvening', 'evening', 'Administration'),
(76, 2, 67, '2020-07-01', 'wednesdayEvening', 'evening', 'Administration'),
(77, 2, 64, '2020-07-02', 'thursdayMorning', 'morning', 'Administration'),
(78, 2, 65, '2020-07-02', 'thursdayMorning', 'morning', 'Administration'),
(79, 3, 65, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Administration'),
(80, 3, 68, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Administration'),
(81, 3, 69, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Administration'),
(82, 2, 64, '2020-07-02', 'thursdayEvening', 'evening', 'Administration'),
(83, 2, 65, '2020-07-02', 'thursdayEvening', 'evening', 'Administration'),
(84, 6, 64, '2020-07-03', 'fridayMorning', 'morning', 'Administration'),
(85, 6, 66, '2020-07-03', 'fridayMorning', 'morning', 'Administration'),
(86, 6, 69, '2020-07-03', 'fridayMorning', 'morning', 'Administration'),
(87, 6, 70, '2020-07-03', 'fridayMorning', 'morning', 'Administration'),
(88, 2, 64, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Administration'),
(89, 2, 65, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Administration'),
(90, 2, 65, '2020-07-03', 'fridayEvening', 'evening', 'Administration'),
(91, 2, 66, '2020-07-03', 'fridayEvening', 'evening', 'Administration'),
(92, 3, 64, '2020-07-04', 'saturdayMorning', 'morning', 'Administration'),
(93, 3, 65, '2020-07-04', 'saturdayMorning', 'morning', 'Administration'),
(94, 3, 66, '2020-07-04', 'saturdayMorning', 'morning', 'Administration'),
(95, 2, 66, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Administration'),
(96, 2, 68, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Administration'),
(97, 5, 65, '2020-07-04', 'saturdayEvening', 'evening', 'Administration'),
(98, 5, 66, '2020-07-04', 'saturdayEvening', 'evening', 'Administration'),
(99, 5, 67, '2020-07-04', 'saturdayEvening', 'evening', 'Administration'),
(100, 5, 68, '2020-07-04', 'saturdayEvening', 'evening', 'Administration'),
(101, 5, 70, '2020-07-04', 'saturdayEvening', 'evening', 'Administration'),
(102, 2, 65, '2020-07-05', 'sundayMorning', 'morning', 'Administration'),
(103, 2, 67, '2020-07-05', 'sundayMorning', 'morning', 'Administration'),
(104, 2, 65, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Administration'),
(105, 2, 66, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Administration'),
(106, 2, 66, '2020-07-05', 'sundayEvening', 'evening', 'Administration'),
(107, 2, 68, '2020-07-05', 'sundayEvening', 'evening', 'Administration'),
(108, 6, 62, '2020-06-29', 'mondayMorning', 'morning', 'Depot worker'),
(109, 6, 77, '2020-06-29', 'mondayMorning', 'morning', 'Depot worker'),
(110, 6, 78, '2020-06-29', 'mondayMorning', 'morning', 'Depot worker'),
(111, 6, 79, '2020-06-29', 'mondayMorning', 'morning', 'Depot worker'),
(112, 6, 84, '2020-06-29', 'mondayMorning', 'morning', 'Depot worker'),
(113, 6, 86, '2020-06-29', 'mondayMorning', 'morning', 'Depot worker'),
(114, 6, 62, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Depot worker'),
(115, 6, 76, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Depot worker'),
(116, 6, 77, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Depot worker'),
(117, 6, 78, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Depot worker'),
(118, 6, 79, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Depot worker'),
(119, 6, 83, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Depot worker'),
(120, 4, 62, '2020-06-29', 'mondayEvening', 'evening', 'Depot worker'),
(121, 4, 76, '2020-06-29', 'mondayEvening', 'evening', 'Depot worker'),
(122, 4, 77, '2020-06-29', 'mondayEvening', 'evening', 'Depot worker'),
(123, 4, 78, '2020-06-29', 'mondayEvening', 'evening', 'Depot worker'),
(124, 5, 62, '2020-06-30', 'tuesdayMorning', 'morning', 'Depot worker'),
(125, 5, 76, '2020-06-30', 'tuesdayMorning', 'morning', 'Depot worker'),
(126, 5, 77, '2020-06-30', 'tuesdayMorning', 'morning', 'Depot worker'),
(127, 5, 78, '2020-06-30', 'tuesdayMorning', 'morning', 'Depot worker'),
(128, 5, 83, '2020-06-30', 'tuesdayMorning', 'morning', 'Depot worker'),
(129, 5, 62, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Depot worker'),
(130, 5, 76, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Depot worker'),
(131, 5, 77, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Depot worker'),
(132, 5, 86, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Depot worker'),
(133, 5, 88, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Depot worker'),
(134, 3, 62, '2020-06-30', 'tuesdayEvening', 'evening', 'Depot worker'),
(135, 3, 77, '2020-06-30', 'tuesdayEvening', 'evening', 'Depot worker'),
(136, 3, 83, '2020-06-30', 'tuesdayEvening', 'evening', 'Depot worker'),
(137, 6, 62, '2020-07-01', 'wednesdayMorning', 'morning', 'Depot worker'),
(138, 6, 76, '2020-07-01', 'wednesdayMorning', 'morning', 'Depot worker'),
(139, 6, 77, '2020-07-01', 'wednesdayMorning', 'morning', 'Depot worker'),
(140, 6, 79, '2020-07-01', 'wednesdayMorning', 'morning', 'Depot worker'),
(141, 6, 83, '2020-07-01', 'wednesdayMorning', 'morning', 'Depot worker'),
(142, 6, 84, '2020-07-01', 'wednesdayMorning', 'morning', 'Depot worker'),
(143, 4, 62, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Depot worker'),
(144, 4, 76, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Depot worker'),
(145, 4, 77, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Depot worker'),
(146, 4, 79, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Depot worker'),
(147, 4, 62, '2020-07-01', 'wednesdayEvening', 'evening', 'Depot worker'),
(148, 4, 76, '2020-07-01', 'wednesdayEvening', 'evening', 'Depot worker'),
(149, 4, 77, '2020-07-01', 'wednesdayEvening', 'evening', 'Depot worker'),
(150, 4, 78, '2020-07-01', 'wednesdayEvening', 'evening', 'Depot worker'),
(151, 4, 62, '2020-07-02', 'thursdayMorning', 'morning', 'Depot worker'),
(152, 4, 77, '2020-07-02', 'thursdayMorning', 'morning', 'Depot worker'),
(153, 4, 78, '2020-07-02', 'thursdayMorning', 'morning', 'Depot worker'),
(154, 4, 83, '2020-07-02', 'thursdayMorning', 'morning', 'Depot worker'),
(155, 3, 62, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Depot worker'),
(156, 3, 77, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Depot worker'),
(157, 3, 78, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Depot worker'),
(158, 2, 62, '2020-07-02', 'thursdayEvening', 'evening', 'Depot worker'),
(159, 2, 76, '2020-07-02', 'thursdayEvening', 'evening', 'Depot worker'),
(160, 4, 62, '2020-07-03', 'fridayMorning', 'morning', 'Depot worker'),
(161, 4, 76, '2020-07-03', 'fridayMorning', 'morning', 'Depot worker'),
(162, 4, 77, '2020-07-03', 'fridayMorning', 'morning', 'Depot worker'),
(163, 4, 78, '2020-07-03', 'fridayMorning', 'morning', 'Depot worker'),
(164, 4, 62, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Depot worker'),
(165, 4, 76, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Depot worker'),
(166, 4, 77, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Depot worker'),
(167, 4, 78, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Depot worker'),
(168, 4, 62, '2020-07-03', 'fridayEvening', 'evening', 'Depot worker'),
(169, 4, 76, '2020-07-03', 'fridayEvening', 'evening', 'Depot worker'),
(170, 4, 77, '2020-07-03', 'fridayEvening', 'evening', 'Depot worker'),
(172, 5, 62, '2020-07-04', 'saturdayMorning', 'morning', 'Depot worker'),
(173, 5, 76, '2020-07-04', 'saturdayMorning', 'morning', 'Depot worker'),
(174, 5, 77, '2020-07-04', 'saturdayMorning', 'morning', 'Depot worker'),
(175, 5, 79, '2020-07-04', 'saturdayMorning', 'morning', 'Depot worker'),
(176, 5, 83, '2020-07-04', 'saturdayMorning', 'morning', 'Depot worker'),
(177, 4, 62, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Depot worker'),
(178, 4, 76, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Depot worker'),
(179, 4, 77, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Depot worker'),
(180, 4, 79, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Depot worker'),
(181, 3, 62, '2020-07-04', 'saturdayEvening', 'evening', 'Depot worker'),
(182, 3, 76, '2020-07-04', 'saturdayEvening', 'evening', 'Depot worker'),
(183, 3, 77, '2020-07-04', 'saturdayEvening', 'evening', 'Depot worker'),
(184, 3, 62, '2020-07-05', 'sundayMorning', 'morning', 'Depot worker'),
(185, 3, 76, '2020-07-05', 'sundayMorning', 'morning', 'Depot worker'),
(186, 3, 77, '2020-07-05', 'sundayMorning', 'morning', 'Depot worker'),
(187, 3, 62, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Depot worker'),
(188, 3, 77, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Depot worker'),
(189, 3, 78, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Depot worker'),
(190, 2, 62, '2020-07-05', 'sundayEvening', 'evening', 'Depot worker'),
(191, 2, 77, '2020-07-05', 'sundayEvening', 'evening', 'Depot worker'),
(192, 4, 92, '2020-06-29', 'mondayMorning', 'morning', 'Security'),
(193, 4, 94, '2020-06-29', 'mondayMorning', 'morning', 'Security'),
(194, 4, 95, '2020-06-29', 'mondayMorning', 'morning', 'Security'),
(195, 4, 97, '2020-06-29', 'mondayMorning', 'morning', 'Security'),
(196, 4, 92, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Security'),
(197, 4, 94, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Security'),
(198, 4, 95, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Security'),
(199, 4, 97, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Security'),
(200, 4, 92, '2020-06-29', 'mondayEvening', 'evening', 'Security'),
(201, 4, 94, '2020-06-29', 'mondayEvening', 'evening', 'Security'),
(202, 4, 95, '2020-06-29', 'mondayEvening', 'evening', 'Security'),
(203, 4, 96, '2020-06-29', 'mondayEvening', 'evening', 'Security'),
(204, 4, 92, '2020-06-30', 'tuesdayMorning', 'morning', 'Security'),
(205, 4, 93, '2020-06-30', 'tuesdayMorning', 'morning', 'Security'),
(206, 4, 95, '2020-06-30', 'tuesdayMorning', 'morning', 'Security'),
(207, 4, 96, '2020-06-30', 'tuesdayMorning', 'morning', 'Security'),
(208, 4, 92, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Security'),
(209, 4, 93, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Security'),
(210, 4, 95, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Security'),
(211, 4, 96, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Security'),
(212, 4, 92, '2020-06-30', 'tuesdayEvening', 'evening', 'Security'),
(213, 4, 93, '2020-06-30', 'tuesdayEvening', 'evening', 'Security'),
(214, 4, 95, '2020-06-30', 'tuesdayEvening', 'evening', 'Security'),
(215, 4, 97, '2020-06-30', 'tuesdayEvening', 'evening', 'Security'),
(216, 4, 92, '2020-07-01', 'wednesdayMorning', 'morning', 'Security'),
(217, 4, 93, '2020-07-01', 'wednesdayMorning', 'morning', 'Security'),
(218, 4, 95, '2020-07-01', 'wednesdayMorning', 'morning', 'Security'),
(219, 4, 97, '2020-07-01', 'wednesdayMorning', 'morning', 'Security'),
(220, 4, 92, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Security'),
(221, 4, 93, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Security'),
(222, 4, 95, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Security'),
(223, 4, 96, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Security'),
(224, 4, 92, '2020-07-01', 'wednesdayEvening', 'evening', 'Security'),
(225, 4, 93, '2020-07-01', 'wednesdayEvening', 'evening', 'Security'),
(226, 4, 95, '2020-07-01', 'wednesdayEvening', 'evening', 'Security'),
(227, 4, 96, '2020-07-01', 'wednesdayEvening', 'evening', 'Security'),
(228, 4, 92, '2020-07-02', 'thursdayMorning', 'morning', 'Security'),
(229, 4, 93, '2020-07-02', 'thursdayMorning', 'morning', 'Security'),
(230, 4, 94, '2020-07-02', 'thursdayMorning', 'morning', 'Security'),
(231, 4, 95, '2020-07-02', 'thursdayMorning', 'morning', 'Security'),
(232, 4, 92, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Security'),
(233, 4, 93, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Security'),
(234, 4, 94, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Security'),
(235, 4, 95, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Security'),
(236, 4, 92, '2020-07-02', 'thursdayEvening', 'evening', 'Security'),
(237, 4, 93, '2020-07-02', 'thursdayEvening', 'evening', 'Security'),
(238, 4, 94, '2020-07-02', 'thursdayEvening', 'evening', 'Security'),
(239, 4, 95, '2020-07-02', 'thursdayEvening', 'evening', 'Security'),
(240, 4, 92, '2020-07-03', 'fridayMorning', 'morning', 'Security'),
(241, 4, 93, '2020-07-03', 'fridayMorning', 'morning', 'Security'),
(242, 4, 94, '2020-07-03', 'fridayMorning', 'morning', 'Security'),
(243, 4, 95, '2020-07-03', 'fridayMorning', 'morning', 'Security'),
(244, 4, 92, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Security'),
(245, 4, 93, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Security'),
(246, 4, 94, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Security'),
(247, 4, 95, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Security'),
(248, 4, 92, '2020-07-03', 'fridayEvening', 'evening', 'Security'),
(249, 4, 94, '2020-07-03', 'fridayEvening', 'evening', 'Security'),
(250, 4, 95, '2020-07-03', 'fridayEvening', 'evening', 'Security'),
(251, 4, 96, '2020-07-03', 'fridayEvening', 'evening', 'Security'),
(252, 4, 94, '2020-07-04', 'saturdayMorning', 'morning', 'Security'),
(253, 4, 95, '2020-07-04', 'saturdayMorning', 'morning', 'Security'),
(254, 4, 97, '2020-07-04', 'saturdayMorning', 'morning', 'Security'),
(255, 4, 99, '2020-07-04', 'saturdayMorning', 'morning', 'Security'),
(256, 4, 94, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Security'),
(257, 4, 96, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Security'),
(258, 4, 97, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Security'),
(259, 4, 98, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Security'),
(260, 4, 93, '2020-07-04', 'saturdayEvening', 'evening', 'Security'),
(261, 4, 94, '2020-07-04', 'saturdayEvening', 'evening', 'Security'),
(262, 4, 96, '2020-07-04', 'saturdayEvening', 'evening', 'Security'),
(263, 4, 97, '2020-07-04', 'saturdayEvening', 'evening', 'Security'),
(264, 4, 93, '2020-07-05', 'sundayMorning', 'morning', 'Security'),
(265, 4, 94, '2020-07-05', 'sundayMorning', 'morning', 'Security'),
(266, 4, 96, '2020-07-05', 'sundayMorning', 'morning', 'Security'),
(267, 4, 97, '2020-07-05', 'sundayMorning', 'morning', 'Security'),
(268, 4, 93, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Security'),
(269, 4, 94, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Security'),
(270, 4, 96, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Security'),
(271, 4, 97, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Security'),
(272, 4, 93, '2020-07-05', 'sundayEvening', 'evening', 'Security'),
(273, 4, 94, '2020-07-05', 'sundayEvening', 'evening', 'Security'),
(274, 4, 96, '2020-07-05', 'sundayEvening', 'evening', 'Security'),
(275, 4, 97, '2020-07-05', 'sundayEvening', 'evening', 'Security'),
(276, 3, 85, '2020-06-29', 'mondayMorning', 'morning', 'Mobile devices'),
(277, 3, 104, '2020-06-29', 'mondayMorning', 'morning', 'Mobile devices'),
(278, 3, 106, '2020-06-29', 'mondayMorning', 'morning', 'Mobile devices'),
(279, 4, 85, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Mobile devices'),
(280, 4, 104, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Mobile devices'),
(281, 4, 106, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Mobile devices'),
(282, 4, 108, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Mobile devices'),
(283, 3, 85, '2020-06-29', 'mondayEvening', 'evening', 'Mobile devices'),
(284, 3, 104, '2020-06-29', 'mondayEvening', 'evening', 'Mobile devices'),
(285, 3, 105, '2020-06-29', 'mondayEvening', 'evening', 'Mobile devices'),
(286, 3, 85, '2020-06-30', 'tuesdayMorning', 'morning', 'Mobile devices'),
(287, 3, 104, '2020-06-30', 'tuesdayMorning', 'morning', 'Mobile devices'),
(288, 3, 105, '2020-06-30', 'tuesdayMorning', 'morning', 'Mobile devices'),
(289, 3, 105, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Mobile devices'),
(290, 3, 109, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Mobile devices'),
(291, 3, 111, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Mobile devices'),
(292, 3, 85, '2020-06-30', 'tuesdayEvening', 'evening', 'Mobile devices'),
(293, 3, 104, '2020-06-30', 'tuesdayEvening', 'evening', 'Mobile devices'),
(294, 3, 105, '2020-06-30', 'tuesdayEvening', 'evening', 'Mobile devices'),
(295, 4, 85, '2020-07-01', 'wednesdayMorning', 'morning', 'Mobile devices'),
(296, 4, 104, '2020-07-01', 'wednesdayMorning', 'morning', 'Mobile devices'),
(297, 4, 105, '2020-07-01', 'wednesdayMorning', 'morning', 'Mobile devices'),
(298, 4, 106, '2020-07-01', 'wednesdayMorning', 'morning', 'Mobile devices'),
(299, 4, 85, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Mobile devices'),
(300, 4, 104, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Mobile devices'),
(301, 4, 105, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Mobile devices'),
(302, 4, 108, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Mobile devices'),
(303, 4, 105, '2020-07-01', 'wednesdayEvening', 'evening', 'Mobile devices'),
(304, 4, 109, '2020-07-01', 'wednesdayEvening', 'evening', 'Mobile devices'),
(305, 4, 111, '2020-07-01', 'wednesdayEvening', 'evening', 'Mobile devices'),
(306, 4, 112, '2020-07-01', 'wednesdayEvening', 'evening', 'Mobile devices'),
(307, 2, 104, '2020-07-02', 'thursdayMorning', 'morning', 'Mobile devices'),
(308, 2, 106, '2020-07-02', 'thursdayMorning', 'morning', 'Mobile devices'),
(309, 4, 85, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Mobile devices'),
(310, 4, 104, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Mobile devices'),
(311, 4, 105, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Mobile devices'),
(312, 4, 106, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Mobile devices'),
(313, 5, 85, '2020-07-02', 'thursdayEvening', 'evening', 'Mobile devices'),
(314, 5, 104, '2020-07-02', 'thursdayEvening', 'evening', 'Mobile devices'),
(315, 5, 105, '2020-07-02', 'thursdayEvening', 'evening', 'Mobile devices'),
(316, 5, 106, '2020-07-02', 'thursdayEvening', 'evening', 'Mobile devices'),
(317, 5, 108, '2020-07-02', 'thursdayEvening', 'evening', 'Mobile devices'),
(318, 2, 85, '2020-07-03', 'fridayMorning', 'morning', 'Mobile devices'),
(319, 2, 105, '2020-07-03', 'fridayMorning', 'morning', 'Mobile devices'),
(320, 3, 104, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Mobile devices'),
(321, 3, 109, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Mobile devices'),
(322, 3, 111, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Mobile devices'),
(323, 4, 104, '2020-07-03', 'fridayEvening', 'evening', 'Mobile devices'),
(324, 4, 105, '2020-07-03', 'fridayEvening', 'evening', 'Mobile devices'),
(325, 4, 106, '2020-07-03', 'fridayEvening', 'evening', 'Mobile devices'),
(326, 4, 108, '2020-07-03', 'fridayEvening', 'evening', 'Mobile devices'),
(327, 4, 85, '2020-07-04', 'saturdayMorning', 'morning', 'Mobile devices'),
(328, 4, 104, '2020-07-04', 'saturdayMorning', 'morning', 'Mobile devices'),
(329, 4, 105, '2020-07-04', 'saturdayMorning', 'morning', 'Mobile devices'),
(330, 4, 106, '2020-07-04', 'saturdayMorning', 'morning', 'Mobile devices'),
(331, 4, 85, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Mobile devices'),
(332, 4, 104, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Mobile devices'),
(333, 4, 105, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Mobile devices'),
(334, 4, 106, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Mobile devices'),
(335, 4, 85, '2020-07-04', 'saturdayEvening', 'evening', 'Mobile devices'),
(336, 4, 104, '2020-07-04', 'saturdayEvening', 'evening', 'Mobile devices'),
(337, 4, 105, '2020-07-04', 'saturdayEvening', 'evening', 'Mobile devices'),
(338, 4, 106, '2020-07-04', 'saturdayEvening', 'evening', 'Mobile devices'),
(339, 2, 85, '2020-07-05', 'sundayMorning', 'morning', 'Mobile devices'),
(340, 2, 104, '2020-07-05', 'sundayMorning', 'morning', 'Mobile devices'),
(341, 2, 104, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Mobile devices'),
(342, 2, 106, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Mobile devices'),
(343, 2, 106, '2020-07-05', 'sundayEvening', 'evening', 'Mobile devices'),
(344, 2, 108, '2020-07-05', 'sundayEvening', 'evening', 'Mobile devices'),
(345, 2, 85, '2020-06-29', 'mondayMorning', 'morning', 'Home automation'),
(346, 2, 104, '2020-06-29', 'mondayMorning', 'morning', 'Home automation'),
(347, 3, 85, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Home automation'),
(348, 3, 104, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Home automation'),
(349, 3, 106, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Home automation'),
(350, 2, 85, '2020-06-29', 'mondayEvening', 'evening', 'Home automation'),
(351, 2, 104, '2020-06-29', 'mondayEvening', 'evening', 'Home automation'),
(352, 4, 85, '2020-06-30', 'tuesdayMorning', 'morning', 'Home automation'),
(353, 4, 104, '2020-06-30', 'tuesdayMorning', 'morning', 'Home automation'),
(354, 4, 105, '2020-06-30', 'tuesdayMorning', 'morning', 'Home automation'),
(355, 4, 106, '2020-06-30', 'tuesdayMorning', 'morning', 'Home automation'),
(356, 4, 105, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Home automation'),
(357, 4, 109, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Home automation'),
(358, 4, 111, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Home automation'),
(359, 4, 112, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Home automation'),
(360, 4, 85, '2020-06-30', 'tuesdayEvening', 'evening', 'Home automation'),
(361, 4, 104, '2020-06-30', 'tuesdayEvening', 'evening', 'Home automation'),
(362, 4, 105, '2020-06-30', 'tuesdayEvening', 'evening', 'Home automation'),
(363, 4, 106, '2020-06-30', 'tuesdayEvening', 'evening', 'Home automation'),
(364, 3, 85, '2020-07-01', 'wednesdayMorning', 'morning', 'Home automation'),
(365, 3, 104, '2020-07-01', 'wednesdayMorning', 'morning', 'Home automation'),
(366, 3, 105, '2020-07-01', 'wednesdayMorning', 'morning', 'Home automation'),
(367, 3, 85, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Home automation'),
(368, 3, 104, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Home automation'),
(369, 3, 105, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Home automation'),
(370, 3, 105, '2020-07-01', 'wednesdayEvening', 'evening', 'Home automation'),
(371, 3, 109, '2020-07-01', 'wednesdayEvening', 'evening', 'Home automation'),
(372, 3, 111, '2020-07-01', 'wednesdayEvening', 'evening', 'Home automation'),
(373, 2, 104, '2020-07-02', 'thursdayMorning', 'morning', 'Home automation'),
(374, 2, 106, '2020-07-02', 'thursdayMorning', 'morning', 'Home automation'),
(375, 2, 85, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Home automation'),
(376, 2, 104, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Home automation'),
(377, 2, 85, '2020-07-02', 'thursdayEvening', 'evening', 'Home automation'),
(378, 2, 104, '2020-07-02', 'thursdayEvening', 'evening', 'Home automation'),
(379, 4, 85, '2020-07-03', 'fridayMorning', 'morning', 'Home automation'),
(380, 4, 105, '2020-07-03', 'fridayMorning', 'morning', 'Home automation'),
(381, 4, 108, '2020-07-03', 'fridayMorning', 'morning', 'Home automation'),
(382, 4, 109, '2020-07-03', 'fridayMorning', 'morning', 'Home automation'),
(383, 4, 104, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Home automation'),
(384, 4, 109, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Home automation'),
(385, 4, 111, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Home automation'),
(386, 4, 112, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Home automation'),
(387, 4, 104, '2020-07-03', 'fridayEvening', 'evening', 'Home automation'),
(388, 4, 105, '2020-07-03', 'fridayEvening', 'evening', 'Home automation'),
(389, 4, 106, '2020-07-03', 'fridayEvening', 'evening', 'Home automation'),
(390, 4, 108, '2020-07-03', 'fridayEvening', 'evening', 'Home automation'),
(391, 3, 85, '2020-07-04', 'saturdayMorning', 'morning', 'Home automation'),
(392, 3, 104, '2020-07-04', 'saturdayMorning', 'morning', 'Home automation'),
(393, 3, 105, '2020-07-04', 'saturdayMorning', 'morning', 'Home automation'),
(394, 3, 85, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Home automation'),
(395, 3, 104, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Home automation'),
(396, 3, 105, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Home automation'),
(397, 3, 85, '2020-07-04', 'saturdayEvening', 'evening', 'Home automation'),
(398, 3, 104, '2020-07-04', 'saturdayEvening', 'evening', 'Home automation'),
(399, 3, 105, '2020-07-04', 'saturdayEvening', 'evening', 'Home automation'),
(400, 2, 85, '2020-07-05', 'sundayMorning', 'morning', 'Home automation'),
(401, 2, 104, '2020-07-05', 'sundayMorning', 'morning', 'Home automation'),
(402, 2, 104, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Home automation'),
(403, 2, 106, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Home automation'),
(404, 2, 106, '2020-07-05', 'sundayEvening', 'evening', 'Home automation'),
(405, 2, 108, '2020-07-05', 'sundayEvening', 'evening', 'Home automation'),
(406, 2, 85, '2020-06-29', 'mondayMorning', 'morning', 'Entertainments'),
(407, 2, 104, '2020-06-29', 'mondayMorning', 'morning', 'Entertainments'),
(408, 2, 85, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Entertainments'),
(409, 2, 104, '2020-06-29', 'mondayAfternoon', 'afternoon', 'Entertainments'),
(410, 2, 85, '2020-06-29', 'mondayEvening', 'evening', 'Entertainments'),
(411, 2, 104, '2020-06-29', 'mondayEvening', 'evening', 'Entertainments'),
(412, 3, 85, '2020-06-30', 'tuesdayMorning', 'morning', 'Entertainments'),
(413, 3, 104, '2020-06-30', 'tuesdayMorning', 'morning', 'Entertainments'),
(414, 3, 105, '2020-06-30', 'tuesdayMorning', 'morning', 'Entertainments'),
(415, 3, 105, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Entertainments'),
(416, 3, 109, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Entertainments'),
(417, 3, 111, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Entertainments'),
(418, 3, 85, '2020-06-30', 'tuesdayEvening', 'evening', 'Entertainments'),
(419, 3, 104, '2020-06-30', 'tuesdayEvening', 'evening', 'Entertainments'),
(420, 3, 105, '2020-06-30', 'tuesdayEvening', 'evening', 'Entertainments'),
(421, 2, 85, '2020-07-01', 'wednesdayMorning', 'morning', 'Entertainments'),
(422, 2, 104, '2020-07-01', 'wednesdayMorning', 'morning', 'Entertainments'),
(423, 2, 85, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Entertainments'),
(424, 2, 104, '2020-07-01', 'wednesdayAfternoon', 'afternoon', 'Entertainments'),
(425, 2, 105, '2020-07-01', 'wednesdayEvening', 'evening', 'Entertainments'),
(426, 2, 109, '2020-07-01', 'wednesdayEvening', 'evening', 'Entertainments'),
(427, 3, 104, '2020-07-02', 'thursdayMorning', 'morning', 'Entertainments'),
(428, 3, 106, '2020-07-02', 'thursdayMorning', 'morning', 'Entertainments'),
(429, 3, 109, '2020-07-02', 'thursdayMorning', 'morning', 'Entertainments'),
(430, 3, 85, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Entertainments'),
(431, 3, 104, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Entertainments'),
(432, 3, 105, '2020-07-02', 'thursdayAfternoon', 'afternoon', 'Entertainments'),
(433, 3, 85, '2020-07-02', 'thursdayEvening', 'evening', 'Entertainments'),
(434, 3, 104, '2020-07-02', 'thursdayEvening', 'evening', 'Entertainments'),
(435, 3, 105, '2020-07-02', 'thursdayEvening', 'evening', 'Entertainments'),
(436, 3, 85, '2020-07-03', 'fridayMorning', 'morning', 'Entertainments'),
(437, 3, 105, '2020-07-03', 'fridayMorning', 'morning', 'Entertainments'),
(438, 3, 108, '2020-07-03', 'fridayMorning', 'morning', 'Entertainments'),
(439, 3, 104, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Entertainments'),
(440, 3, 109, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Entertainments'),
(441, 3, 111, '2020-07-03', 'fridayAfternoon', 'afternoon', 'Entertainments'),
(442, 3, 104, '2020-07-03', 'fridayEvening', 'evening', 'Entertainments'),
(443, 3, 105, '2020-07-03', 'fridayEvening', 'evening', 'Entertainments'),
(444, 3, 106, '2020-07-03', 'fridayEvening', 'evening', 'Entertainments'),
(445, 4, 85, '2020-07-04', 'saturdayMorning', 'morning', 'Entertainments'),
(446, 4, 104, '2020-07-04', 'saturdayMorning', 'morning', 'Entertainments'),
(447, 4, 105, '2020-07-04', 'saturdayMorning', 'morning', 'Entertainments'),
(448, 4, 106, '2020-07-04', 'saturdayMorning', 'morning', 'Entertainments'),
(449, 4, 85, '2020-07-04', 'saturdayEvening', 'evening', 'Entertainments'),
(450, 4, 104, '2020-07-04', 'saturdayEvening', 'evening', 'Entertainments'),
(451, 4, 105, '2020-07-04', 'saturdayEvening', 'evening', 'Entertainments'),
(452, 4, 106, '2020-07-04', 'saturdayEvening', 'evening', 'Entertainments'),
(453, 4, 85, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Entertainments'),
(454, 4, 104, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Entertainments'),
(455, 4, 105, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Entertainments'),
(456, 4, 106, '2020-07-04', 'saturdayAfternoon', 'afternoon', 'Entertainments'),
(457, 2, 85, '2020-07-05', 'sundayMorning', 'morning', 'Entertainments'),
(458, 2, 104, '2020-07-05', 'sundayMorning', 'morning', 'Entertainments'),
(459, 2, 104, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Entertainments'),
(460, 2, 106, '2020-07-05', 'sundayAfternoon', 'afternoon', 'Entertainments'),
(461, 2, 106, '2020-07-05', 'sundayEvening', 'evening', 'Entertainments'),
(462, 2, 108, '2020-07-05', 'sundayEvening', 'evening', 'Entertainments'),
(463, 0, 86, '2020-07-04', 'saturdayEvening', 'evening', 'Depot worker'),
(464, 0, 90, '2020-06-30', 'tuesdayAfternoon', 'afternoon', 'Depot worker');

-- --------------------------------------------------------

--
-- Table structure for table `vacationday`
--

CREATE TABLE `vacationday` (
  `id` int(11) NOT NULL,
  `employeeid` int(11) NOT NULL,
  `vacationdate` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `vacationday`
--

INSERT INTO `vacationday` (`id`, `employeeid`, `vacationdate`) VALUES
(1, 64, '2020-06-19'),
(2, 64, '2020-06-20'),
(3, 64, '2020-06-03'),
(4, 64, '2020-06-04'),
(5, 64, '2020-06-05'),
(6, 64, '2020-06-06'),
(7, 64, '2020-06-23'),
(8, 64, '2020-06-24'),
(9, 64, '2020-06-25'),
(10, 64, '2020-06-26'),
(11, 64, '2020-06-09'),
(12, 64, '2020-06-10'),
(13, 64, '2020-06-11'),
(14, 64, '2020-06-22'),
(15, 64, '2020-07-20'),
(16, 64, '2020-07-21'),
(17, 64, '2020-07-22'),
(18, 64, '2020-09-02'),
(19, 64, '2020-09-03'),
(20, 64, '2020-09-04'),
(21, 64, '2020-05-01'),
(22, 64, '2020-05-02'),
(23, 61, '2020-06-22'),
(24, 61, '2020-06-22'),
(25, 61, '2020-06-23'),
(26, 61, '2020-06-24'),
(27, 61, '2020-06-24'),
(28, 61, '2020-06-25'),
(29, 61, '2020-06-22'),
(30, 61, '2020-06-26'),
(31, 61, '2020-06-27'),
(32, 61, '2020-06-29'),
(33, 61, '2020-06-29'),
(34, 61, '2020-06-30'),
(35, 61, '2020-07-01'),
(36, 61, '2020-06-22'),
(37, 64, '2020-08-21'),
(38, 64, '2020-08-21'),
(39, 64, '2020-06-22'),
(92, 61, '2020-06-22'),
(93, 61, '2020-06-23'),
(94, 66, '2020-06-26'),
(95, 66, '2020-06-27'),
(96, 66, '2020-06-28'),
(97, 66, '2020-06-29'),
(98, 66, '2020-06-30'),
(99, 66, '2020-07-01'),
(100, 66, '2020-08-10'),
(101, 66, '2020-08-11'),
(102, 66, '2020-08-12'),
(103, 66, '2020-08-13'),
(104, 66, '2020-08-14'),
(105, 66, '2020-08-15'),
(106, 66, '2020-08-16'),
(107, 66, '2020-06-29'),
(108, 66, '2020-06-30'),
(109, 66, '2020-07-01'),
(110, 66, '2020-07-02'),
(111, 66, '2020-07-03'),
(112, 66, '2020-07-04'),
(113, 66, '2020-07-05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `availability`
--
ALTER TABLE `availability`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `employeeid` (`employeeid`);

--
-- Indexes for table `contract`
--
ALTER TABLE `contract`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_CONTRACTEMPLOYEE` (`employeeid`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `employee`
--
ALTER TABLE `employee`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_EMPLOYEESTORE` (`store`);

--
-- Indexes for table `product`
--
ALTER TABLE `product`
  ADD PRIMARY KEY (`barcode`),
  ADD KEY `FK_PRODUCTSTORE` (`store`);

--
-- Indexes for table `restockrequest`
--
ALTER TABLE `restockrequest`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_RESTOCKREQUESTSTORE` (`store`),
  ADD KEY `FK_RESTOCKREQUESTPRODUCT` (`productbarcode`);

--
-- Indexes for table `shift`
--
ALTER TABLE `shift`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_SHIFTEMPLOYEE` (`employeeid`);

--
-- Indexes for table `stockupdate`
--
ALTER TABLE `stockupdate`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_STOCKUPDATEPRODUCT` (`productbarcode`);

--
-- Indexes for table `store`
--
ALTER TABLE `store`
  ADD PRIMARY KEY (`name`),
  ADD KEY `company` (`company`) USING BTREE;

--
-- Indexes for table `template`
--
ALTER TABLE `template`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `templateshift`
--
ALTER TABLE `templateshift`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_TEMPLATESHIFTSHIFT` (`templateid`);

--
-- Indexes for table `temporaryshiftholder`
--
ALTER TABLE `temporaryshiftholder`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `vacationday`
--
ALTER TABLE `vacationday`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK_SICKLEAVEEMPLOYEE` (`employeeid`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `availability`
--
ALTER TABLE `availability`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `contract`
--
ALTER TABLE `contract`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=241;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `employee`
--
ALTER TABLE `employee`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=130;

--
-- AUTO_INCREMENT for table `product`
--
ALTER TABLE `product`
  MODIFY `barcode` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `restockrequest`
--
ALTER TABLE `restockrequest`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=98;

--
-- AUTO_INCREMENT for table `shift`
--
ALTER TABLE `shift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=713;

--
-- AUTO_INCREMENT for table `stockupdate`
--
ALTER TABLE `stockupdate`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `template`
--
ALTER TABLE `template`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `templateshift`
--
ALTER TABLE `templateshift`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=174;

--
-- AUTO_INCREMENT for table `temporaryshiftholder`
--
ALTER TABLE `temporaryshiftholder`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=465;

--
-- AUTO_INCREMENT for table `vacationday`
--
ALTER TABLE `vacationday`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=114;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `availability`
--
ALTER TABLE `availability`
  ADD CONSTRAINT `FK_AVAILABILITYEMPLOYEE` FOREIGN KEY (`employeeid`) REFERENCES `employee` (`id`);

--
-- Constraints for table `contract`
--
ALTER TABLE `contract`
  ADD CONSTRAINT `FK_CONTRACTEMPLOYEE` FOREIGN KEY (`employeeid`) REFERENCES `employee` (`id`);

--
-- Constraints for table `employee`
--
ALTER TABLE `employee`
  ADD CONSTRAINT `FK_EMPLOYEESTORE` FOREIGN KEY (`store`) REFERENCES `store` (`name`);

--
-- Constraints for table `product`
--
ALTER TABLE `product`
  ADD CONSTRAINT `FK_PRODUCTSTORE` FOREIGN KEY (`store`) REFERENCES `store` (`name`);

--
-- Constraints for table `restockrequest`
--
ALTER TABLE `restockrequest`
  ADD CONSTRAINT `FK_RESTOCKREQUESTPRODUCT` FOREIGN KEY (`productbarcode`) REFERENCES `product` (`barcode`),
  ADD CONSTRAINT `FK_RESTOCKREQUESTSTORE` FOREIGN KEY (`store`) REFERENCES `store` (`name`);

--
-- Constraints for table `shift`
--
ALTER TABLE `shift`
  ADD CONSTRAINT `FK_SHIFTEMPLOYEE` FOREIGN KEY (`employeeid`) REFERENCES `employee` (`id`);

--
-- Constraints for table `stockupdate`
--
ALTER TABLE `stockupdate`
  ADD CONSTRAINT `FK_STOCKUPDATEPRODUCT` FOREIGN KEY (`productbarcode`) REFERENCES `product` (`barcode`);

--
-- Constraints for table `templateshift`
--
ALTER TABLE `templateshift`
  ADD CONSTRAINT `FK_TEMPLATESHIFTSHIFT` FOREIGN KEY (`templateid`) REFERENCES `template` (`id`);

--
-- Constraints for table `vacationday`
--
ALTER TABLE `vacationday`
  ADD CONSTRAINT `FK_SICKLEAVEEMPLOYEE` FOREIGN KEY (`employeeid`) REFERENCES `employee` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
