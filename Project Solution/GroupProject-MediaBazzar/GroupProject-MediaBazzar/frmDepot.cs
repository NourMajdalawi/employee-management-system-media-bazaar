﻿using Classes;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroupProject_MediaBazzar
{

    public partial class frmDepot : Form
    {
        Store store;

        DataHelper dh = new DataHelper();
        public frmDepot(string loggedInAs, Store store)
        {
            InitializeComponent();
            lbLoggedInAs.Text += loggedInAs;
            this.store = store;
            //make sure when the application is opened, both of the datagridviews will show all products and restock requests
            showallproducts();
            showallproductsoverview();
            ShowRestockRequests();
        }


        //Remove restock requests that have been handled
        private void btnConfirm_Click(object sender, EventArgs e)
        {
            try
            {
                int selectedRowIndex = dgvRequests.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dgvRequests.Rows[selectedRowIndex];
                int id = Convert.ToInt32(selectedRow.Cells["id"].Value);
                store.ConfirmRestockRequest(id);
                MessageBox.Show("Confirmation completed succesfully");
                ShowRestockRequests();
            }
            catch
            {
                MessageBox.Show("Please select a row to confirm");
            }
        }

        //search on products and filter on the corresponding input of the user, only show these results into the datagridview
        private void BtnOverview_Click(object sender, EventArgs e)
        {
                showallproductsoverview();
        }

        //search button to filter on a specific item, and show only results corresponding to that item
        private void btnSearchItem_Click_1(object sender, EventArgs e)
        {
                showallproducts();
        }
        //Modify the stock value of a product and save the reason in order to keep track of the history. 
        private void btnModify_Click(object sender, EventArgs e)
        {
            try
            {
                //make sure the value is updated in the database and that the two datagridviews are updated according to the new stock value
                int selectedRowIndex = dgvFoundItems.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dgvFoundItems.Rows[selectedRowIndex];
                int barcode = Convert.ToInt32(selectedRow.Cells["barcode"].Value);
                int amount = Convert.ToInt32(tbAmountToChange.Text);

                int productBarcode = barcode;
                string reason = tbReasonDepotworker.Text;

                if (reason == "")
                {
                    MessageBox.Show("Please insert reason for modification");
                }
                else
                {
                    store.ModifyStockAmount(barcode, amount);
                    showallproducts();
                    showallproductsoverview();

                    //save the reason for the value change into the database
                    StockChange stockChange = new StockChange(productBarcode, reason);
                    if (store.AddStockChange(stockChange))
                    {
                        MessageBox.Show("Modification done succesfully");
                        tbAmountToChange.Clear();
                        tbReasonDepotworker.Clear();
                    }
                    else
                    {
                        MessageBox.Show("Failed to modify stock value");
                    }
                }
            }
            catch
            {
                MessageBox.Show("Please fill in amount to modify");
            }

        }
        // show all restock requests
        private void ShowRestockRequests()
        {
            dgvRequests.Rows.Clear();
            List<RestockRequest> restockRequests = store.GetAllrestockrequests();

            foreach (RestockRequest restockRequest in restockRequests)
            {
                dgvRequests.Rows.Add(restockRequest.id, restockRequest.productBarcode, restockRequest.productAmount, restockRequest.floor, restockRequest.date);
            }

            dgvRequests.Sort(dgvRequests.Columns[4], ListSortDirection.Ascending);
        }

        //Show all the products in the modify tab
        private void showallproducts()
        {
            dgvFoundItems.Rows.Clear();
            string input = tbItemModSearch.Text.ToLower();
            List<Product> products = store.GetProducts(input);
            foreach (Product product in products)
            {
                dgvFoundItems.Rows.Add(product.barcode, product.productName, product.modelNumber, product.manufacturerBrand, product.category, product.normalPrice, product.promotionPrice, product.stockInDepot, product.stockOnShelves, product.amountsold);
            }
        }
        //Show all the products in the overview tab
        private void showallproductsoverview()
        {
            dataGridView2.Rows.Clear();
            string input = tbInfo.Text.ToLower();
            List<Product> products = store.GetProducts(input);
            foreach (Product product in products)
            {
                dataGridView2.Rows.Add(product.barcode, product.productName, product.modelNumber, product.manufacturerBrand, product.category, product.normalPrice, product.promotionPrice, product.stockInDepot, product.stockOnShelves, product.amountsold);
            }
        }
        //The logout button on the top will make sure you are logged out and sent to login form.
        private void btnLogOut_Click(object sender, EventArgs e)
        {
            frmLogin loginform = new frmLogin();
            loginform.Show();
            Hide();
        }

      
    }
}
