﻿namespace GroupProject_MediaBazzar
{
    partial class DepotWorker
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DepotWorker));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.ManagingStockPage = new MetroFramework.Controls.MetroTabPage();
            this.RestockRequestPage = new MetroFramework.Controls.MetroTabControl();
            this.metroTabPage1 = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.lbRestockRequestes = new System.Windows.Forms.ListBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.metroTabPage2 = new MetroFramework.Controls.MetroTabPage();
            this.tbReasonDepotworker = new MetroFramework.Controls.MetroTextBox();
            this.metroTextBox1 = new MetroFramework.Controls.MetroTextBox();
            this.btnModify = new System.Windows.Forms.Button();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.metroLabel3 = new MetroFramework.Controls.MetroLabel();
            this.tbItemIdDepotWorker = new MetroFramework.Controls.MetroTextBox();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.btnSearchItem = new System.Windows.Forms.Button();
            this.lblSearchforAnItem = new MetroFramework.Controls.MetroLabel();
            this.OverviewPage = new MetroFramework.Controls.MetroTabPage();
            this.metroLabel7 = new MetroFramework.Controls.MetroLabel();
            this.btnOverviewInDepot = new System.Windows.Forms.Button();
            this.rbBrand = new MetroFramework.Controls.MetroRadioButton();
            this.rbId = new MetroFramework.Controls.MetroRadioButton();
            this.rbName = new MetroFramework.Controls.MetroRadioButton();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.tbInfo = new MetroFramework.Controls.MetroTextBox();
            this.lblInfo = new MetroFramework.Controls.MetroLabel();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.metroTabControl1.SuspendLayout();
            this.ManagingStockPage.SuspendLayout();
            this.RestockRequestPage.SuspendLayout();
            this.metroTabPage1.SuspendLayout();
            this.metroTabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.OverviewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.btnLogOut);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1266, 118);
            this.panel1.TabIndex = 5;
            // 
            // btnLogOut
            // 
            this.btnLogOut.BackColor = System.Drawing.Color.Black;
            this.btnLogOut.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOut.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnLogOut.Location = new System.Drawing.Point(1168, 11);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(95, 52);
            this.btnLogOut.TabIndex = 1;
            this.btnLogOut.Text = "Log Out";
            this.btnLogOut.UseVisualStyleBackColor = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(330, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(253, 40);
            this.label1.TabIndex = 4;
            this.label1.Text = "Logged in as a depot worker:\r\n\r\n";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(203, 120);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.ManagingStockPage);
            this.metroTabControl1.Controls.Add(this.OverviewPage);
            this.metroTabControl1.CustomBackground = true;
            this.metroTabControl1.ItemSize = new System.Drawing.Size(280, 55);
            this.metroTabControl1.Location = new System.Drawing.Point(8, 122);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 1;
            this.metroTabControl1.Size = new System.Drawing.Size(1255, 552);
            this.metroTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTabControl1.TabIndex = 6;
            // 
            // ManagingStockPage
            // 
            this.ManagingStockPage.Controls.Add(this.RestockRequestPage);
            this.ManagingStockPage.HorizontalScrollbarBarColor = true;
            this.ManagingStockPage.Location = new System.Drawing.Point(4, 59);
            this.ManagingStockPage.Name = "ManagingStockPage";
            this.ManagingStockPage.Size = new System.Drawing.Size(1247, 489);
            this.ManagingStockPage.TabIndex = 0;
            this.ManagingStockPage.Text = "Manage Stock";
            this.ManagingStockPage.VerticalScrollbarBarColor = true;
            // 
            // RestockRequestPage
            // 
            this.RestockRequestPage.Controls.Add(this.metroTabPage1);
            this.RestockRequestPage.Controls.Add(this.metroTabPage2);
            this.RestockRequestPage.CustomBackground = true;
            this.RestockRequestPage.Location = new System.Drawing.Point(0, 0);
            this.RestockRequestPage.Name = "RestockRequestPage";
            this.RestockRequestPage.SelectedIndex = 1;
            this.RestockRequestPage.Size = new System.Drawing.Size(1242, 486);
            this.RestockRequestPage.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.RestockRequestPage.Style = MetroFramework.MetroColorStyle.Teal;
            this.RestockRequestPage.TabIndex = 81;
            // 
            // metroTabPage1
            // 
            this.metroTabPage1.Controls.Add(this.metroLabel2);
            this.metroTabPage1.Controls.Add(this.btnConfirm);
            this.metroTabPage1.Controls.Add(this.lbRestockRequestes);
            this.metroTabPage1.Controls.Add(this.metroLabel1);
            this.metroTabPage1.HorizontalScrollbarBarColor = true;
            this.metroTabPage1.Location = new System.Drawing.Point(4, 39);
            this.metroTabPage1.Name = "metroTabPage1";
            this.metroTabPage1.Size = new System.Drawing.Size(1234, 443);
            this.metroTabPage1.TabIndex = 0;
            this.metroTabPage1.Text = "Restock requests";
            this.metroTabPage1.VerticalScrollbarBarColor = true;
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(73, 23);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(332, 20);
            this.metroLabel2.TabIndex = 84;
            this.metroLabel2.Text = "Recieving restock requests from the manager:";
            // 
            // btnConfirm
            // 
            this.btnConfirm.BackColor = System.Drawing.Color.DarkCyan;
            this.btnConfirm.Location = new System.Drawing.Point(774, 375);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(136, 45);
            this.btnConfirm.TabIndex = 83;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = false;
            // 
            // lbRestockRequestes
            // 
            this.lbRestockRequestes.FormattingEnabled = true;
            this.lbRestockRequestes.ItemHeight = 16;
            this.lbRestockRequestes.Location = new System.Drawing.Point(73, 64);
            this.lbRestockRequestes.Name = "lbRestockRequestes";
            this.lbRestockRequestes.Size = new System.Drawing.Size(655, 356);
            this.lbRestockRequestes.TabIndex = 82;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(264, -49);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(140, 20);
            this.metroLabel1.TabIndex = 81;
            this.metroLabel1.Text = "Restock requestes:";
            // 
            // metroTabPage2
            // 
            this.metroTabPage2.Controls.Add(this.tbReasonDepotworker);
            this.metroTabPage2.Controls.Add(this.metroTextBox1);
            this.metroTabPage2.Controls.Add(this.btnModify);
            this.metroTabPage2.Controls.Add(this.metroLabel5);
            this.metroTabPage2.Controls.Add(this.metroLabel4);
            this.metroTabPage2.Controls.Add(this.metroLabel3);
            this.metroTabPage2.Controls.Add(this.tbItemIdDepotWorker);
            this.metroTabPage2.Controls.Add(this.dataGridView1);
            this.metroTabPage2.Controls.Add(this.btnSearchItem);
            this.metroTabPage2.Controls.Add(this.lblSearchforAnItem);
            this.metroTabPage2.HorizontalScrollbarBarColor = true;
            this.metroTabPage2.Location = new System.Drawing.Point(4, 39);
            this.metroTabPage2.Name = "metroTabPage2";
            this.metroTabPage2.Size = new System.Drawing.Size(1234, 443);
            this.metroTabPage2.TabIndex = 1;
            this.metroTabPage2.Text = "Modify Items value";
            this.metroTabPage2.VerticalScrollbarBarColor = true;
            // 
            // tbReasonDepotworker
            // 
            this.tbReasonDepotworker.Location = new System.Drawing.Point(35, 304);
            this.tbReasonDepotworker.Name = "tbReasonDepotworker";
            this.tbReasonDepotworker.Size = new System.Drawing.Size(247, 70);
            this.tbReasonDepotworker.TabIndex = 90;
            // 
            // metroTextBox1
            // 
            this.metroTextBox1.Location = new System.Drawing.Point(127, 215);
            this.metroTextBox1.Name = "metroTextBox1";
            this.metroTextBox1.Size = new System.Drawing.Size(142, 23);
            this.metroTextBox1.TabIndex = 89;
            // 
            // btnModify
            // 
            this.btnModify.BackColor = System.Drawing.Color.DarkCyan;
            this.btnModify.Location = new System.Drawing.Point(74, 380);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(136, 45);
            this.btnModify.TabIndex = 88;
            this.btnModify.Text = "Modify!";
            this.btnModify.UseVisualStyleBackColor = false;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(35, 265);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(57, 20);
            this.metroLabel5.TabIndex = 87;
            this.metroLabel5.Text = "Reason:";
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(35, 218);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(61, 20);
            this.metroLabel4.TabIndex = 86;
            this.metroLabel4.Text = "Amount:";
            // 
            // metroLabel3
            // 
            this.metroLabel3.AutoSize = true;
            this.metroLabel3.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel3.Location = new System.Drawing.Point(35, 159);
            this.metroLabel3.Name = "metroLabel3";
            this.metroLabel3.Size = new System.Drawing.Size(247, 20);
            this.metroLabel3.TabIndex = 85;
            this.metroLabel3.Text = "Select an item to modify its value!";
            // 
            // tbItemIdDepotWorker
            // 
            this.tbItemIdDepotWorker.Location = new System.Drawing.Point(114, 42);
            this.tbItemIdDepotWorker.Name = "tbItemIdDepotWorker";
            this.tbItemIdDepotWorker.Size = new System.Drawing.Size(168, 23);
            this.tbItemIdDepotWorker.TabIndex = 84;
            // 
            // dataGridView1
            // 
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(382, 45);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(826, 354);
            this.dataGridView1.TabIndex = 83;
            // 
            // btnSearchItem
            // 
            this.btnSearchItem.BackColor = System.Drawing.Color.DarkCyan;
            this.btnSearchItem.Location = new System.Drawing.Point(74, 86);
            this.btnSearchItem.Name = "btnSearchItem";
            this.btnSearchItem.Size = new System.Drawing.Size(136, 45);
            this.btnSearchItem.TabIndex = 82;
            this.btnSearchItem.Text = "Search Item";
            this.btnSearchItem.UseVisualStyleBackColor = false;
            // 
            // lblSearchforAnItem
            // 
            this.lblSearchforAnItem.AutoSize = true;
            this.lblSearchforAnItem.Location = new System.Drawing.Point(35, 45);
            this.lblSearchforAnItem.Name = "lblSearchforAnItem";
            this.lblSearchforAnItem.Size = new System.Drawing.Size(54, 20);
            this.lblSearchforAnItem.TabIndex = 80;
            this.lblSearchforAnItem.Text = "Item id:";
            // 
            // OverviewPage
            // 
            this.OverviewPage.Controls.Add(this.metroLabel7);
            this.OverviewPage.Controls.Add(this.tbInfo);
            this.OverviewPage.Controls.Add(this.lblInfo);
            this.OverviewPage.Controls.Add(this.btnOverviewInDepot);
            this.OverviewPage.Controls.Add(this.rbBrand);
            this.OverviewPage.Controls.Add(this.rbId);
            this.OverviewPage.Controls.Add(this.dataGridView2);
            this.OverviewPage.Controls.Add(this.rbName);
            this.OverviewPage.Cursor = System.Windows.Forms.Cursors.SizeAll;
            this.OverviewPage.HorizontalScrollbarBarColor = true;
            this.OverviewPage.Location = new System.Drawing.Point(4, 59);
            this.OverviewPage.Name = "OverviewPage";
            this.OverviewPage.Size = new System.Drawing.Size(1247, 489);
            this.OverviewPage.TabIndex = 1;
            this.OverviewPage.Text = "Overview";
            this.OverviewPage.VerticalScrollbarBarColor = true;
            // 
            // metroLabel7
            // 
            this.metroLabel7.AutoSize = true;
            this.metroLabel7.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel7.Location = new System.Drawing.Point(36, 72);
            this.metroLabel7.Name = "metroLabel7";
            this.metroLabel7.Size = new System.Drawing.Size(289, 20);
            this.metroLabel7.Style = MetroFramework.MetroColorStyle.Black;
            this.metroLabel7.TabIndex = 9;
            this.metroLabel7.Text = "How do you want to search for an item?";
            // 
            // btnOverviewInDepot
            // 
            this.btnOverviewInDepot.BackColor = System.Drawing.Color.DarkCyan;
            this.btnOverviewInDepot.Location = new System.Drawing.Point(83, 223);
            this.btnOverviewInDepot.Name = "btnOverviewInDepot";
            this.btnOverviewInDepot.Size = new System.Drawing.Size(136, 45);
            this.btnOverviewInDepot.TabIndex = 83;
            this.btnOverviewInDepot.Text = "Overview";
            this.btnOverviewInDepot.UseVisualStyleBackColor = false;
            // 
            // rbBrand
            // 
            this.rbBrand.AutoSize = true;
            this.rbBrand.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.rbBrand.Location = new System.Drawing.Point(211, 120);
            this.rbBrand.Name = "rbBrand";
            this.rbBrand.Size = new System.Drawing.Size(64, 20);
            this.rbBrand.TabIndex = 8;
            this.rbBrand.TabStop = true;
            this.rbBrand.Text = "Brand";
            this.rbBrand.UseVisualStyleBackColor = true;
            // 
            // rbId
            // 
            this.rbId.AutoSize = true;
            this.rbId.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.rbId.Location = new System.Drawing.Point(139, 120);
            this.rbId.Name = "rbId";
            this.rbId.Size = new System.Drawing.Size(38, 20);
            this.rbId.TabIndex = 7;
            this.rbId.TabStop = true;
            this.rbId.Text = "id";
            this.rbId.UseVisualStyleBackColor = true;
            // 
            // rbName
            // 
            this.rbName.AutoSize = true;
            this.rbName.FontSize = MetroFramework.MetroLinkSize.Medium;
            this.rbName.Location = new System.Drawing.Point(47, 120);
            this.rbName.Name = "rbName";
            this.rbName.Size = new System.Drawing.Size(65, 20);
            this.rbName.TabIndex = 6;
            this.rbName.TabStop = true;
            this.rbName.Text = "Name";
            this.rbName.UseVisualStyleBackColor = true;
            // 
            // dataGridView2
            // 
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(410, 72);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 51;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(799, 393);
            this.dataGridView2.TabIndex = 2;
            // 
            // tbInfo
            // 
            this.tbInfo.Location = new System.Drawing.Point(109, 174);
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.Size = new System.Drawing.Size(203, 23);
            this.tbInfo.TabIndex = 10;
            // 
            // lblInfo
            // 
            this.lblInfo.AutoSize = true;
            this.lblInfo.Location = new System.Drawing.Point(47, 174);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(35, 20);
            this.lblInfo.TabIndex = 11;
            this.lblInfo.Text = "Info:";
            // 
            // DepotWorker
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1266, 686);
            this.Controls.Add(this.metroTabControl1);
            this.Controls.Add(this.panel1);
            this.Name = "DepotWorker";
            this.Text = "DepotWorker Application";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.metroTabControl1.ResumeLayout(false);
            this.ManagingStockPage.ResumeLayout(false);
            this.RestockRequestPage.ResumeLayout(false);
            this.metroTabPage1.ResumeLayout(false);
            this.metroTabPage1.PerformLayout();
            this.metroTabPage2.ResumeLayout(false);
            this.metroTabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.OverviewPage.ResumeLayout(false);
            this.OverviewPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage ManagingStockPage;
        private MetroFramework.Controls.MetroTabPage OverviewPage;
        private MetroFramework.Controls.MetroTabControl RestockRequestPage;
        private MetroFramework.Controls.MetroTabPage metroTabPage1;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private System.Windows.Forms.Button btnConfirm;
        private System.Windows.Forms.ListBox lbRestockRequestes;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private MetroFramework.Controls.MetroTabPage metroTabPage2;
        private MetroFramework.Controls.MetroTextBox tbReasonDepotworker;
        private MetroFramework.Controls.MetroTextBox metroTextBox1;
        private System.Windows.Forms.Button btnModify;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private MetroFramework.Controls.MetroLabel metroLabel3;
        private MetroFramework.Controls.MetroTextBox tbItemIdDepotWorker;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button btnSearchItem;
        private MetroFramework.Controls.MetroLabel lblSearchforAnItem;
        private System.Windows.Forms.DataGridView dataGridView2;
        private MetroFramework.Controls.MetroLabel metroLabel7;
        private System.Windows.Forms.Button btnOverviewInDepot;
        private MetroFramework.Controls.MetroRadioButton rbBrand;
        private MetroFramework.Controls.MetroRadioButton rbId;
        private MetroFramework.Controls.MetroRadioButton rbName;
        private MetroFramework.Controls.MetroLabel lblInfo;
        private MetroFramework.Controls.MetroTextBox tbInfo;
    }
}