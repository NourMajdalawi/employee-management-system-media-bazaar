﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroupProject_MediaBazzar
{
    public partial class frmEditEmployee : Form
    {
        Store store;
        Employee employee;
        public frmEditEmployee(Store store, Employee employee)
        {
            InitializeComponent();

            //Pre fill all fields with the employee's current data
            this.store = store;
            tbFirstName.Text = employee.firstName;
            tbLastName.Text = employee.lastName;
            tbNationality.Text = employee.nationality;
            tbRegion.Text = employee.region;
            tbZipcode.Text = employee.zipcode;
            tbStreetName.Text = employee.streetName;
            tbPhoneNumber.Text = employee.phonenumber;
            tbPassword.Text = employee.password;
            tbEmergencyPhoneNumber.Text = employee.emergencyPhonenumber;
            tbEmail.Text = employee.emailAddress;
            List<string> languages =  employee.spokenLanguages;
            tbusername.Text = employee.username;
            dtpBirthday.Value = ((DateTime)employee.birthDate);
            this.employee = employee;
        }
        private void btnSave_Click(object sender, EventArgs e)
        {
            List<String> variables = new List<String>();
            //Get all new data for the employee 
            int id = this.employee.id;
            string username = tbusername.Text;
            string firstName = tbFirstName.Text;
            string lastName = tbLastName.Text;
            string nationality = tbNationality.Text;
            string region = tbRegion.Text;
            string zipCode = tbZipcode.Text;
            string streetName = tbStreetName.Text;
            string phoneNumber = tbPhoneNumber.Text;
            string password = tbPassword.Text;
            string emergencyPhoneNumber = tbEmergencyPhoneNumber.Text;
            string emailAddress = tbEmail.Text;
          
            DateTime birthDate = dtpBirthday.Value;

            //Add all the variables to a list
            variables.Add(id.ToString());
            variables.Add(username);
            variables.Add(firstName);
            variables.Add(lastName);
            variables.Add(nationality);
            variables.Add(region);
            variables.Add(zipCode);
            variables.Add(streetName);
            variables.Add(phoneNumber);
            variables.Add(password);
            variables.Add(emergencyPhoneNumber);
            variables.Add(emailAddress);
         
            variables.Add(birthDate.ToString());

            //Check if every field was filled in, if it was not, show a warning and return
            foreach(string var in variables)
            {
                if(var == "")
                {
                    MessageBox.Show("Make sure all fields are filled in before submitting changes");
                    return;
                }
            }

            //Update the employee and close the editemployee form
            Employee updatedEmployee = new Employee(id, username, firstName, lastName, this.employee.gender, birthDate, nationality, emailAddress, this.employee.bsn, region, zipCode, streetName, phoneNumber, password, emergencyPhoneNumber,this.employee.spokenLanguages,this.employee.spouse);
            if (store.UpdateEmployeeInfo(updatedEmployee))
            {
                MessageBox.Show("updated successfully");
                this.Close();
            }
        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
    }
}

