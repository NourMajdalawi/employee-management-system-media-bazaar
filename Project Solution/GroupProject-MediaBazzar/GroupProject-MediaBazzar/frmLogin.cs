﻿using Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MySql;
using MySql.Data.MySqlClient;

namespace GroupProject_MediaBazzar
{
    public partial class frmLogin : Form
    {
        DataHelper dh;
        Store store;
        public frmLogin()
        {
            InitializeComponent();
            dh = new DataHelper();
            store = new Store("MediaBazaarEindhoven", dh);
    }

        private void CloseLogInPage_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void MinimizeLogInPage_Click(object sender, EventArgs e)
        {
            WindowState = FormWindowState.Minimized;
        }
        private void btnLogIn_Click_1(object sender, EventArgs e)
        {
            string username = tbUserName.Text;
            string password = tbPass.Text;
            Position executedQuery = Position.None;
            try
            {
                executedQuery = dh.Login(username, password);
                if(executedQuery == Position.None)
                {
                    MessageBox.Show("No user with this combination of username and password was found.");
                    return;
                }
                switch(executedQuery)
                {
                    case Position.Admin:
                        frmAdministration adminForm = new frmAdministration(dh.GetFullName(username), store);
                        adminForm.Show();
                        Hide();
                        break;
                    case Position.DepotWorker:
                        frmDepot depotForm = new frmDepot(dh.GetFullName(username), store);
                        depotForm.Show();
                        Hide();
                        break;
                    case Position.Manager:
                        frmManager managerForm = new frmManager(dh.GetFullName(username), store);
                        managerForm.Show();
                        Hide();
                        break;
                    case Position.RetailWorker:
                        MessageBox.Show("Logged in as retail worker, no forms for this position yet.");
                        
                        break;
                }
                //this.Hide();
            }
            catch (FormatException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void frmLogin_Load(object sender, EventArgs e)
        {

        }
    }
}