﻿namespace GroupProject_MediaBazzar
{
    partial class frmDepot
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDepot));
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnLogOut = new System.Windows.Forms.Button();
            this.lbLoggedInAs = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.metroTabControl1 = new MetroFramework.Controls.MetroTabControl();
            this.ManageStockPage = new MetroFramework.Controls.MetroTabPage();
            this.metroTabControl2 = new MetroFramework.Controls.MetroTabControl();
            this.RestockRequestsPage = new MetroFramework.Controls.MetroTabPage();
            this.dgvRequests = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productbarcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productamount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.floor = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Date = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.pictureBox4 = new System.Windows.Forms.PictureBox();
            this.btnConfirm = new System.Windows.Forms.Button();
            this.metroLabel2 = new MetroFramework.Controls.MetroLabel();
            this.modifyItemsPage = new MetroFramework.Controls.MetroTabPage();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.dgvFoundItems = new System.Windows.Forms.DataGridView();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.pictureBox3 = new System.Windows.Forms.PictureBox();
            this.tbAmountToChange = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel4 = new MetroFramework.Controls.MetroLabel();
            this.tbReasonDepotworker = new MetroFramework.Controls.MetroTextBox();
            this.metroLabel5 = new MetroFramework.Controls.MetroLabel();
            this.btnModify = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.tbItemModSearch = new MetroFramework.Controls.MetroTextBox();
            this.lblSearchforAnItem = new MetroFramework.Controls.MetroLabel();
            this.btnSearchItem = new System.Windows.Forms.Button();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.OverviewPage = new MetroFramework.Controls.MetroTabPage();
            this.pictureBox6 = new System.Windows.Forms.PictureBox();
            this.gbSearch = new System.Windows.Forms.GroupBox();
            this.metroLabel6 = new MetroFramework.Controls.MetroLabel();
            this.tbInfo = new MetroFramework.Controls.MetroTextBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.btnOverview = new System.Windows.Forms.Button();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.bar = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Productnamee = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.modelnumber = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.brandname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.category = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.normalp = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.promoprice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockindepot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.stockonshelves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.amoutnsold = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Barcode = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.productname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Modelnr = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.brand = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.categorie = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.NormalPrice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Promotionprice = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Stockdepot = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.StockShelves = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.AmountSold = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.metroTabControl1.SuspendLayout();
            this.ManageStockPage.SuspendLayout();
            this.metroTabControl2.SuspendLayout();
            this.RestockRequestsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRequests)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).BeginInit();
            this.modifyItemsPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFoundItems)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.OverviewPage.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).BeginInit();
            this.gbSearch.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.btnLogOut);
            this.panel1.Controls.Add(this.lbLoggedInAs);
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(1, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1586, 124);
            this.panel1.TabIndex = 6;
            // 
            // btnLogOut
            // 
            this.btnLogOut.BackColor = System.Drawing.Color.Black;
            this.btnLogOut.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnLogOut.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.btnLogOut.Location = new System.Drawing.Point(1467, 11);
            this.btnLogOut.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLogOut.Name = "btnLogOut";
            this.btnLogOut.Size = new System.Drawing.Size(95, 52);
            this.btnLogOut.TabIndex = 1;
            this.btnLogOut.Text = "Log Out";
            this.btnLogOut.UseVisualStyleBackColor = false;
            this.btnLogOut.Click += new System.EventHandler(this.btnLogOut_Click);
            // 
            // lbLoggedInAs
            // 
            this.lbLoggedInAs.AutoSize = true;
            this.lbLoggedInAs.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbLoggedInAs.Location = new System.Drawing.Point(331, 46);
            this.lbLoggedInAs.Name = "lbLoggedInAs";
            this.lbLoggedInAs.Size = new System.Drawing.Size(129, 20);
            this.lbLoggedInAs.TabIndex = 4;
            this.lbLoggedInAs.Text = "Logged in as: ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(203, 121);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // metroTabControl1
            // 
            this.metroTabControl1.Controls.Add(this.ManageStockPage);
            this.metroTabControl1.Controls.Add(this.OverviewPage);
            this.metroTabControl1.CustomBackground = true;
            this.metroTabControl1.Location = new System.Drawing.Point(1, 130);
            this.metroTabControl1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.metroTabControl1.Name = "metroTabControl1";
            this.metroTabControl1.SelectedIndex = 0;
            this.metroTabControl1.Size = new System.Drawing.Size(1575, 572);
            this.metroTabControl1.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.metroTabControl1.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTabControl1.TabIndex = 7;
            // 
            // ManageStockPage
            // 
            this.ManageStockPage.Controls.Add(this.metroTabControl2);
            this.ManageStockPage.HorizontalScrollbarBarColor = true;
            this.ManageStockPage.Location = new System.Drawing.Point(4, 39);
            this.ManageStockPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.ManageStockPage.Name = "ManageStockPage";
            this.ManageStockPage.Size = new System.Drawing.Size(1567, 529);
            this.ManageStockPage.Style = MetroFramework.MetroColorStyle.Teal;
            this.ManageStockPage.TabIndex = 0;
            this.ManageStockPage.Text = "Manage Stock";
            this.ManageStockPage.VerticalScrollbarBarColor = true;
            this.ManageStockPage.VerticalScrollbarSize = 11;
            // 
            // metroTabControl2
            // 
            this.metroTabControl2.Controls.Add(this.RestockRequestsPage);
            this.metroTabControl2.Controls.Add(this.modifyItemsPage);
            this.metroTabControl2.Location = new System.Drawing.Point(0, 2);
            this.metroTabControl2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.metroTabControl2.Name = "metroTabControl2";
            this.metroTabControl2.SelectedIndex = 1;
            this.metroTabControl2.Size = new System.Drawing.Size(1571, 536);
            this.metroTabControl2.Style = MetroFramework.MetroColorStyle.Teal;
            this.metroTabControl2.TabIndex = 2;
            // 
            // RestockRequestsPage
            // 
            this.RestockRequestsPage.Controls.Add(this.dgvRequests);
            this.RestockRequestsPage.Controls.Add(this.pictureBox4);
            this.RestockRequestsPage.Controls.Add(this.btnConfirm);
            this.RestockRequestsPage.Controls.Add(this.metroLabel2);
            this.RestockRequestsPage.HorizontalScrollbarBarColor = true;
            this.RestockRequestsPage.Location = new System.Drawing.Point(4, 39);
            this.RestockRequestsPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.RestockRequestsPage.Name = "RestockRequestsPage";
            this.RestockRequestsPage.Size = new System.Drawing.Size(1563, 493);
            this.RestockRequestsPage.TabIndex = 0;
            this.RestockRequestsPage.Text = "Restock Requests";
            this.RestockRequestsPage.VerticalScrollbarBarColor = true;
            this.RestockRequestsPage.VerticalScrollbarSize = 11;
            // 
            // dgvRequests
            // 
            this.dgvRequests.AllowUserToAddRows = false;
            this.dgvRequests.AllowUserToDeleteRows = false;
            this.dgvRequests.AllowUserToResizeColumns = false;
            this.dgvRequests.AllowUserToResizeRows = false;
            this.dgvRequests.ColumnHeadersHeight = 29;
            this.dgvRequests.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.productbarcode,
            this.productamount,
            this.floor,
            this.Date});
            this.dgvRequests.Location = new System.Drawing.Point(385, 24);
            this.dgvRequests.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvRequests.MultiSelect = false;
            this.dgvRequests.Name = "dgvRequests";
            this.dgvRequests.ReadOnly = true;
            this.dgvRequests.RowHeadersWidth = 51;
            this.dgvRequests.RowTemplate.Height = 24;
            this.dgvRequests.ShowEditingIcon = false;
            this.dgvRequests.Size = new System.Drawing.Size(1169, 458);
            this.dgvRequests.TabIndex = 90;
            // 
            // id
            // 
            this.id.HeaderText = "Id";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            this.id.Width = 125;
            // 
            // productbarcode
            // 
            this.productbarcode.HeaderText = "Product barcode";
            this.productbarcode.MinimumWidth = 6;
            this.productbarcode.Name = "productbarcode";
            this.productbarcode.ReadOnly = true;
            this.productbarcode.Width = 125;
            // 
            // productamount
            // 
            this.productamount.HeaderText = "Product amount";
            this.productamount.MinimumWidth = 6;
            this.productamount.Name = "productamount";
            this.productamount.ReadOnly = true;
            this.productamount.Width = 125;
            // 
            // floor
            // 
            this.floor.HeaderText = "Floor";
            this.floor.MinimumWidth = 6;
            this.floor.Name = "floor";
            this.floor.ReadOnly = true;
            this.floor.Width = 125;
            // 
            // Date
            // 
            this.Date.HeaderText = "Date";
            this.Date.MinimumWidth = 6;
            this.Date.Name = "Date";
            this.Date.ReadOnly = true;
            this.Date.Width = 125;
            // 
            // pictureBox4
            // 
            this.pictureBox4.BackColor = System.Drawing.Color.White;
            this.pictureBox4.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox4.Image")));
            this.pictureBox4.Location = new System.Drawing.Point(0, 2);
            this.pictureBox4.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox4.Name = "pictureBox4";
            this.pictureBox4.Size = new System.Drawing.Size(379, 174);
            this.pictureBox4.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox4.TabIndex = 88;
            this.pictureBox4.TabStop = false;
            // 
            // btnConfirm
            // 
            this.btnConfirm.BackColor = System.Drawing.Color.DarkCyan;
            this.btnConfirm.Location = new System.Drawing.Point(102, 189);
            this.btnConfirm.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnConfirm.Name = "btnConfirm";
            this.btnConfirm.Size = new System.Drawing.Size(171, 63);
            this.btnConfirm.TabIndex = 87;
            this.btnConfirm.Text = "Confirm";
            this.btnConfirm.UseVisualStyleBackColor = false;
            this.btnConfirm.Click += new System.EventHandler(this.btnConfirm_Click);
            // 
            // metroLabel2
            // 
            this.metroLabel2.AutoSize = true;
            this.metroLabel2.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel2.Location = new System.Drawing.Point(385, 2);
            this.metroLabel2.Name = "metroLabel2";
            this.metroLabel2.Size = new System.Drawing.Size(332, 20);
            this.metroLabel2.TabIndex = 86;
            this.metroLabel2.Text = "Recieving restock requests from the manager:";
            // 
            // modifyItemsPage
            // 
            this.modifyItemsPage.Controls.Add(this.pictureBox7);
            this.modifyItemsPage.Controls.Add(this.dgvFoundItems);
            this.modifyItemsPage.Controls.Add(this.groupBox2);
            this.modifyItemsPage.Controls.Add(this.groupBox1);
            this.modifyItemsPage.HorizontalScrollbarBarColor = true;
            this.modifyItemsPage.Location = new System.Drawing.Point(4, 39);
            this.modifyItemsPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.modifyItemsPage.Name = "modifyItemsPage";
            this.modifyItemsPage.Size = new System.Drawing.Size(1563, 493);
            this.modifyItemsPage.TabIndex = 1;
            this.modifyItemsPage.Text = "Modify items value";
            this.modifyItemsPage.VerticalScrollbarBarColor = true;
            this.modifyItemsPage.VerticalScrollbarSize = 11;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(15, 26);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(211, 205);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox7.TabIndex = 115;
            this.pictureBox7.TabStop = false;
            // 
            // dgvFoundItems
            // 
            this.dgvFoundItems.AllowUserToAddRows = false;
            this.dgvFoundItems.AllowUserToDeleteRows = false;
            this.dgvFoundItems.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFoundItems.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Barcode,
            this.productname,
            this.Modelnr,
            this.brand,
            this.categorie,
            this.NormalPrice,
            this.Promotionprice,
            this.Stockdepot,
            this.StockShelves,
            this.AmountSold});
            this.dgvFoundItems.Location = new System.Drawing.Point(15, 259);
            this.dgvFoundItems.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dgvFoundItems.Name = "dgvFoundItems";
            this.dgvFoundItems.ReadOnly = true;
            this.dgvFoundItems.RowHeadersWidth = 51;
            this.dgvFoundItems.RowTemplate.Height = 24;
            this.dgvFoundItems.Size = new System.Drawing.Size(1489, 223);
            this.dgvFoundItems.TabIndex = 114;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.White;
            this.groupBox2.Controls.Add(this.metroLabel1);
            this.groupBox2.Controls.Add(this.pictureBox3);
            this.groupBox2.Controls.Add(this.tbAmountToChange);
            this.groupBox2.Controls.Add(this.metroLabel4);
            this.groupBox2.Controls.Add(this.tbReasonDepotworker);
            this.groupBox2.Controls.Add(this.metroLabel5);
            this.groupBox2.Controls.Add(this.btnModify);
            this.groupBox2.Location = new System.Drawing.Point(810, 14);
            this.groupBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox2.Size = new System.Drawing.Size(694, 217);
            this.groupBox2.TabIndex = 112;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Change Item value";
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.FontWeight = MetroFramework.MetroLabelWeight.Bold;
            this.metroLabel1.Location = new System.Drawing.Point(197, 28);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(144, 20);
            this.metroLabel1.TabIndex = 111;
            this.metroLabel1.Text = "Select an item first:";
            // 
            // pictureBox3
            // 
            this.pictureBox3.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox3.Image")));
            this.pictureBox3.Location = new System.Drawing.Point(21, 33);
            this.pictureBox3.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox3.Name = "pictureBox3";
            this.pictureBox3.Size = new System.Drawing.Size(61, 50);
            this.pictureBox3.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox3.TabIndex = 110;
            this.pictureBox3.TabStop = false;
            // 
            // tbAmountToChange
            // 
            this.tbAmountToChange.Location = new System.Drawing.Point(200, 69);
            this.tbAmountToChange.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbAmountToChange.Name = "tbAmountToChange";
            this.tbAmountToChange.Size = new System.Drawing.Size(141, 25);
            this.tbAmountToChange.TabIndex = 107;
            // 
            // metroLabel4
            // 
            this.metroLabel4.AutoSize = true;
            this.metroLabel4.Location = new System.Drawing.Point(99, 69);
            this.metroLabel4.Name = "metroLabel4";
            this.metroLabel4.Size = new System.Drawing.Size(65, 20);
            this.metroLabel4.TabIndex = 105;
            this.metroLabel4.Text = "Amount :";
            // 
            // tbReasonDepotworker
            // 
            this.tbReasonDepotworker.Location = new System.Drawing.Point(200, 124);
            this.tbReasonDepotworker.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbReasonDepotworker.Name = "tbReasonDepotworker";
            this.tbReasonDepotworker.Size = new System.Drawing.Size(247, 70);
            this.tbReasonDepotworker.TabIndex = 108;
            // 
            // metroLabel5
            // 
            this.metroLabel5.AutoSize = true;
            this.metroLabel5.Location = new System.Drawing.Point(99, 124);
            this.metroLabel5.Name = "metroLabel5";
            this.metroLabel5.Size = new System.Drawing.Size(57, 20);
            this.metroLabel5.TabIndex = 106;
            this.metroLabel5.Text = "Reason:";
            // 
            // btnModify
            // 
            this.btnModify.BackColor = System.Drawing.Color.DarkCyan;
            this.btnModify.Location = new System.Drawing.Point(515, 124);
            this.btnModify.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnModify.Name = "btnModify";
            this.btnModify.Size = new System.Drawing.Size(147, 66);
            this.btnModify.TabIndex = 109;
            this.btnModify.Text = "Modify";
            this.btnModify.UseVisualStyleBackColor = false;
            this.btnModify.Click += new System.EventHandler(this.btnModify_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.tbItemModSearch);
            this.groupBox1.Controls.Add(this.lblSearchforAnItem);
            this.groupBox1.Controls.Add(this.btnSearchItem);
            this.groupBox1.Controls.Add(this.pictureBox2);
            this.groupBox1.Location = new System.Drawing.Point(292, 14);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(375, 217);
            this.groupBox1.TabIndex = 111;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Search";
            // 
            // tbItemModSearch
            // 
            this.tbItemModSearch.Location = new System.Drawing.Point(152, 53);
            this.tbItemModSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbItemModSearch.Name = "tbItemModSearch";
            this.tbItemModSearch.Size = new System.Drawing.Size(168, 30);
            this.tbItemModSearch.TabIndex = 109;
            // 
            // lblSearchforAnItem
            // 
            this.lblSearchforAnItem.AutoSize = true;
            this.lblSearchforAnItem.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.lblSearchforAnItem.Location = new System.Drawing.Point(88, 53);
            this.lblSearchforAnItem.Name = "lblSearchforAnItem";
            this.lblSearchforAnItem.Size = new System.Drawing.Size(46, 25);
            this.lblSearchforAnItem.TabIndex = 106;
            this.lblSearchforAnItem.Text = "Info:";
            // 
            // btnSearchItem
            // 
            this.btnSearchItem.BackColor = System.Drawing.Color.DarkCyan;
            this.btnSearchItem.Location = new System.Drawing.Point(88, 124);
            this.btnSearchItem.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnSearchItem.Name = "btnSearchItem";
            this.btnSearchItem.Size = new System.Drawing.Size(232, 48);
            this.btnSearchItem.TabIndex = 107;
            this.btnSearchItem.Text = "Search Item";
            this.btnSearchItem.UseVisualStyleBackColor = false;
            this.btnSearchItem.Click += new System.EventHandler(this.btnSearchItem_Click_1);
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox2.Image")));
            this.pictureBox2.Location = new System.Drawing.Point(12, 28);
            this.pictureBox2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(58, 55);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox2.TabIndex = 83;
            this.pictureBox2.TabStop = false;
            // 
            // OverviewPage
            // 
            this.OverviewPage.Controls.Add(this.pictureBox6);
            this.OverviewPage.Controls.Add(this.gbSearch);
            this.OverviewPage.Controls.Add(this.dataGridView2);
            this.OverviewPage.HorizontalScrollbarBarColor = true;
            this.OverviewPage.Location = new System.Drawing.Point(4, 39);
            this.OverviewPage.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.OverviewPage.Name = "OverviewPage";
            this.OverviewPage.Size = new System.Drawing.Size(1567, 529);
            this.OverviewPage.TabIndex = 1;
            this.OverviewPage.Text = "Items Overview";
            this.OverviewPage.VerticalScrollbarBarColor = true;
            this.OverviewPage.VerticalScrollbarSize = 11;
            // 
            // pictureBox6
            // 
            this.pictureBox6.BackColor = System.Drawing.Color.White;
            this.pictureBox6.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox6.Image")));
            this.pictureBox6.Location = new System.Drawing.Point(10, 22);
            this.pictureBox6.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox6.Name = "pictureBox6";
            this.pictureBox6.Size = new System.Drawing.Size(158, 146);
            this.pictureBox6.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox6.TabIndex = 104;
            this.pictureBox6.TabStop = false;
            // 
            // gbSearch
            // 
            this.gbSearch.BackColor = System.Drawing.Color.White;
            this.gbSearch.Controls.Add(this.metroLabel6);
            this.gbSearch.Controls.Add(this.tbInfo);
            this.gbSearch.Controls.Add(this.pictureBox5);
            this.gbSearch.Controls.Add(this.btnOverview);
            this.gbSearch.Location = new System.Drawing.Point(370, 32);
            this.gbSearch.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbSearch.Name = "gbSearch";
            this.gbSearch.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbSearch.Size = new System.Drawing.Size(419, 136);
            this.gbSearch.TabIndex = 103;
            this.gbSearch.TabStop = false;
            this.gbSearch.Text = "Search";
            // 
            // metroLabel6
            // 
            this.metroLabel6.AutoSize = true;
            this.metroLabel6.FontSize = MetroFramework.MetroLabelSize.Tall;
            this.metroLabel6.Location = new System.Drawing.Point(102, 38);
            this.metroLabel6.Name = "metroLabel6";
            this.metroLabel6.Size = new System.Drawing.Size(46, 25);
            this.metroLabel6.TabIndex = 107;
            this.metroLabel6.Text = "Info:";
            // 
            // tbInfo
            // 
            this.tbInfo.Location = new System.Drawing.Point(180, 38);
            this.tbInfo.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbInfo.Name = "tbInfo";
            this.tbInfo.Size = new System.Drawing.Size(163, 23);
            this.tbInfo.TabIndex = 106;
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(12, 28);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(51, 49);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 83;
            this.pictureBox5.TabStop = false;
            // 
            // btnOverview
            // 
            this.btnOverview.BackColor = System.Drawing.Color.DarkCyan;
            this.btnOverview.Location = new System.Drawing.Point(102, 75);
            this.btnOverview.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnOverview.Name = "btnOverview";
            this.btnOverview.Size = new System.Drawing.Size(241, 46);
            this.btnOverview.TabIndex = 102;
            this.btnOverview.Text = "Overview";
            this.btnOverview.UseVisualStyleBackColor = false;
            this.btnOverview.Click += new System.EventHandler(this.BtnOverview_Click);
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.bar,
            this.Productnamee,
            this.modelnumber,
            this.brandname,
            this.category,
            this.normalp,
            this.promoprice,
            this.stockindepot,
            this.stockonshelves,
            this.amoutnsold});
            this.dataGridView2.Location = new System.Drawing.Point(10, 186);
            this.dataGridView2.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            this.dataGridView2.RowHeadersWidth = 51;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(1494, 337);
            this.dataGridView2.TabIndex = 3;
            // 
            // bar
            // 
            this.bar.HeaderText = "Barcode";
            this.bar.MinimumWidth = 6;
            this.bar.Name = "bar";
            this.bar.ReadOnly = true;
            this.bar.Width = 50;
            // 
            // Productnamee
            // 
            this.Productnamee.HeaderText = "Product name";
            this.Productnamee.MinimumWidth = 6;
            this.Productnamee.Name = "Productnamee";
            this.Productnamee.ReadOnly = true;
            this.Productnamee.Width = 125;
            // 
            // modelnumber
            // 
            this.modelnumber.HeaderText = "Model number";
            this.modelnumber.MinimumWidth = 6;
            this.modelnumber.Name = "modelnumber";
            this.modelnumber.ReadOnly = true;
            this.modelnumber.Width = 125;
            // 
            // brandname
            // 
            this.brandname.HeaderText = "Manufacturer brand";
            this.brandname.MinimumWidth = 6;
            this.brandname.Name = "brandname";
            this.brandname.ReadOnly = true;
            this.brandname.Width = 125;
            // 
            // category
            // 
            this.category.HeaderText = "Category";
            this.category.MinimumWidth = 6;
            this.category.Name = "category";
            this.category.ReadOnly = true;
            this.category.Width = 125;
            // 
            // normalp
            // 
            this.normalp.HeaderText = "Normal price";
            this.normalp.MinimumWidth = 6;
            this.normalp.Name = "normalp";
            this.normalp.ReadOnly = true;
            // 
            // promoprice
            // 
            this.promoprice.HeaderText = "Promotion price";
            this.promoprice.MinimumWidth = 6;
            this.promoprice.Name = "promoprice";
            this.promoprice.ReadOnly = true;
            // 
            // stockindepot
            // 
            this.stockindepot.HeaderText = "Stock in depot";
            this.stockindepot.MinimumWidth = 6;
            this.stockindepot.Name = "stockindepot";
            this.stockindepot.ReadOnly = true;
            // 
            // stockonshelves
            // 
            this.stockonshelves.HeaderText = "Stock on shelves";
            this.stockonshelves.MinimumWidth = 6;
            this.stockonshelves.Name = "stockonshelves";
            this.stockonshelves.ReadOnly = true;
            // 
            // amoutnsold
            // 
            this.amoutnsold.HeaderText = "Amount sold";
            this.amoutnsold.MinimumWidth = 6;
            this.amoutnsold.Name = "amoutnsold";
            this.amoutnsold.ReadOnly = true;
            // 
            // Barcode
            // 
            this.Barcode.HeaderText = "Barcode";
            this.Barcode.MinimumWidth = 6;
            this.Barcode.Name = "Barcode";
            this.Barcode.ReadOnly = true;
            this.Barcode.Width = 65;
            // 
            // productname
            // 
            this.productname.HeaderText = "Product name";
            this.productname.MinimumWidth = 6;
            this.productname.Name = "productname";
            this.productname.ReadOnly = true;
            this.productname.Width = 125;
            // 
            // Modelnr
            // 
            this.Modelnr.HeaderText = "Model number";
            this.Modelnr.MinimumWidth = 6;
            this.Modelnr.Name = "Modelnr";
            this.Modelnr.ReadOnly = true;
            // 
            // brand
            // 
            this.brand.HeaderText = "Manufacturer brand";
            this.brand.MinimumWidth = 6;
            this.brand.Name = "brand";
            this.brand.ReadOnly = true;
            this.brand.Width = 125;
            // 
            // categorie
            // 
            this.categorie.HeaderText = "Category";
            this.categorie.MinimumWidth = 6;
            this.categorie.Name = "categorie";
            this.categorie.ReadOnly = true;
            this.categorie.Width = 125;
            // 
            // NormalPrice
            // 
            this.NormalPrice.HeaderText = "Normal price";
            this.NormalPrice.MinimumWidth = 6;
            this.NormalPrice.Name = "NormalPrice";
            this.NormalPrice.ReadOnly = true;
            // 
            // Promotionprice
            // 
            this.Promotionprice.HeaderText = "Promotion price";
            this.Promotionprice.MinimumWidth = 6;
            this.Promotionprice.Name = "Promotionprice";
            this.Promotionprice.ReadOnly = true;
            // 
            // Stockdepot
            // 
            this.Stockdepot.HeaderText = "Stock in depot";
            this.Stockdepot.MinimumWidth = 6;
            this.Stockdepot.Name = "Stockdepot";
            this.Stockdepot.ReadOnly = true;
            // 
            // StockShelves
            // 
            this.StockShelves.HeaderText = "Stock on shelves";
            this.StockShelves.MinimumWidth = 6;
            this.StockShelves.Name = "StockShelves";
            this.StockShelves.ReadOnly = true;
            // 
            // AmountSold
            // 
            this.AmountSold.HeaderText = "Amount sold";
            this.AmountSold.MinimumWidth = 6;
            this.AmountSold.Name = "AmountSold";
            this.AmountSold.ReadOnly = true;
            // 
            // frmDepot
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1575, 703);
            this.Controls.Add(this.metroTabControl1);
            this.Controls.Add(this.panel1);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmDepot";
            this.Text = "DepotWorker";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.metroTabControl1.ResumeLayout(false);
            this.ManageStockPage.ResumeLayout(false);
            this.metroTabControl2.ResumeLayout(false);
            this.RestockRequestsPage.ResumeLayout(false);
            this.RestockRequestsPage.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvRequests)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox4)).EndInit();
            this.modifyItemsPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFoundItems)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox3)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.OverviewPage.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox6)).EndInit();
            this.gbSearch.ResumeLayout(false);
            this.gbSearch.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnLogOut;
        private System.Windows.Forms.Label lbLoggedInAs;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroTabControl metroTabControl1;
        private MetroFramework.Controls.MetroTabPage ManageStockPage;
        private MetroFramework.Controls.MetroTabPage OverviewPage;
        private MetroFramework.Controls.MetroTabControl metroTabControl2;
        private MetroFramework.Controls.MetroTabPage RestockRequestsPage;
        private MetroFramework.Controls.MetroTabPage modifyItemsPage;
        private System.Windows.Forms.Button btnConfirm;
        private MetroFramework.Controls.MetroLabel metroLabel2;
        private MetroFramework.Controls.MetroTextBox tbReasonDepotworker;
        private MetroFramework.Controls.MetroTextBox tbAmountToChange;
        private MetroFramework.Controls.MetroLabel metroLabel5;
        private MetroFramework.Controls.MetroLabel metroLabel4;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button btnModify;
        private System.Windows.Forms.Button btnOverview;
        private System.Windows.Forms.PictureBox pictureBox4;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.PictureBox pictureBox3;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.PictureBox pictureBox6;
        private System.Windows.Forms.GroupBox gbSearch;
        private MetroFramework.Controls.MetroLabel metroLabel6;
        private MetroFramework.Controls.MetroTextBox tbInfo;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.DataGridView dgvFoundItems;
        private MetroFramework.Controls.MetroTextBox tbItemModSearch;
        private MetroFramework.Controls.MetroLabel lblSearchforAnItem;
        private System.Windows.Forms.Button btnSearchItem;
        private System.Windows.Forms.DataGridView dgvRequests;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn productbarcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn productamount;
        private System.Windows.Forms.DataGridViewTextBoxColumn floor;
        private System.Windows.Forms.DataGridViewTextBoxColumn Date;
        private MetroFramework.Controls.MetroLabel metroLabel1;
        private System.Windows.Forms.PictureBox pictureBox7;
        private System.Windows.Forms.DataGridViewTextBoxColumn bar;
        private System.Windows.Forms.DataGridViewTextBoxColumn Productnamee;
        private System.Windows.Forms.DataGridViewTextBoxColumn modelnumber;
        private System.Windows.Forms.DataGridViewTextBoxColumn brandname;
        private System.Windows.Forms.DataGridViewTextBoxColumn category;
        private System.Windows.Forms.DataGridViewTextBoxColumn normalp;
        private System.Windows.Forms.DataGridViewTextBoxColumn promoprice;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockindepot;
        private System.Windows.Forms.DataGridViewTextBoxColumn stockonshelves;
        private System.Windows.Forms.DataGridViewTextBoxColumn amoutnsold;
        private System.Windows.Forms.DataGridViewTextBoxColumn Barcode;
        private System.Windows.Forms.DataGridViewTextBoxColumn productname;
        private System.Windows.Forms.DataGridViewTextBoxColumn Modelnr;
        private System.Windows.Forms.DataGridViewTextBoxColumn brand;
        private System.Windows.Forms.DataGridViewTextBoxColumn categorie;
        private System.Windows.Forms.DataGridViewTextBoxColumn NormalPrice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Promotionprice;
        private System.Windows.Forms.DataGridViewTextBoxColumn Stockdepot;
        private System.Windows.Forms.DataGridViewTextBoxColumn StockShelves;
        private System.Windows.Forms.DataGridViewTextBoxColumn AmountSold;
    }
}