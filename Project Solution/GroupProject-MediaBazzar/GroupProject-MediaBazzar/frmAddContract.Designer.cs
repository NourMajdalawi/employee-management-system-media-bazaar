﻿namespace GroupProject_MediaBazzar
{
    partial class frmAddContract
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmAddContract));
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmbFTE = new System.Windows.Forms.ComboBox();
            this.metroLabel1 = new MetroFramework.Controls.MetroLabel();
            this.nudHourlySalary = new System.Windows.Forms.NumericUpDown();
            this.cbbAddContractPosition = new System.Windows.Forms.ComboBox();
            this.cbEndDate = new MetroFramework.Controls.MetroCheckBox();
            this.metroLabel8 = new MetroFramework.Controls.MetroLabel();
            this.lblPosition = new MetroFramework.Controls.MetroLabel();
            this.btnAddContract = new System.Windows.Forms.Button();
            this.metroLabel9 = new MetroFramework.Controls.MetroLabel();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.pictureBox7 = new System.Windows.Forms.PictureBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHourlySalary)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.White;
            this.groupBox1.Controls.Add(this.cmbFTE);
            this.groupBox1.Controls.Add(this.metroLabel1);
            this.groupBox1.Controls.Add(this.nudHourlySalary);
            this.groupBox1.Controls.Add(this.cbbAddContractPosition);
            this.groupBox1.Controls.Add(this.cbEndDate);
            this.groupBox1.Controls.Add(this.metroLabel8);
            this.groupBox1.Controls.Add(this.lblPosition);
            this.groupBox1.Controls.Add(this.btnAddContract);
            this.groupBox1.Controls.Add(this.metroLabel9);
            this.groupBox1.Controls.Add(this.dtpStartDate);
            this.groupBox1.Controls.Add(this.dtpEndDate);
            this.groupBox1.Location = new System.Drawing.Point(26, 106);
            this.groupBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.groupBox1.Size = new System.Drawing.Size(353, 333);
            this.groupBox1.TabIndex = 96;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Add Contract";
            // 
            // cmbFTE
            // 
            this.cmbFTE.FormattingEnabled = true;
            this.cmbFTE.Location = new System.Drawing.Point(131, 87);
            this.cmbFTE.Name = "cmbFTE";
            this.cmbFTE.Size = new System.Drawing.Size(129, 24);
            this.cmbFTE.TabIndex = 107;
            // 
            // metroLabel1
            // 
            this.metroLabel1.AutoSize = true;
            this.metroLabel1.Location = new System.Drawing.Point(12, 87);
            this.metroLabel1.Name = "metroLabel1";
            this.metroLabel1.Size = new System.Drawing.Size(35, 20);
            this.metroLabel1.TabIndex = 106;
            this.metroLabel1.Text = "FTE:";
            // 
            // nudHourlySalary
            // 
            this.nudHourlySalary.DecimalPlaces = 2;
            this.nudHourlySalary.Location = new System.Drawing.Point(131, 46);
            this.nudHourlySalary.Margin = new System.Windows.Forms.Padding(4);
            this.nudHourlySalary.Name = "nudHourlySalary";
            this.nudHourlySalary.Size = new System.Drawing.Size(127, 22);
            this.nudHourlySalary.TabIndex = 96;
            // 
            // cbbAddContractPosition
            // 
            this.cbbAddContractPosition.FormattingEnabled = true;
            this.cbbAddContractPosition.Items.AddRange(new object[] {
            "RetailWorker",
            "DepotWorker",
            "Manager",
            "Admin",
            "Security",
            "None"});
            this.cbbAddContractPosition.Location = new System.Drawing.Point(131, 129);
            this.cbbAddContractPosition.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbbAddContractPosition.Name = "cbbAddContractPosition";
            this.cbbAddContractPosition.Size = new System.Drawing.Size(129, 24);
            this.cbbAddContractPosition.TabIndex = 95;
            // 
            // cbEndDate
            // 
            this.cbEndDate.AutoSize = true;
            this.cbEndDate.Location = new System.Drawing.Point(10, 210);
            this.cbEndDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.cbEndDate.Name = "cbEndDate";
            this.cbEndDate.Size = new System.Drawing.Size(79, 17);
            this.cbEndDate.TabIndex = 94;
            this.cbEndDate.Text = "End date:";
            this.cbEndDate.UseVisualStyleBackColor = true;
            // 
            // metroLabel8
            // 
            this.metroLabel8.AutoSize = true;
            this.metroLabel8.Location = new System.Drawing.Point(12, 46);
            this.metroLabel8.Name = "metroLabel8";
            this.metroLabel8.Size = new System.Drawing.Size(94, 20);
            this.metroLabel8.TabIndex = 85;
            this.metroLabel8.Text = "Hourly Salary:";
            // 
            // lblPosition
            // 
            this.lblPosition.AutoSize = true;
            this.lblPosition.Location = new System.Drawing.Point(12, 129);
            this.lblPosition.Name = "lblPosition";
            this.lblPosition.Size = new System.Drawing.Size(59, 20);
            this.lblPosition.TabIndex = 82;
            this.lblPosition.Text = "Position:";
            // 
            // btnAddContract
            // 
            this.btnAddContract.BackColor = System.Drawing.Color.DarkCyan;
            this.btnAddContract.Location = new System.Drawing.Point(44, 256);
            this.btnAddContract.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddContract.Name = "btnAddContract";
            this.btnAddContract.Size = new System.Drawing.Size(250, 53);
            this.btnAddContract.TabIndex = 92;
            this.btnAddContract.Text = "Add Contract";
            this.btnAddContract.UseVisualStyleBackColor = false;
            this.btnAddContract.Click += new System.EventHandler(this.btnAddContract_Click);
            // 
            // metroLabel9
            // 
            this.metroLabel9.AutoSize = true;
            this.metroLabel9.Location = new System.Drawing.Point(10, 169);
            this.metroLabel9.Name = "metroLabel9";
            this.metroLabel9.Size = new System.Drawing.Size(71, 20);
            this.metroLabel9.TabIndex = 87;
            this.metroLabel9.Text = "Start date:";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Location = new System.Drawing.Point(115, 169);
            this.dtpStartDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(192, 22);
            this.dtpStartDate.TabIndex = 89;
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Location = new System.Drawing.Point(115, 210);
            this.dtpEndDate.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(192, 22);
            this.dtpEndDate.TabIndex = 90;
            // 
            // pictureBox7
            // 
            this.pictureBox7.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox7.Image")));
            this.pictureBox7.Location = new System.Drawing.Point(26, 11);
            this.pictureBox7.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox7.Name = "pictureBox7";
            this.pictureBox7.Size = new System.Drawing.Size(353, 73);
            this.pictureBox7.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox7.TabIndex = 93;
            this.pictureBox7.TabStop = false;
            // 
            // AddContractForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(391, 450);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.pictureBox7);
            this.Name = "AddContractForm";
            this.Text = "AddContractForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nudHourlySalary)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox7)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown nudHourlySalary;
        private System.Windows.Forms.ComboBox cbbAddContractPosition;
        private MetroFramework.Controls.MetroCheckBox cbEndDate;
        private System.Windows.Forms.PictureBox pictureBox7;
        private MetroFramework.Controls.MetroLabel metroLabel8;
        private MetroFramework.Controls.MetroLabel lblPosition;
        private System.Windows.Forms.Button btnAddContract;
        private MetroFramework.Controls.MetroLabel metroLabel9;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.ComboBox cmbFTE;
        private MetroFramework.Controls.MetroLabel metroLabel1;
    }
}