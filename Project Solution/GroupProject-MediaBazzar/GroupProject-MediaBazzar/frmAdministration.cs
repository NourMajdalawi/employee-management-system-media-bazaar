﻿using Classes;
using System;
using System.Collections.Generic;
using System.Windows.Forms;

namespace GroupProject_MediaBazzar
{
    public partial class frmAdministration : Form
    {
        Store store;
        Shift sd;

        public frmAdministration(string loggedInName, Store store)
        {
            //Initialize, show the name of the employee that is logged in and show all employees in the datagridview
            InitializeComponent();
            lbLoggedInAs.Text += loggedInName;
            this.store = store;
            sd = new Shift();
            dgvTemplateView.DataSource = store.GetTemplates();
            ShowAllEmployees();
            InitializeCombobox();
            RefreshDepartments();
        }

        public void InitializeCombobox()
        {
            List<string> departments = store.DepartmentSelectInitialize();
            cbbFloor.DataSource = departments;
            foreach (string dpt in departments)
            {
                cbbFloor.DisplayMember = dpt.ToString();
            }
        }

        public void ShowAllEmployees()
        {
            //Clear the datagridview
            dgvEmployeeInformation.Rows.Clear();
            dgvEditEmployeeDetails.Rows.Clear();
            string input = tbInput.Text;

            //Get all employees and add them to the datagridview
            List<Employee> employees = store.GetFilteredEmployees(input);
            foreach (Employee employee in employees)
            {
                dgvEditEmployeeDetails.Rows.Add(employee.id, employee.firstName, employee.lastName, employee.gender, employee.bsn, employee.birthDate, employee.emailAddress,
                    employee.nationality, employee.spokenLanguagesAsString(), employee.region, employee.zipcode, employee.streetName, employee.phonenumber, employee.emergencyPhonenumber);
            }
        }

        private void btnLogOut_Click(object sender, EventArgs e)
        {
            //Log out the user and reopen the login screen
            frmLogin frmLogin = new frmLogin();
            frmLogin.Show();
            this.Close();
        }
        private void btnAssignEmployee_Click(object sender, EventArgs e)
        {
            if (rbMorning.Checked == false && rbEvening.Checked == false && rbAfternoon.Checked == false || cbbFloor.SelectedItem == null)
            {
                MessageBox.Show("Not enough data was selected to assign a shift.");
                return;
            }


            if (mcbSelectMultiple.Checked == true && lbSelectedEmployees.Items.Count != 0)
            {
                int succesCount = lbSelectedEmployees.Items.Count;
                int returnCount = 0;
                foreach (string item in lbSelectedEmployees.Items)
                {
                    string[] employees = item.Split(' ');
                    if (AddShift(Convert.ToInt32(employees[2])))
                    {
                        returnCount++;
                    }
                }
                if (returnCount == succesCount)
                {
                    MessageBox.Show("Shifts added successfully");
                    GetAllAvailableEmployees();
                    lbSelectedEmployees.Items.Clear();
                }
                else
                {
                    MessageBox.Show("Failed to add 1 or more shifts.");
                }
            }
            else if (mcbSelectMultiple.Checked == false)
            {
                int selectedrowindex = dgvEmployeeInformation.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dgvEmployeeInformation.Rows[selectedrowindex];
                if (AddShift(Convert.ToInt32(selectedRow.Cells["id"].Value)))
                {
                    MessageBox.Show("Shift added succesfully");
                    GetAllAvailableEmployees();
                }
                else
                {
                    MessageBox.Show("Failed to add shift.");
                }
            }
            else { MessageBox.Show("When selecting multiple employees, selection can not be zero."); }

        }

        private bool AddShift(int employeeID)
        {
            //Get the date and what daysegment needs to be used
            DateTime date = dtpShiftDate.Value;
            string daySegment = "";

            if (rbMorning.Checked)
            { daySegment = "morning"; }
            else if (rbAfternoon.Checked)
            { daySegment = "afternoon"; }
            else if (rbEvening.Checked)
            { daySegment = "evening"; }
            else
            { MessageBox.Show("please select day segment"); }

            //Get what floor needs to be used and what employee is selected
            try
            {
                string floor = cbbFloor.SelectedItem.ToString();
                string employeeName = (store.GetEmployeeById(employeeID).firstName.ToLower() + " " + store.GetEmployeeById(employeeID).lastName.ToLower());

                //Add the employee to a shift and check if it was succesful
                if (store.GetEmployeeById(employeeID) != null)
                {
                    Shift shift = new Shift(-1, store.GetEmployeeById(employeeID).id, date, daySegment, floor);
                    if (store.AddShift(shift))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else return false;
            }
            catch (NullReferenceException)
            {
                return false;
            }
        }



        private DateTime lowerBoundSet(DateTime date)
        {
            while (date.DayOfWeek != DayOfWeek.Monday)
            {
                date = date.AddDays(-1);
            }
            return date;
        }

        private DateTime upperBoundSet(DateTime date)
        {
            while (date.DayOfWeek != DayOfWeek.Sunday)
            {
                date = date.AddDays(1);
            }
            return date;
        }

        private void dgvEmployeeInformation_DoubleClick(object sender, EventArgs e)
        {
            if (mcbSelectMultiple.Checked)
            {
                int selectedrowindex = dgvEmployeeInformation.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dgvEmployeeInformation.Rows[selectedrowindex];
                int id = Convert.ToInt32(selectedRow.Cells["id"].Value);
                string selectedName = (store.GetEmployeeById(id).firstName + " " + store.GetEmployeeById(id).lastName + " " + id.ToString());
                if (lbSelectedEmployees.Items.Contains(selectedName) != true)
                {
                    lbSelectedEmployees.Items.Add(selectedName);
                }
                else MessageBox.Show("This employee has already been selected");
            }
        }




        private void cbbFloor_SelectedIndexChanged(object sender, EventArgs e)
        {
            GetAllAvailableEmployees();
        }
        private void mcbSelMult_CheckedChanged_1(object sender, EventArgs e)
        {
            if (mcbSelectMultiple.Checked)
            {
                lbSelectedEmployees.Visible = true;
            }
            else
            {
                lbSelectedEmployees.Visible = false;
                lbSelectedEmployees.Items.Clear();
            }
        }


        private void GetAllAvailableEmployees()
        {
            //check voor floor
            string floor = cbbFloor.SelectedItem.ToString().ToLower();
            string input = "SA|" + floor;
            dgvEmployeeInformation.Rows.Clear();

            List<Employee> employees = store.GetFilteredEmployees(input);

            //check voor date
            DateTime date = dtpShiftDate.Value.Date;
            //check voor daysegment
            string daySegment = "";
            if (rbMorning.Checked)
            {
                daySegment = "morning";
            }
            else if (rbAfternoon.Checked)
            {
                daySegment = "afternoon";
            }
            else if (rbEvening.Checked)
            {
                daySegment = "evening";
            }


            foreach (Employee employee in employees)
            {
                if (employee.GetActiveContract() == null)
                {
                    return;
                }
                double fte = employee.GetActiveContract().fte;
                double fteAssigned = 0;
                DateTime lowerBound = lowerBoundSet(dtpShiftDate.Value.Date);
                DateTime upperBound = upperBoundSet(dtpShiftDate.Value.Date);
                foreach (Shift shift in store.GetShiftsBetween(lowerBound, upperBound))
                {
                    if (shift.employeeId == employee.id)
                    {
                        fteAssigned += 0.1;
                    }

                }

                if (store.SearchForShift(employee.id, floor, daySegment, date) == false)
                {
                    if (fte > fteAssigned)
                    {
                        double remainingFte = fte - fteAssigned;
                        dgvEmployeeInformation.Rows.Add(employee.id, employee.firstName, employee.lastName, fte, remainingFte);
                    }
                }
            }

        }

        private void DtpShiftDate_ValueChanged(object sender, EventArgs e)
        {
            GetAllAvailableEmployees();
        }

        private void RbMorning_CheckedChanged(object sender, EventArgs e)
        {
            GetAllAvailableEmployees();
        }

        private void RbAfternoon_CheckedChanged(object sender, EventArgs e)
        {
            GetAllAvailableEmployees();
        }

        private void RbEvening_CheckedChanged(object sender, EventArgs e)
        {
            GetAllAvailableEmployees();
        }

        private void btnSearchEmployee_Click_1(object sender, EventArgs e)
        {
            ShowAllEmployees();
        }

        private void btnEditEmplyeeData_Click(object sender, EventArgs e)
        {
            try
            {
                //Retrieve the currently selected employee's id from the datagridview and open the editemployee form for that employee
                int selectedrowindex = dgvEditEmployeeDetails.SelectedCells[0].RowIndex;
                DataGridViewRow selectedRow = dgvEditEmployeeDetails.Rows[selectedrowindex];
                int id = Convert.ToInt32(selectedRow.Cells["employeeid"].Value);
                frmEditEmployee editEmployee = new frmEditEmployee(store, store.GetEmployeeById(id));
                editEmployee.Show();

            }
            catch
            {
                //Warn users if they didn't select an employee to edit
                MessageBox.Show("Please select row to modify");
            }
        }

        private void OpenShiftOverview()
        {

            frmShiftOverview so = new frmShiftOverview(store, store.GetShifts());
            so.Show();
        }

        private void btnOverview_Click(object sender, EventArgs e)
        {
            OpenShiftOverview();
        }

        private void btnAddDepartment_Click(object sender, EventArgs e)
        {
            if (cbbPosition.SelectedItem == null)
            {
                MessageBox.Show("Please ensure all fields are properly filled.");
                return;
            }
            Position position = (Position)Enum.Parse(typeof(Position), cbbPosition.SelectedItem.ToString());
            string departmentName = tbDepartmentName.Text;
            int expectedRequiredShiftsPerWeek = Convert.ToInt32(nudRequiredEmployees.Value);
            if (departmentName == null || departmentName == "")
            {
                MessageBox.Show("Please ensure all fields are properly filled.");
                return;
            }
            if (expectedRequiredShiftsPerWeek == 0)
            {
                MessageBox.Show("Please ensure all fields are properly filled.");
                return;
            }
            Department department = new Department(departmentName, position, expectedRequiredShiftsPerWeek);
            store.AddDepartment(department);
            RefreshDepartments();

        }

        private void btnEditDepartment_Click(object sender, EventArgs e)
        {
            if (lbxDepartments.SelectedItem == null)
            {
                MessageBox.Show("Please select a department from the list first.");
                return;
            }
            List<Department> departments = store.GetAllDepartments();
            int id = departments[lbxDepartments.SelectedIndex].Id;
            Position position = (Position)Enum.Parse(typeof(Position), cbbPosition.SelectedItem.ToString());
            string departmentName = tbDepartmentName.Text;
            int expectedRequiredShiftsPerWeek = Convert.ToInt32(nudRequiredEmployees.Value);
            Department department = new Department(id, departmentName, position, expectedRequiredShiftsPerWeek);
            store.EditDepartment(department);
            RefreshDepartments();
        }

        private void btnDeleteDepartment_Click(object sender, EventArgs e)
        {
            List<Department> departments = store.GetAllDepartments();
            int id = departments[lbxDepartments.SelectedIndex].Id;
            store.DeleteDepartment(id);
            RefreshDepartments();
        }

        public void RefreshDepartments()
        {
            lbxDepartments.Items.Clear();
            foreach (Department department in store.GetAllDepartments())
            {
                lbxDepartments.Items.Add(department.ToString());
            }
            cbbPosition.Items.Clear();
            var values = Enum.GetValues(typeof(Position));
            foreach (Position position in values)
            {
                cbbPosition.Items.Add(position.ToString());
            }
            tbDepartmentName.Clear();
            nudRequiredEmployees.Value = 0;

        }

        private void lbxDepartments_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                List<Department> departments = store.GetAllDepartments();
                tbDepartmentName.Text = departments[lbxDepartments.SelectedIndex].Name;
                cbbPosition.SelectedItem = departments[lbxDepartments.SelectedIndex].position;
                nudRequiredEmployees.Value = departments[lbxDepartments.SelectedIndex].ExpectedRequiredShiftsPerWeek;
            }
            catch
            {

            }

        }

        private void btnGenerateAutoSchedule_Click(object sender, EventArgs e)
        {
            AutoScheduler autoScheduler = new AutoScheduler();
            int selectedrowindex = dgvTemplateView.SelectedCells[0].RowIndex;
            DataGridViewRow selectedRow = dgvTemplateView.Rows[selectedrowindex];
            int templateId = Convert.ToInt32(selectedRow.Cells["id"].Value);
            DateTime lowerBound = lowerBoundSet(dtpAutoScheduler.Value);
            DateTime upperBound = upperBoundSet(dtpAutoScheduler.Value);
            MessageBox.Show("The scheduler is now starting, please wait this can take a couple of minutes to generate. Close this message in order to proceed");

            if (store.CreateTemporaryTable())
            {
                List<TemplateShift> templateShifts = autoScheduler.GenerateSchedule(templateId, lowerBound, upperBound);
                frmShiftOverview shiftOverviewForm = new frmShiftOverview(store, templateShifts, lowerBound, upperBound);
                shiftOverviewForm.Show();
            }
            else
            {
                store.DeleteTemporaryTable();
                MessageBox.Show("Ann error occured please try again!");
            }
        }
    }
}
