﻿namespace GroupProject_MediaBazzar
{
    partial class frmLogin
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmLogin));
            this.panel1 = new System.Windows.Forms.Panel();
            this.MinimizeLogInPage = new System.Windows.Forms.Label();
            this.CloseLogInPage = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.tbUserName = new MetroFramework.Controls.MetroTextBox();
            this.tbPass = new MetroFramework.Controls.MetroTextBox();
            this.btnLogIn = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.MinimizeLogInPage);
            this.panel1.Controls.Add(this.CloseLogInPage);
            this.panel1.Location = new System.Drawing.Point(12, 2);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(397, 46);
            this.panel1.TabIndex = 0;
            // 
            // MinimizeLogInPage
            // 
            this.MinimizeLogInPage.AutoSize = true;
            this.MinimizeLogInPage.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.MinimizeLogInPage.Location = new System.Drawing.Point(339, 11);
            this.MinimizeLogInPage.Name = "MinimizeLogInPage";
            this.MinimizeLogInPage.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MinimizeLogInPage.Size = new System.Drawing.Size(19, 21);
            this.MinimizeLogInPage.TabIndex = 1;
            this.MinimizeLogInPage.Text = "-";
            this.MinimizeLogInPage.Click += new System.EventHandler(this.MinimizeLogInPage_Click);
            // 
            // CloseLogInPage
            // 
            this.CloseLogInPage.AutoSize = true;
            this.CloseLogInPage.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.CloseLogInPage.Location = new System.Drawing.Point(373, 11);
            this.CloseLogInPage.Name = "CloseLogInPage";
            this.CloseLogInPage.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.CloseLogInPage.Size = new System.Drawing.Size(23, 21);
            this.CloseLogInPage.TabIndex = 0;
            this.CloseLogInPage.Text = "X";
            this.CloseLogInPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.CloseLogInPage.Click += new System.EventHandler(this.CloseLogInPage_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(83, 89);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(252, 150);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 11;
            this.pictureBox1.TabStop = false;
            // 
            // tbUserName
            // 
            this.tbUserName.Location = new System.Drawing.Point(128, 288);
            this.tbUserName.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbUserName.Name = "tbUserName";
            this.tbUserName.Size = new System.Drawing.Size(253, 30);
            this.tbUserName.TabIndex = 12;
            // 
            // tbPass
            // 
            this.tbPass.Location = new System.Drawing.Point(129, 337);
            this.tbPass.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.tbPass.Name = "tbPass";
            this.tbPass.PasswordChar = '*';
            this.tbPass.Size = new System.Drawing.Size(252, 30);
            this.tbPass.TabIndex = 13;
            // 
            // btnLogIn
            // 
            this.btnLogIn.BackColor = System.Drawing.Color.DarkCyan;
            this.btnLogIn.Location = new System.Drawing.Point(83, 400);
            this.btnLogIn.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnLogIn.Name = "btnLogIn";
            this.btnLogIn.Size = new System.Drawing.Size(252, 50);
            this.btnLogIn.TabIndex = 83;
            this.btnLogIn.Text = "Login";
            this.btnLogIn.UseVisualStyleBackColor = false;
            this.btnLogIn.Click += new System.EventHandler(this.btnLogIn_Click_1);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(17, 288);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(81, 17);
            this.label1.TabIndex = 86;
            this.label1.Text = "User name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(17, 337);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(73, 17);
            this.label2.TabIndex = 87;
            this.label2.Text = "Password:";
            // 
            // frmLogin
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.WhiteSmoke;
            this.ClientSize = new System.Drawing.Size(407, 510);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnLogIn);
            this.Controls.Add(this.tbPass);
            this.Controls.Add(this.tbUserName);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.panel1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "frmLogin";
            this.Text = "LoginPage";
            this.Load += new System.EventHandler(this.frmLogin_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label MinimizeLogInPage;
        private System.Windows.Forms.Label CloseLogInPage;
        private System.Windows.Forms.PictureBox pictureBox1;
        private MetroFramework.Controls.MetroTextBox tbUserName;
        private MetroFramework.Controls.MetroTextBox tbPass;
        private System.Windows.Forms.Button btnLogIn;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}