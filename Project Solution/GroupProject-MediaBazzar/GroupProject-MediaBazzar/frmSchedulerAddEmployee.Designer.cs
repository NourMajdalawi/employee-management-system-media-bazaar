﻿namespace GroupProject_MediaBazzar
{
    partial class frmSchedulerAddEmployee
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvEmployeesAvailableToAdd = new System.Windows.Forms.DataGridView();
            this.id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.firstName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lastName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.cbbShiftSelect = new System.Windows.Forms.ComboBox();
            this.btnAddToScheduler = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeesAvailableToAdd)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvEmployeesAvailableToAdd
            // 
            this.dgvEmployeesAvailableToAdd.AllowUserToAddRows = false;
            this.dgvEmployeesAvailableToAdd.AllowUserToDeleteRows = false;
            this.dgvEmployeesAvailableToAdd.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvEmployeesAvailableToAdd.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvEmployeesAvailableToAdd.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.id,
            this.firstName,
            this.lastName});
            this.dgvEmployeesAvailableToAdd.Location = new System.Drawing.Point(31, 100);
            this.dgvEmployeesAvailableToAdd.Name = "dgvEmployeesAvailableToAdd";
            this.dgvEmployeesAvailableToAdd.ReadOnly = true;
            this.dgvEmployeesAvailableToAdd.RowHeadersWidth = 51;
            this.dgvEmployeesAvailableToAdd.RowTemplate.Height = 24;
            this.dgvEmployeesAvailableToAdd.Size = new System.Drawing.Size(737, 309);
            this.dgvEmployeesAvailableToAdd.TabIndex = 0;
            // 
            // id
            // 
            this.id.HeaderText = "id";
            this.id.MinimumWidth = 6;
            this.id.Name = "id";
            this.id.ReadOnly = true;
            // 
            // firstName
            // 
            this.firstName.HeaderText = "firstName";
            this.firstName.MinimumWidth = 6;
            this.firstName.Name = "firstName";
            this.firstName.ReadOnly = true;
            // 
            // lastName
            // 
            this.lastName.HeaderText = "lastName";
            this.lastName.MinimumWidth = 6;
            this.lastName.Name = "lastName";
            this.lastName.ReadOnly = true;
            // 
            // cbbShiftSelect
            // 
            this.cbbShiftSelect.FormattingEnabled = true;
            this.cbbShiftSelect.Items.AddRange(new object[] {
            "mondayMorning",
            "mondayAfternoon",
            "mondayEvening",
            "tuesdayMorning",
            "tuesdayAfternoon",
            "tuesdayEvening",
            "wednesdayMorning",
            "wednesdayAfternoon",
            "wednesdayEvening",
            "thursdayMorning",
            "thursdayAfternoon",
            "thursdayEvening",
            "fridayMorning",
            "fridayAfternoon",
            "fridayEvening",
            "saturdayMorning",
            "saturdayAfternoon",
            "saturdayEvening",
            "sundayMorning",
            "sundayAfternoon",
            "sundayEvening"});
            this.cbbShiftSelect.Location = new System.Drawing.Point(31, 12);
            this.cbbShiftSelect.Name = "cbbShiftSelect";
            this.cbbShiftSelect.Size = new System.Drawing.Size(404, 24);
            this.cbbShiftSelect.TabIndex = 1;
            this.cbbShiftSelect.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // btnAddToScheduler
            // 
            this.btnAddToScheduler.BackColor = System.Drawing.Color.DarkCyan;
            this.btnAddToScheduler.Location = new System.Drawing.Point(31, 50);
            this.btnAddToScheduler.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddToScheduler.Name = "btnAddToScheduler";
            this.btnAddToScheduler.Size = new System.Drawing.Size(189, 35);
            this.btnAddToScheduler.TabIndex = 97;
            this.btnAddToScheduler.Text = "Add to Schedule";
            this.btnAddToScheduler.UseVisualStyleBackColor = false;
            this.btnAddToScheduler.Click += new System.EventHandler(this.btnAddToScheduler_Click);
            // 
            // frmSchedulerAddEmployee
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.btnAddToScheduler);
            this.Controls.Add(this.cbbShiftSelect);
            this.Controls.Add(this.dgvEmployeesAvailableToAdd);
            this.Name = "frmSchedulerAddEmployee";
            this.Text = "frmSchedulerAddEmployee";
            ((System.ComponentModel.ISupportInitialize)(this.dgvEmployeesAvailableToAdd)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvEmployeesAvailableToAdd;
        private System.Windows.Forms.ComboBox cbbShiftSelect;
        private System.Windows.Forms.Button btnAddToScheduler;
        private System.Windows.Forms.DataGridViewTextBoxColumn id;
        private System.Windows.Forms.DataGridViewTextBoxColumn firstName;
        private System.Windows.Forms.DataGridViewTextBoxColumn lastName;
    }
}