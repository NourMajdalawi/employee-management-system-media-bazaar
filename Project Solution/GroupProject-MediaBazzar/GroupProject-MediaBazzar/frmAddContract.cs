﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroupProject_MediaBazzar
{
    public partial class frmAddContract : Form
    {
        Employee employee;
        public frmAddContract( Employee employee)
        {
            InitializeComponent();
            this.employee = employee;
            UpdateFTEcombobox();
        }

        private void btnAddContract_Click(object sender, EventArgs e)
        {
            //Get the selected employee from the datagridview

            try
            {
                //Get the values to add to the employee's contract
                int employeeId = this.employee.id;
                double hourlysalary = Convert.ToDouble(nudHourlySalary.Value);
                Position position = (Position)Enum.Parse(typeof(Position), cbbAddContractPosition.Text);
                DateTime startDate = dtpStartDate.Value;
                DateTime endDate = dtpEndDate.Value;
                double fte = Convert.ToDouble(cmbFTE.SelectedItem);
                Contract contract;
                if (cbEndDate.Checked)
                {
                    //If there needs to be an enddate, add it
                    contract = new Contract(false, true, 0, employeeId, position, hourlysalary, startDate, endDate, fte, true);
                }
                else
                {
                    //If no enddate is needed, ommit it
                    contract = new Contract(false, true, 0, employeeId, position, hourlysalary, startDate, fte,true);
                }
                //Add the contract to the employee
                employee.AddContract(contract);

                MessageBox.Show("Employee and contract have been succesfully added.");
                this.Close();
            }
            catch
            {
                MessageBox.Show("Make sure you have all fields filled in correctly and an employee selected before attempting to add a contract.");
            }
        }
        public void UpdateFTEcombobox()
        {
            List<double> ftes = new List<double>();
            ftes.Add(0.1);
            ftes.Add(0.2);
            ftes.Add(0.3);
            ftes.Add(0.4);
            ftes.Add(0.5);
            ftes.Add(0.6);
            ftes.Add(0.7);
            ftes.Add(0.8);
            ftes.Add(0.9);
            ftes.Add(1.0);
            cmbFTE.DataSource = ftes;
            foreach (int i in ftes)
            {
                cmbFTE.DisplayMember = i.ToString();
            }
        }

       
    }
}