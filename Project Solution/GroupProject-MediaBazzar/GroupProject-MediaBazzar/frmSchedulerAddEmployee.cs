﻿using Classes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GroupProject_MediaBazzar
{
    public partial class frmSchedulerAddEmployee : Form
    {
        DataHelper dh;
        DateTime lower;
        DateTime upper;
        string Department = "";

        public frmSchedulerAddEmployee(string Department, DateTime lower, DateTime upper)
        {
            InitializeComponent();
            dh = new DataHelper();
            this.Department = Department;
            this.lower = lower;
            this.upper = upper;
        }

        private void btnAddToScheduler_Click(object sender, EventArgs e)
        {
            DateTime date = GetDate();
            int SelectedEmployeeRowIndex = 0;
            DataGridViewRow selectedEmployeeRow;
            int employeeid = -1;
            try
            {
                SelectedEmployeeRowIndex = dgvEmployeesAvailableToAdd.SelectedCells[0].RowIndex;
                selectedEmployeeRow = dgvEmployeesAvailableToAdd.Rows[SelectedEmployeeRowIndex];
                employeeid = Convert.ToInt32(selectedEmployeeRow.Cells["id"].Value);
            }
            catch
            {

            }
            if (cbbShiftSelect.SelectedItem == null)
            {
                MessageBox.Show("Please ensure all fields are properly filled.");
                return;
            }
            string daySegment = "";

            if (cbbShiftSelect.SelectedItem.ToString().Contains("Morning"))
            {
                daySegment = "morning";
            }
            else if (cbbShiftSelect.SelectedItem.ToString().Contains("Afternoon"))
            {
                daySegment = "afternoon";
            }
            else if (cbbShiftSelect.SelectedItem.ToString().Contains("Evening"))
            {
                daySegment = "evening";
            }
            if (dh.FindEmployeeInTemporaryShift(employeeid, cbbShiftSelect.SelectedItem.ToString()) == employeeid)
            {
                MessageBox.Show("This employee is already assigned to the scehule");
                return;
            }
            if (dh.InsertEmployeeIntoTemporaryScheduler(employeeid, Department, cbbShiftSelect.SelectedItem.ToString(), daySegment, date))
            {
                MessageBox.Show("Employee added to proposed schedule.");
                return;
            }
            else
            {
                MessageBox.Show("Something went wrong while adding the employee to the schedule");
            }
        }
        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<Employee> employees;
            List<Availability> availabilities;


            availabilities = dh.GetAllAvailability();
            employees = dh.GetActiveEmployeesForDepartment(Department);
            employees = AddAvailabilityToEmployees(availabilities, employees);
            dgvEmployeesAvailableToAdd.Rows.Clear();

            foreach (Employee employee in employees)
            {
                double assignedFTE = dh.GetAssignedEmployeeFTEInTemporaryShift(employee.id);
                Contract currentContract = employee.GetActiveContract();
                if (employee.IsAvailable(cbbShiftSelect.SelectedItem.ToString()))
                {
                    if (!(dh.FindEmployeeInTemporaryShift(employee.id, cbbShiftSelect.SelectedItem.ToString()) == employee.id))
                    {
                        if (currentContract.fte >= assignedFTE)
                        {
                            dgvEmployeesAvailableToAdd.Rows.Add(employee.id, employee.firstName, employee.lastName);
                        }
                    }
                }
            }
        }

        public List<Employee> AddAvailabilityToEmployees(List<Availability> availabilities, List<Employee> employees)
        {
            foreach (Employee employee in employees)
            {
                foreach (Availability availbility in availabilities)
                {
                    if (employee.id == availbility.EmployeeId)
                    {
                        employee.availability = availbility;
                    }
                }
            }
            return employees;
        }
        private DateTime GetDate()
        {
            if (cbbShiftSelect.SelectedItem == null)
            {
                return DateTime.Today;
            }
            List<DateTime> Dates = new List<DateTime>();
            DateTime date = lower;
            while (date <= upper)
            {
                Dates.Add(date);
                date = date.AddDays(1);
            }
            foreach (DateTime datetime in Dates)
            {
                if (cbbShiftSelect.SelectedItem.ToString().Contains(datetime.DayOfWeek.ToString().ToLower()))
                {
                    return datetime;
                }
            }
            return DateTime.Today;
        }
    }
}
