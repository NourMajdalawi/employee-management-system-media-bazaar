﻿namespace GroupProject_MediaBazzar
{
    partial class frmShiftOverview
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmShiftOverview));
            this.dtpShiftView = new System.Windows.Forms.DateTimePicker();
            this.dgvShiftView = new System.Windows.Forms.DataGridView();
            this.btnRemove = new System.Windows.Forms.Button();
            this.gbRemove = new System.Windows.Forms.GroupBox();
            this.pictureBox5 = new System.Windows.Forms.PictureBox();
            this.gbShiftPerWeek = new System.Windows.Forms.GroupBox();
            this.btnPublish = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnAddToSchedule = new System.Windows.Forms.Button();
            this.btnRemoveFromSchedule = new System.Windows.Forms.Button();
            this.dgvAutoSchedulerView = new System.Windows.Forms.DataGridView();
            this.cbbDepartment = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.dgvShiftView)).BeginInit();
            this.gbRemove.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).BeginInit();
            this.gbShiftPerWeek.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAutoSchedulerView)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpShiftView
            // 
            this.dtpShiftView.Location = new System.Drawing.Point(127, 51);
            this.dtpShiftView.Name = "dtpShiftView";
            this.dtpShiftView.Size = new System.Drawing.Size(185, 22);
            this.dtpShiftView.TabIndex = 0;
            this.dtpShiftView.ValueChanged += new System.EventHandler(this.dtpShiftView_ValueChanged);
            // 
            // dgvShiftView
            // 
            this.dgvShiftView.AllowUserToAddRows = false;
            this.dgvShiftView.AllowUserToDeleteRows = false;
            this.dgvShiftView.AllowUserToResizeColumns = false;
            this.dgvShiftView.AllowUserToResizeRows = false;
            this.dgvShiftView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvShiftView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvShiftView.Location = new System.Drawing.Point(24, 274);
            this.dgvShiftView.Name = "dgvShiftView";
            this.dgvShiftView.RowHeadersWidthSizeMode = System.Windows.Forms.DataGridViewRowHeadersWidthSizeMode.AutoSizeToAllHeaders;
            this.dgvShiftView.RowTemplate.Height = 24;
            this.dgvShiftView.Size = new System.Drawing.Size(1146, 363);
            this.dgvShiftView.TabIndex = 1;
            // 
            // btnRemove
            // 
            this.btnRemove.BackColor = System.Drawing.Color.DarkCyan;
            this.btnRemove.Location = new System.Drawing.Point(36, 35);
            this.btnRemove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemove.Name = "btnRemove";
            this.btnRemove.Size = new System.Drawing.Size(226, 61);
            this.btnRemove.TabIndex = 0;
            this.btnRemove.Text = "Remove employee from shift";
            this.btnRemove.UseVisualStyleBackColor = false;
            this.btnRemove.Click += new System.EventHandler(this.btnRemove_Click_1);
            // 
            // gbRemove
            // 
            this.gbRemove.BackColor = System.Drawing.Color.White;
            this.gbRemove.Controls.Add(this.btnRemove);
            this.gbRemove.Location = new System.Drawing.Point(862, 129);
            this.gbRemove.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbRemove.Name = "gbRemove";
            this.gbRemove.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.gbRemove.Size = new System.Drawing.Size(308, 121);
            this.gbRemove.TabIndex = 90;
            this.gbRemove.TabStop = false;
            this.gbRemove.Text = "Remove ";
            // 
            // pictureBox5
            // 
            this.pictureBox5.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox5.Image")));
            this.pictureBox5.Location = new System.Drawing.Point(16, 21);
            this.pictureBox5.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox5.Name = "pictureBox5";
            this.pictureBox5.Size = new System.Drawing.Size(92, 94);
            this.pictureBox5.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox5.TabIndex = 91;
            this.pictureBox5.TabStop = false;
            // 
            // gbShiftPerWeek
            // 
            this.gbShiftPerWeek.Controls.Add(this.pictureBox5);
            this.gbShiftPerWeek.Controls.Add(this.dtpShiftView);
            this.gbShiftPerWeek.Location = new System.Drawing.Point(24, 135);
            this.gbShiftPerWeek.Name = "gbShiftPerWeek";
            this.gbShiftPerWeek.Size = new System.Drawing.Size(340, 121);
            this.gbShiftPerWeek.TabIndex = 92;
            this.gbShiftPerWeek.TabStop = false;
            this.gbShiftPerWeek.Text = "Shifts per week";
            // 
            // btnPublish
            // 
            this.btnPublish.BackColor = System.Drawing.Color.DarkCyan;
            this.btnPublish.Location = new System.Drawing.Point(382, 221);
            this.btnPublish.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnPublish.Name = "btnPublish";
            this.btnPublish.Size = new System.Drawing.Size(446, 35);
            this.btnPublish.TabIndex = 1;
            this.btnPublish.Text = "Publish";
            this.btnPublish.UseVisualStyleBackColor = false;
            this.btnPublish.Click += new System.EventHandler(this.btnPublish_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Teal;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1264, 119);
            this.panel1.TabIndex = 93;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackColor = System.Drawing.Color.Black;
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(203, 121);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // btnAddToSchedule
            // 
            this.btnAddToSchedule.BackColor = System.Drawing.Color.DarkCyan;
            this.btnAddToSchedule.Location = new System.Drawing.Point(382, 173);
            this.btnAddToSchedule.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnAddToSchedule.Name = "btnAddToSchedule";
            this.btnAddToSchedule.Size = new System.Drawing.Size(202, 35);
            this.btnAddToSchedule.TabIndex = 95;
            this.btnAddToSchedule.Text = "Add To Schedule";
            this.btnAddToSchedule.UseVisualStyleBackColor = false;
            this.btnAddToSchedule.Click += new System.EventHandler(this.btnAddToSchedule_Click);
            // 
            // btnRemoveFromSchedule
            // 
            this.btnRemoveFromSchedule.BackColor = System.Drawing.Color.DarkCyan;
            this.btnRemoveFromSchedule.Location = new System.Drawing.Point(639, 173);
            this.btnRemoveFromSchedule.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.btnRemoveFromSchedule.Name = "btnRemoveFromSchedule";
            this.btnRemoveFromSchedule.Size = new System.Drawing.Size(189, 35);
            this.btnRemoveFromSchedule.TabIndex = 96;
            this.btnRemoveFromSchedule.Text = "Remove From Schedule";
            this.btnRemoveFromSchedule.UseVisualStyleBackColor = false;
            this.btnRemoveFromSchedule.Click += new System.EventHandler(this.btnRemoveFromSchedule_Click);
            // 
            // dgvAutoSchedulerView
            // 
            this.dgvAutoSchedulerView.AllowUserToAddRows = false;
            this.dgvAutoSchedulerView.AllowUserToDeleteRows = false;
            this.dgvAutoSchedulerView.AllowUserToResizeColumns = false;
            this.dgvAutoSchedulerView.AllowUserToResizeRows = false;
            this.dgvAutoSchedulerView.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvAutoSchedulerView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvAutoSchedulerView.Location = new System.Drawing.Point(24, 274);
            this.dgvAutoSchedulerView.Name = "dgvAutoSchedulerView";
            this.dgvAutoSchedulerView.ReadOnly = true;
            this.dgvAutoSchedulerView.RowHeadersWidth = 51;
            this.dgvAutoSchedulerView.RowTemplate.Height = 24;
            this.dgvAutoSchedulerView.Size = new System.Drawing.Size(1146, 363);
            this.dgvAutoSchedulerView.TabIndex = 100;
            // 
            // cbbDepartment
            // 
            this.cbbDepartment.FormattingEnabled = true;
            this.cbbDepartment.Location = new System.Drawing.Point(462, 135);
            this.cbbDepartment.Name = "cbbDepartment";
            this.cbbDepartment.Size = new System.Drawing.Size(279, 24);
            this.cbbDepartment.TabIndex = 102;
            this.cbbDepartment.SelectedIndexChanged += new System.EventHandler(this.cbPerDepartment_SelectedIndexChanged);
            // 
            // frmShiftOverview
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1264, 649);
            this.Controls.Add(this.cbbDepartment);
            this.Controls.Add(this.dgvAutoSchedulerView);
            this.Controls.Add(this.btnRemoveFromSchedule);
            this.Controls.Add(this.btnAddToSchedule);
            this.Controls.Add(this.btnPublish);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.gbShiftPerWeek);
            this.Controls.Add(this.gbRemove);
            this.Controls.Add(this.dgvShiftView);
            this.Name = "frmShiftOverview";
            this.Text = "ShiftOverview";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmShiftOverview_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.dgvShiftView)).EndInit();
            this.gbRemove.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox5)).EndInit();
            this.gbShiftPerWeek.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvAutoSchedulerView)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpShiftView;
        private System.Windows.Forms.DataGridView dgvShiftView;
        private System.Windows.Forms.Button btnRemove;
        private System.Windows.Forms.GroupBox gbRemove;
        private System.Windows.Forms.PictureBox pictureBox5;
        private System.Windows.Forms.GroupBox gbShiftPerWeek;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnPublish;
        private System.Windows.Forms.Button btnAddToSchedule;
        private System.Windows.Forms.Button btnRemoveFromSchedule;
        private System.Windows.Forms.DataGridView dgvAutoSchedulerView;
        private System.Windows.Forms.ComboBox cbbDepartment;
    }
}