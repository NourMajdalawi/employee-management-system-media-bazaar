﻿using System;

public enum DepartureType
{
    Quit,
    Fired,
    ContractChanged
}
