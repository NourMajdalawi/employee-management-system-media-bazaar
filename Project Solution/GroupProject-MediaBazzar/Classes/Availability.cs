﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public class Availability
    {
        public int EmployeeId { get; private set; }
        public bool mondayMorning   {get; private set;}  
        public bool mondayAfternoon   {get; private set;}
        public bool mondayEvening     {get; private set;}
        public bool tuesdayMorning    {get; private set;}
        public bool tuesdayAfternoon  {get; private set;}
        public bool tuesdayEvening    {get; private set;}
        public bool wednesdayMorning  {get; private set;}
        public bool wednesdayAfternoon{get; private set;}
        public bool wednesdayEvening  {get; private set;}
        public bool thursdayMorning   {get; private set;}
        public bool thursdayAfternoon {get; private set;}
        public bool thursdayEvening   {get; private set;}
        public bool fridayMorning     {get; private set;}
        public bool fridayAfternoon   {get; private set;}
        public bool fridayEvening     {get; private set;}
        public bool saturdayMorning   {get; private set;}
        public bool saturdayAfternoon {get; private set;}
        public bool saturdayEvening   {get; private set;}
        public bool sundayMorning     {get; private set;}
        public bool sundayAfternoon   {get; private set;}
        public bool sundayEvening     {get; private set;}

        public Availability(int employeeId, bool mondayMorning, bool mondayAfternoon, bool mondayEvening, bool tuesdayMorning, bool tuesdayAfternoon, bool tuesdayEvening, bool wednesdayMorning, bool wednesdayAfternoon, bool wednesdayEvening, bool thursdayMorning, bool thursdayAfternoon, bool thursdayEvening, bool fridayMorning, bool fridayAfternoon, bool fridayEvening, bool saturdayMorning, bool saturdayAfternoon, bool saturdayEvening, bool sundayMorning, bool sundayAfternoon, bool sundayEvening)
        {
            EmployeeId = employeeId;
            this.mondayMorning = mondayMorning;
            this.mondayAfternoon = mondayAfternoon;
            this.mondayEvening = mondayEvening;
            this.tuesdayMorning = tuesdayMorning;
            this.tuesdayAfternoon = tuesdayAfternoon;
            this.tuesdayEvening = tuesdayEvening;
            this.wednesdayMorning = wednesdayMorning;
            this.wednesdayAfternoon = wednesdayAfternoon;
            this.wednesdayEvening = wednesdayEvening;
            this.thursdayMorning = thursdayMorning;
            this.thursdayAfternoon = thursdayAfternoon;
            this.thursdayEvening = thursdayEvening;
            this.fridayMorning = fridayMorning;
            this.fridayAfternoon = fridayAfternoon;
            this.fridayEvening = fridayEvening;
            this.saturdayMorning = saturdayMorning;
            this.saturdayAfternoon = saturdayAfternoon;
            this.saturdayEvening = saturdayEvening;
            this.sundayMorning = sundayMorning;
            this.sundayAfternoon = sundayAfternoon;
            this.sundayEvening = sundayEvening;
        }

        public bool IsAvailable(string shift)
        {
            switch (shift.ToLower())
            {
                case "mondaymorning":
                    if(mondayMorning)
                    {
                        return true;
                    } return false;
                case "mondayafternoon":
                    if (mondayAfternoon)
                    {
                        return true;
                    }
                    return false;
                case "mondayevening":
                    if (mondayEvening)
                    {
                        return true;
                    }
                    return false;
                case "tuesdaymorning":
                    if (tuesdayMorning)
                    {
                        return true;
                    }
                    return false;
                case "tuesdayafternoon":
                    if (tuesdayAfternoon)
                    {
                        return true;
                    }
                    return false;
                case "tuesdayevening":
                    if (tuesdayEvening)
                    {
                        return true;
                    }
                    return false;
                case "wednesdaymorning":
                    if (wednesdayMorning)
                    {
                        return true;
                    }
                    return false;
                case "wednesdayafternoon":
                    if (wednesdayAfternoon)
                    {
                        return true;
                    }
                    return false;
                case "wednesdayevening":
                    if (wednesdayEvening)
                    {
                        return true;
                    }
                    return false;
                case "thursdaymorning":
                    if (thursdayMorning)
                    {
                        return true;
                    }
                    return false;
                case "thursdayafternoon":
                    if (thursdayAfternoon)
                    {
                        return true;
                    }
                    return false;
                case "thursdayevening":
                    if (thursdayEvening)
                    {
                        return true;
                    }
                    return false;
                case "fridaymorning":
                    if (fridayMorning)
                    {
                        return true;
                    }
                    return false;
                case "fridayafternoon":
                    if (fridayAfternoon)
                    {
                        return true;
                    }
                    return false;
                case "fridayevening":
                    if (fridayEvening)
                    {
                        return true;
                    }
                    return false;
                case "saturdaymorning":
                    if (saturdayMorning)
                    {
                        return true;
                    }
                    return false;
                case "saturdayafternoon":
                    if (saturdayAfternoon)
                    {
                        return true;
                    }
                    return false;
                case "saturdayevening":
                    if (saturdayEvening)
                    {
                        return true;
                    }
                    return false;
                case "sundaymorning":
                    if (sundayMorning)
                    {
                        return true;
                    }
                    return false;
                case "sundayafternoon":
                    if (sundayAfternoon)
                    {
                        return true;
                    }
                    return false;
                case "sundayevening":
                    if (sundayEvening)
                    {
                        return true;
                    }
                    return false;
                default:
                    return false;
            }
        }
    }
}
