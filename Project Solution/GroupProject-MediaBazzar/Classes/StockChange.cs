﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public class StockChange
    {
        public StockChange(int productBarcode, string reason)
        {
            this.productBarcode = productBarcode;
            this.reason = reason;
        }

        public int productBarcode { get; }
        public string reason { get; }

    }
}