﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GroupProject_MediaBazzar
{
    public class Department
    {
        public int Id { get; private set; }
        public string Name { get; private set; }
        public Position position { get; private set; }
        public int ExpectedRequiredShiftsPerWeek { get; private set; }

        public Department(string name, Position position, int expectedRequiredShiftsPerWeek)
        {
            Name = name;
            this.position = position;
            ExpectedRequiredShiftsPerWeek = expectedRequiredShiftsPerWeek;
        }

        public Department(int id, string name, Position position, int expectedRequiredShiftsPerWeek)
        {
            Id = id;
            Name = name;
            this.position = position;
            ExpectedRequiredShiftsPerWeek = expectedRequiredShiftsPerWeek;
        }

        public override string ToString()
        {
            return $"Department: {Name} - {position.ToString()}. Expected required shifts per week: {(ExpectedRequiredShiftsPerWeek).ToString()}";
        }

    }
}
