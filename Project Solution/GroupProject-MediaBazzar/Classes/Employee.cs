﻿using Classes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProject_MediaBazzar
{
    public class Employee
    {
        DataHelper dh;

        public int id { get; }
        public string username { get; }
        public string firstName { get; }
        public string lastName { get; }
        public Gender gender { get; }
        public string bsn { get; }
        public DateTime birthDate { get; }
        public string emailAddress { get; }
        public string nationality { get; }
        public string region { get; }
        public string zipcode { get; }
        public string streetName { get; }
        public string phonenumber { get; }
        public string emergencyPhonenumber { get; }
        public string password { get; }
        public bool spouse { get; }
        public Position currentPosition { get; private set; }
        public List<string> spokenLanguages { get; }
        public List<string> sickLeaves { get; }
        public List<string> workedShifts { get; }
        public List<Contract> contracts;
        public Availability availability;

        public string spokenLanguagesAsString()
        {
            string s = "";
            for (int i = 0; i < spokenLanguages.Count; i++)
            {
                s += spokenLanguages[i];
                if (i < spokenLanguages.Count - 1)
                {
                    s += ",";
                }
            }
            return s;
        }

        public Employee(int id, string username, string firstName, string lastName, Gender gender, DateTime birthDate, string nationality, string emailAddress, string bsn,
        string region, string zipcode, string streetName, string phonenumber, string emergencyPhonenumber, List<string> spokenLanguages, bool spouse)
        {
            //Check if id is bigger than 1 so we only assign it an id if we are retrieving it from the database
            if (id > -1)
            {
                this.id = id;
            }
            dh = new DataHelper();
            this.username = username;
            this.firstName = firstName;
            this.lastName = lastName;
            this.gender = gender;
            this.birthDate = birthDate;
            this.nationality = nationality;
            this.emailAddress = emailAddress;
            this.bsn = bsn;
            this.region = region;
            this.zipcode = zipcode;
            this.streetName = streetName;
            this.phonenumber = phonenumber;
           
            this.emergencyPhonenumber = emergencyPhonenumber;
            this.spokenLanguages = spokenLanguages;
           
            this.spouse = spouse;
            sickLeaves = new List<string>();
            workedShifts = new List<string>();
            contracts = new List<Contract>();
        }

        public Employee(int id, string username,string firstName, string lastName, Gender gender, DateTime birthDate, string nationality, string emailAddress, string bsn,
        string region, string zipcode, string streetName, string phonenumber, string password, string emergencyPhonenumber, List<string> spokenLanguages, bool spouse)
        {
            //Check if id is bigger than 1 so we only assign it an id if we are retrieving it from the database
            if (id > -1)
            {
                this.id = id;
            }
            dh = new DataHelper();
            this.username = username;
            this.firstName = firstName;
            this.lastName = lastName;
            this.gender = gender;
            this.birthDate = birthDate;
            this.nationality = nationality;
            this.emailAddress = emailAddress;
            this.bsn = bsn;
            this.region = region;
            this.zipcode = zipcode;
            this.streetName = streetName;
            this.phonenumber = phonenumber;
            this.password = password;
            this.emergencyPhonenumber = emergencyPhonenumber;
            this.spokenLanguages = spokenLanguages;

            this.spouse = spouse;
            sickLeaves = new List<string>();
            workedShifts = new List<string>();
            contracts = new List<Contract>();
 
        }
        public Employee()
        {
            //Only used in the dataHelper for edit employee details
        }

        public void AddContract(Contract contract)
        {
            //Add a contract to the contract table
            dh.AddContract(contract);
            currentPosition = contract.position;

            //update the employee's current position in the employee table
            dh.UpdateEmployeePosition(this, contract.position);
        }
        public bool AddNewContract(Contract contract)
        {
            Contract c = GetActiveContract();
            if (c == null)
            {
                if (!GetAllContracts().Exists(x => x.active == contract.active))
                {
                    //Add a contract to the contract table
                    dh.AddContract(contract);
                    currentPosition = contract.position;

                    //update the employee's current position in the employee table
                    dh.UpdateEmployeePosition(this, contract.position);
                    return true;
                }
            }
            return false;


        }
        public bool EditContract(int currentContractId, Contract contract)
        {
            //Edit contract
            if (contract.active)
            {
                dh.EditContract(currentContractId, contract);
                currentPosition = contract.position;

                //Update the employee's current position in the employee table
                dh.UpdateEmployeePosition(this, contract.position);
                return true;
            }
            return false;
        }
        public bool RenewContract(int currentContractId, Contract contract, DateTime endDate)
        {
            if (contract.active)
            {
                dh.RenewContract(currentContractId, contract, endDate);
                return true;
            }
            return false;
        }
        public bool EndContract(int contractId, DepartureType departureType, string departureReason, DateTime endDate)
        {
            Contract contract = GetContractById(contractId);
            if (contract.active)
            {
                //End contract in the contract table
                dh.EndContract(contractId, departureType, departureReason, endDate);
                currentPosition = Position.None;

                //Update the employee's current position in the employee table
                dh.UpdateEmployeePosition(this, Position.None);
                return true;
            }
            return false;

        }

        public Contract GetContractById(int contractId)
        {
            //Retrieve a contract from the database based on its id
            return dh.GetContractById(contractId);
        }
        public List<Contract> GetAllContracts()
        {
            //Get all contracts that belong to an employee based on the employeeid
            return dh.GetAllContracts(id);
        }

        //get the contract that not active to renew it again
        public Contract GetActiveContract()
        {
            //Get the currently active contract from the database
            List<Contract> contracts = dh.GetAllContracts(id);
            foreach (Contract contract in contracts)
            {
                if (contract.active)
                {
                    return contract;
                }
            }
            return null;
        }

        public bool IsAvailable(string shift)
        {
            return availability.IsAvailable(shift);
        }
    }
}
