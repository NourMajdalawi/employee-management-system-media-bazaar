﻿using Classes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text.RegularExpressions;

namespace GroupProject_MediaBazzar
{
    public class Store
    {
        public string name { get; }
        List<Employee> employees;
        List<Floor> floors;
        List<Shift> shifts;
        DataHelper dh;

        public Store(string name, DataHelper dh)
        {
            this.name = name;
            this.dh = dh;
            employees = new List<Employee>();
            floors = new List<Floor>();
            shifts = dh.GetAllShifts();
        }
        public bool ValidateEmail(string str)
        {
            return Regex.IsMatch(str, @"\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*");
        }
        public bool AddEmployee(Employee employee)
        {

            //Add an employee and return false if it didn't succeed
            if (ValidateEmail(employee.emailAddress))
            {
                if (!dh.GetBSN().Contains(employee.bsn))
                {
                    if (dh.AddEmployee(employee, this))
                    {
                        return true;
                    }
                }
            }
            return false;
        }



        public bool AddStockChange(StockChange stockchange)
        {
            //Add a stockchange and return false if it didn't succeed
            return dh.LogStockUpdate(stockchange);
        }

        public List<Employee> GetFilteredEmployees(string input)
        {
            string InputToParse = input;
            string subStr = "SA|";
            if (InputToParse.Contains(subStr))
            {
                string[] data = InputToParse.Split('|');
                if (data[1] == "manager")
                {
                    return dh.SearchForEmployees("pos|2");
                }
                else if (data[1] == "administration")
                {
                    return dh.SearchForEmployees("pos|3");
                }
                else if (data[1] == "depotworker")
                {
                    return dh.SearchForEmployees("pos|1");
                }
                else if (data[1] == "security")
                {
                    return dh.SearchForEmployees("pos|4");
                }
                else { return dh.SearchForEmployees("pos|0"); }
            }
            else { return dh.SearchForEmployees(input); }
            //Get all employees that pass the criteria

        }

        public DataTable GetShiftsBetweenForPosition(string position, DateTime lowerBound, DateTime upperBound)
        {
            return dh.GetShiftsBetweenForPosition(position, lowerBound, upperBound);
        }

        public DataTable GetShifts(DateTime lowerBound, DateTime upperBound)
        {
            return dh.GetAllShifts(lowerBound, upperBound);
        }

        public List<Shift> GetShiftsBetween( DateTime lowerBound, DateTime upperBound)
        {
            return dh.GetShiftsBetween( lowerBound, upperBound);
        }

        public List<Shift> GetShifts()
        {
            return shifts;
        }
        public DataTable GetTemplateShifts()
        {
            return dh.GetAllTemplateShifts();
        } 
        public DataTable GetTemplateShiftPerDepartment(string department)
        {
            return dh.GetTemplateShiftsPerDepartment(department);
        }

        public List<Product> GetProducts(string input)
        {
            //Get all products that pass the criteria
            return dh.SearchForProducts(input);
        }

        public Employee GetEmployeeById(int id)
        {
            //Get employee by ids
            return dh.GetEmployee(id);
        }

        public bool UpdateEmployeeInfo(Employee employee)
        {
            //Update an employee's details, return if it succeeded or not
            if (dh.UpdateEmployeeDetails(employee, this))
            {
                return true;
            }
            else return false;
        }

        public List<RestockRequest> GetAllrestockrequests()
        {
            //Get all restock requests
            return dh.GetRestockRequests();
        }
        public void ModifyStockAmount(int barcode, int amount)
        {
            //Modify a stock amount
            dh.ModifyStockAmount(barcode, amount);
        }

        public bool RemoveEmployee(int employeeId)
        {
            //Not currently implemented
            return false;
        }

        public bool PlaceRestockRequest(RestockRequest restockRequest)
        {
            //Place a restock request and return if it was succesful or not
            return dh.PlaceRestockRequest(restockRequest, this);
        }

        public void ConfirmRestockRequest(int id)
        {
            //Confirm the restock request
            dh.ConfirmRestockRequest(id);
        }

        public bool AddShift(Shift schedule)
        {
            //Add a shift and return if it was succesful or not
            return dh.AddShift(schedule);
        }

        public int GetNewEmployeeID()
        {
            return dh.GetNewEmployeeID();
        }

        public void RemoveEmployeeFromShift(int id, DateTime date, string daysegment)
        {
            dh.RemoveEmployeeFromShift(id, date, daysegment);
        }

        public List<string> DepartmentSelectInitialize()
        {
            return dh.GetAllDepartmentNames();
        }

        public bool SearchForShift(int employeeId, string floor, string daySegment, DateTime date)
        {
            return dh.SearchForShift(employeeId, floor, daySegment, date);
        }

        public int GetAmountOfEmployees()
        {
            return dh.GetAmountOfEmployees();
        }

        public List<Employee> GetActiveEmployeesForDepartment(string department)
        {
            return dh.GetActiveEmployeesForDepartment(department);
        }

        public double GetRequiredFteForDepartment(string department)
        {
            return dh.GetRequiredFteForDepartment(department);
        }
        public DataTable GetTemplates()
        {
            return dh.GetTemplates();
        }
        public bool AddAvaialbility(int id)
        {
            return dh.AddAvailaility(id);
        }
        #region TemporaryShiftTable
        public bool CreateTemporaryTable()
        {
           return dh.CreateTempraryTableInDB();
        } 
        public void DeleteTemporaryTable()
        {
             dh.deleteTempraryTableInDB();
        } 
        public void RemoveEmployeeFromTemporaryShift(int id, string daySegment)
        {
          dh.RemoveEmployeeFromTemporaryShift(id, daySegment);
        }
        #endregion

        public List<Department> GetAllDepartments()
        {
            return dh.GetAllDepartments();
        }

        public void AddDepartment(Department department)
        {
            dh.AddDepartment(department);
        }

        public void EditDepartment(Department department)
        {
            dh.EditDepartment(department);
        }

        public void DeleteDepartment(int id)
        {
            dh.DeleteDepartment(id);
        }

        public void PublishSchedule()
        {
            dh.PublishSchedule();
        }
    }
}
