﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProject_MediaBazzar
{
    public class Company
    {
        public string name { get; }

        List<Store> stores;
        public Company(string name)
        {
            this.name = name;
            stores = new List<Store>();
        }
        public void AddStore(Store store)
        {
            stores.Add(store);
        }
        public bool RemoveStore(string name)
        {
            //Not currently implemented
            return false;
        }
    }
}
