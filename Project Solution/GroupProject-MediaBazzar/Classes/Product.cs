﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProject_MediaBazzar
{
    public class Product
    {
		public int barcode { get; } 
		public string productName { get; } 
		public int modelNumber { get; } 
        public string category { get; }
        public string manufacturerBrand { get; }
		public double normalPrice { get; }
		public double promotionPrice { get; }
		public int stockInDepot { get; }
		public int stockOnShelves { get; }
		public int amountsold { get; }


        public Product(int barcode, string productName, int modelNumber, string manufacturerBrand, string category, double normalPrice,
                            double promotionPrice, int stockInDepot, int stockOnShelves, int amountsold)
        {
            this.barcode = barcode;
            this.productName = productName;
            this.modelNumber = modelNumber;
            this.manufacturerBrand = manufacturerBrand;
            this.normalPrice = normalPrice;
            this.promotionPrice = promotionPrice;
            this.stockInDepot = stockInDepot;
            this.stockOnShelves = stockOnShelves;
            this.category = category;
            this.amountsold = amountsold;
        }
	}
}
