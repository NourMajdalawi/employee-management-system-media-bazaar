﻿using GroupProject_MediaBazzar;
using Org.BouncyCastle.Crypto.Tls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics.Tracing;
using System.Text;
namespace Classes
{
    public class AutoScheduler
    {
        private DataHelper dh;
        private List<Template> templates;
        private Template template;
        private List<DateTime> shiftDates;
        List<TemplateShift> templateShifts;
        public AutoScheduler()
        {
            dh = new DataHelper();
            templates = new List<Template>();
        }

        #region Templates
        //Adding templates-------------------------------------------------------------------------------------------------------------------------------------------------------
        public void AddTemplate(Template template)
        {
            dh.AddTemplate(template);
        }

        //Getting templates-------------------------------------------------------------------------------------------------------------------------------------------------------
        public List<Template> GetAllTemplates()
        {
            //  templates = dh.GetAllTemplates();
            return templates;
        }
        public void LoadTemplate(int templateId)
        {
            GetAllTemplates();
            foreach (Template template in templates)
            {
                if (template.Id == templateId)
                {
                    this.template = template;
                    return;
                }
            }
            throw new ArgumentException("No template with this id was found");
        }

        //Editing templates-------------------------------------------------------------------------------------------------------------------------------------------------------

        #endregion

        #region Schedules
        //Adding schedules--------------------------------------------------------------------------------------------------------------------------------------------------------
        public List<TemplateShift> GenerateSchedule(int templateId, DateTime lowerBound, DateTime upperBound)
        {
            shiftDates = new List<DateTime>();
            DateTime date = lowerBound;
            while (date <= upperBound)
            {
                shiftDates.Add(date);
                date = date.AddDays(1);
            }

            List<string> departments = dh.GetAllDepartmentNames();
            List<Employee> employees;
            List<Availability> availabilities;
            templateShifts = dh.GetTemplateShifts(templateId);


            // 1 Select a department
            foreach (string department in departments)
            {
                //Get all employees for that department               
                availabilities = dh.GetAllAvailability();
                employees = dh.GetActiveEmployeesForDepartment(department);
                employees = AddAvailabilityToEmployees(availabilities, employees);

                //Loop through all templateshifts
                foreach (TemplateShift templateShift in templateShifts)
                {
                    if (templateShift.Department != department)
                    {
                        continue;
                    }
                    int requiredEmployees = templateShift.RequiredEmployees;
                    int assignedEmployeesCount = 0;

                    //Loop through all employees
                    foreach (Employee employee in employees)
                    {
                        //Check if required amount of employees has already been fulfilled
                        if (requiredEmployees <= assignedEmployeesCount)
                        {
                            //if done, continue to the next daysegment
                            break;
                        }

                        //Check if employee is available
                        if (!employee.IsAvailable(templateShift.DaySegment))
                        {
                            //if not available, check next employee
                            continue;
                        }
                        //Check if employee doesn't already have this shift
                        string temporaryDaySegment;
                        if (templateShift.DaySegment.ToLower().Contains("morning"))
                        {
                            temporaryDaySegment = "morning";
                        }
                        else if (templateShift.DaySegment.ToLower().Contains("afternoon"))
                        {
                            temporaryDaySegment = "afternoon";
                        }
                        else
                        {
                            temporaryDaySegment = "evening";
                        }
                        DateTime shiftDate = DateTime.Today;
                        foreach (DateTime shiftDay in shiftDates)
                        {
                            if (templateShift.DaySegment.Contains(shiftDay.DayOfWeek.ToString().ToLower()))
                            {
                                shiftDate = shiftDay;
                            }
                        }
                        if (dh.SearchForShift(employee.id, department, temporaryDaySegment, shiftDate))
                        {
                            //if this employee has already this shift, check the next
                            continue;
                        }
                        if (dh.SearchForVacationDay(employee.id, shiftDate))
                        {
                            //if this employee has a vacation day on this date, continue
                            continue;
                        }
                        //retrieve current max fte and used fte for the selected timeframe
                        double assignedFTE = dh.GetAssignedFTEForEmployee(employee.id, lowerBound, upperBound);
                        Contract currentContract = employee.GetActiveContract();

                        //Check if employee has FTE remaining
                        if (assignedFTE >= currentContract.fte)
                        {
                            //if not, check next employee
                            continue;
                        }
                        else
                        {
                            //If all checks pass, assign the employee
                            templateShift.AddEmployee(employee);
                            assignedEmployeesCount++;
                            string separatedSegment = templateShift.DaySegment;
                            if (separatedSegment.Contains("Morning"))
                            {
                                separatedSegment = "morning";
                            }
                            else if (separatedSegment.Contains("Afternoon"))
                            {
                                separatedSegment = "afternoon";
                            }
                            else if (separatedSegment.Contains("Evening"))
                            {
                                separatedSegment = "evening";
                            }
                            dh.InsertTempraryTemplateShifts(templateShift.RequiredEmployees, employee.id, shiftDate, templateShift.DaySegment, separatedSegment, department);
                        }
                        templateShift.Date = shiftDate;

                    }
                }
            }
            return templateShifts;
        }
        public void PublishSchedule(List<TemplateShift> templateShifts)
        {
            foreach (TemplateShift shift in templateShifts)
            {
                if (shift.DaySegment == "mondayMorning" || shift.DaySegment == "tuesdayMorning" || shift.DaySegment == "wednesdayMorning" || shift.DaySegment == "thursdayMorning" ||
                    shift.DaySegment == "fridayMorning" || shift.DaySegment == "saturdayMorning" || shift.DaySegment == "sundayMorning")
                {
                    shift.DaySegment = "morning";
                }
                else if (shift.DaySegment == "mondayAfternoon" || shift.DaySegment == "tuesdayAfternoon" || shift.DaySegment == "wednesdayAfternoon" || shift.DaySegment == "thursdayAfternoon" ||
                    shift.DaySegment == "fridayAfternoon" || shift.DaySegment == "saturdayAfternoon" || shift.DaySegment == "sundayAfternoon")
                {
                    shift.DaySegment = "afternoon";
                }
                else
                {
                    shift.DaySegment = "evening";
                }
                foreach (Employee employee in shift.Employees)
                {
                    dh.AddShift(employee.id, shift.Date, shift.DaySegment, shift.Department);
                }
            }
        }
        //Getting schedules-------------------------------------------------------------------------------------------------------------------------------------------------------

        //Editing schedules-------------------------------------------------------------------------------------------------------------------------------------------------------
        public void EditSchedule(List<TemplateShift> templateShifts)
        {

        }

        public List<Employee> AddAvailabilityToEmployees(List<Availability> availabilities, List<Employee> employees)
        {
            foreach (Employee employee in employees)
            {
                foreach (Availability availbility in availabilities)
                {
                    if (employee.id == availbility.EmployeeId)
                    {
                        employee.availability = availbility;
                    }
                }
            }
            return employees;
        }

        #endregion
    }
}
