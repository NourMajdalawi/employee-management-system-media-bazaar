﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GroupProject_MediaBazzar
{
    public class RestockRequest
    {
        public int id { get; }
        public string store { get; }
        public int productBarcode { get; }
        public int productAmount { get; }
        public string floor { get; }
        public DateTime date { get; }
        public int delivered { get; }

        public RestockRequest(int id, int productbarcode, int productamount, string floor, DateTime date)
        {
            //Check if id is bigger than -1, to know if we want to set it or not
            if (id > -1)
            {
                this.id = id;
            }
            this.productBarcode = productbarcode;
            this.productAmount = productamount;
            this.floor = floor;
            this.date = date;
        }
    }
}
