﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProject_MediaBazzar

{
    /// <summary>
    ///RANDOM PASS Generator
    ///USAGE: when creating a new profile for an employee
    ///
    ///TO BE USED in the Manager Form 
    /// </summary>

    public static class PasswordGenerator
    {
        public static List<string> GeneratePasswords(int length, int count, bool includeSymbols)
        {
            //Generate a random password from the possible characters given
            List<string> pwds = new List<string>();
            StringBuilder pass;
            int remainder = length;
            string noSymbolsPass = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            string symbolsPass = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890~!@#$%^&*()-+";
            if (includeSymbols != true)
            {
                Random rnd = new Random();
                while (count > 0)
                {
                    pass = new StringBuilder(length);
                    while (remainder > 0)
                    {
                        pass.Append(noSymbolsPass[rnd.Next(noSymbolsPass.Length)]);
                        remainder--;
                    }
                    remainder = length;
                    pwds.Add(pass.ToString());
                    count--;
                }
                return pwds;
            }
            else
            {
                Random rnd = new Random();
                while (count > 0)
                {
                    pass = new StringBuilder(length);
                    while (remainder > 0)
                    {
                        pass.Append(symbolsPass[rnd.Next(symbolsPass.Length)]);
                        remainder--;
                    }
                    remainder = length;
                    pwds.Add(pass.ToString());
                    count--;
                }
                return pwds;
            }

        }
    }
}
