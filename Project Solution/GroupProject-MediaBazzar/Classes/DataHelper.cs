﻿using GroupProject_MediaBazzar;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading;

namespace Classes
{
    public class DataHelper
    {
        private MySqlConnection connection;

        #region Connection and datahelper
        public DataHelper()
        {
            string server = "Server=studmysql01.fhict.local;Uid=dbi380523;Database=dbi380523;Pwd=databasejorick;";
            connection = new MySqlConnection(server);
        }

        public void OpenConnection()
        {
            connection.Open();
        }
        public void CloseConnection()
        {
            connection.Close();
        }
        #endregion

        #region Login
        public Position Login(string username, string password)
        {
            //set up command parameters
            string sql = "SELECT currentposition FROM employee WHERE username = @username AND password = @password;";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@username", username);
            command.Parameters.AddWithValue("@password", password);

            //If nobody is found, default to no position
            Position position = Position.None;
            try
            {
                //Open the connection and execute command
                OpenConnection();
                MySqlDataReader dr = command.ExecuteReader();

                //Read position from the user table
                while (dr.Read())
                {
                    string s = dr["currentposition"].ToString();
                    position = (Position)Enum.Parse(typeof(Position), s);
                }
                return position;
            }
            catch (MySqlException)
            {
                throw new ArgumentException("No connection could be established with the database.");
            }
            finally
            {
                CloseConnection();
            }
        }

        #endregion

        #region Employees

        //Adding employees-------------------------------------------------------------------------------------------------------------------------------------- 
        public List<string> GetBSN()
        {
            //set up command parameters
            string sql = "SELECT BSN FROM employee;";
            MySqlCommand command = new MySqlCommand(sql, connection);
            List<string> bsns = new List<string>();
            try
            {
                OpenConnection();
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    bsns.Add(dr["BSN"].ToString());

                }
                return bsns;
            }
            catch (MySqlException)
            {
                throw new ArgumentException("No connection could be established with the database.");
            }
            finally
            {
                CloseConnection();
            }
        }
        public bool AddEmployee(Employee employee, Store store)
        {
            //Set up command and add parameters
            string sql = "INSERT INTO employee (id, store, username, password, firstname, lastname, gender, bsn, birthdate,  emailaddress, nationality, spokenlanguages, region, zipcode," +
                " streetname, phonenumber, emergencyphonenumber,spouse) VALUES (@id, @store, @username, @password, @firstname, @lastname, @gender, @bsn, @birthdate,@emailaddress, @nationality," +
                " @spokenlanguages, @region, @zipcode, @streetname, @phonenumber, @emergencyphonenumber, @spouse)";


            MySqlCommand command = new MySqlCommand(sql, connection);

            string spokenLanguages = employee.spokenLanguagesAsString();
            command.Parameters.AddWithValue("@id", employee.id);
            command.Parameters.AddWithValue("@store", store.name);
            command.Parameters.AddWithValue("@username", employee.username);
            command.Parameters.AddWithValue("@password", employee.password);
            command.Parameters.AddWithValue("@firstname", employee.firstName);
            command.Parameters.AddWithValue("@lastname", employee.lastName);
            command.Parameters.AddWithValue("@gender", employee.gender);
            command.Parameters.AddWithValue("@bsn", employee.bsn);
            command.Parameters.AddWithValue("@birthdate", employee.birthDate);
            command.Parameters.AddWithValue("@emailaddress", employee.emailAddress);
            command.Parameters.AddWithValue("@nationality", employee.nationality);
            command.Parameters.AddWithValue("@spokenlanguages", spokenLanguages);
            command.Parameters.AddWithValue("@region", employee.region);
            command.Parameters.AddWithValue("@zipcode", employee.zipcode);
            command.Parameters.AddWithValue("@streetname", employee.streetName);
            command.Parameters.AddWithValue("@phonenumber", employee.phonenumber);
            command.Parameters.AddWithValue("@emergencyphonenumber", employee.emergencyPhonenumber);
            command.Parameters.AddWithValue("@spouse", employee.spouse);

            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();

                return true;
            }
            catch
            {
                return false;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        public bool AddAvailaility(int id)
        {
            string sql = "INSERT INTO availability (employeeid, mondaymorning, mondayafternoon, mondayevening, tuesdaymorning, tuesdayafternoon, tuesdayevening" +
             ", wednesdaymorning, wednesdayafternoon, wednesdayevening, thursdaymorning, thursdayafternoon, " +
             "thursdayevening, fridaymorning, fridayafternoon, fridayevening, saturdaymorning, saturdayafternoon, saturdayevening," +
             "sundaymorning,sundayafternoon, sundayevening) VALUES (@id, 0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0)";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", id);
            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();

                return true;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        //Getting employee data---------------------------------------------------------------------------------------------------------------------------------------------------
        public string GetFullName(string username)
        {
            //Set up command and add parameters
            string sql = "SELECT firstname, lastname FROM employee WHERE username = @username;";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@username", username);
            string fullName = "";
            try
            {
                //Open the connection and execute command
                OpenConnection();
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    //return the full name as a string
                    fullName = dr["firstname"].ToString() + " " + dr["lastname"].ToString();
                }
                return fullName;
            }
            catch (MySqlException ex)
            {
                throw (ex);
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        public Employee GetEmployee(int id)
        {
            //Set up command and add parameter, initialize an employee
            string sql = "SELECT * FROM employee WHERE id= @id";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", id);
            Employee employee = new Employee();

            try
            {
                //Open the connection, execute the command and initialize variables
                OpenConnection();
                MySqlDataReader reader = command.ExecuteReader();
                int employeeId;
                string store, username, password, firstName, lastName, bsn, nationality, emailAddress,
                         region, zipCode, streetName, phoneNumber, emergencyPhoneNumber;
                Gender gender = Gender.None;
                DateTime birthDate;
                string spokenLanguagesString;

                bool spouse;
                List<string> spokenLanguages = new List<string>();

                while (reader.Read())
                {
                    //Read values and store them in the employee
                    employeeId = Convert.ToInt32(reader["id"]);
                    store = Convert.ToString(reader["store"]);
                    username = Convert.ToString(reader["username"]);
                    password = Convert.ToString(reader["password"]);
                    firstName = Convert.ToString(reader["firstname"]);
                    lastName = Convert.ToString(reader["lastname"]);
                    emailAddress = Convert.ToString(reader["emailaddress"]);
                    gender = (Gender)Enum.Parse(typeof(Gender), reader["gender"].ToString());
                    bsn = Convert.ToString(reader["bsn"]);
                    birthDate = reader.GetDateTime(reader.GetOrdinal("birthdate"));
                    nationality = Convert.ToString(reader["nationality"]);
                    spokenLanguagesString = (reader["spokenlanguages"].ToString());
                    region = Convert.ToString(reader["region"]);
                    zipCode = Convert.ToString(reader["zipcode"]);
                    streetName = Convert.ToString(reader["streetname"]);
                    phoneNumber = Convert.ToString(reader["phonenumber"]);
                    emergencyPhoneNumber = Convert.ToString(reader["emergencyphonenumber"]);
                    spouse = Convert.ToBoolean(reader["spouse"]);

                    spokenLanguages = spokenLanguagesString.Split(',').ToList<string>();

                    employee = new Employee(id, username, firstName, lastName, gender, birthDate, nationality,
                    emailAddress, bsn, region, zipCode, streetName, phoneNumber, password, emergencyPhoneNumber, spokenLanguages, spouse);
                }
                return employee;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }

        }
        public List<Employee> SearchForEmployees(string input)
        {
            //Set up command and select a query based upon the criteria given, initialize list of employees
            string sql;
            string[] pos = { "", "" };
            if (input.Contains("pos|"))
            {
                pos = input.Split('|');
                sql = "SELECT * FROM employee WHERE currentposition = @position";
            }
            else if (input == "")
            {
                sql = "SELECT * FROM employee";
            }
            else
            {
                sql = "SELECT * FROM employee WHERE firstname = @input OR lastname = @input OR id = @input OR bsn = @input";
            }
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@input", input);
            if (input.Contains("pos|"))
            {
                command.Parameters.AddWithValue("@position", pos[1]);
            }
            List<Employee> employees = new List<Employee>();
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                MySqlDataReader reader = command.ExecuteReader();
                int id;
                string store, username, password, firstName, lastName, bsn, nationality, emailAddress,
                         region, zipCode, streetName, phoneNumber, emergencyPhoneNumber;
                Gender gender = Gender.None;
                DateTime birthDate;

                bool spouse;
                string spokenLanguagesString;
                List<string> spokenLanguages = new List<string>();

                while (reader.Read())
                {
                    //Read values and store them in the list as instances of the employee class
                    id = Convert.ToInt32(reader["id"]);
                    store = Convert.ToString(reader["store"]);
                    username = Convert.ToString(reader["username"]);
                    password = Convert.ToString(reader["password"]);
                    firstName = Convert.ToString(reader["firstname"]);
                    lastName = Convert.ToString(reader["lastname"]);
                    emailAddress = Convert.ToString(reader["emailaddress"]);
                    gender = (Gender)Enum.Parse(typeof(Gender), reader["gender"].ToString());
                    bsn = Convert.ToString(reader["bsn"]);
                    birthDate = reader.GetDateTime(reader.GetOrdinal("birthdate"));
                    nationality = Convert.ToString(reader["nationality"]);
                    spokenLanguagesString = (reader["spokenlanguages"].ToString());
                    region = Convert.ToString(reader["region"]);
                    zipCode = Convert.ToString(reader["zipcode"]);
                    streetName = Convert.ToString(reader["streetname"]);
                    phoneNumber = Convert.ToString(reader["phonenumber"]);
                    emergencyPhoneNumber = Convert.ToString(reader["emergencyphonenumber"]);

                    spouse = Convert.ToBoolean(reader["spouse"]);
                    spokenLanguages = spokenLanguagesString.Split(',').ToList<string>();

                    employees.Add(new Employee(id, username, firstName, lastName, gender, birthDate, nationality,
                    emailAddress, bsn, region, zipCode, streetName, phoneNumber, password, emergencyPhoneNumber, spokenLanguages, spouse));
                }
                return employees;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        public int GetNewEmployeeID()
        {
            string sql;
            sql = "SELECT MAX(id) FROM employee";
            MySqlCommand command = new MySqlCommand(sql, connection);

            try
            {
                OpenConnection();
                int id = Convert.ToInt32(command.ExecuteScalar());
                return id + 1;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }
        public int GetAmountOfEmployees()
        {
            string sql;
            sql = "SELECT * FROM contract WHERE active = @active";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@active", 1);
            int amount = 0;
            try
            {
                connection.Open();
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    amount++;

                }
                return amount;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public double GetAssignedFTEForEmployee(int employeeId, DateTime lower, DateTime upper)
        {
            string sql;
            sql = "SELECT COUNT(*) FROM shift " +
                  "WHERE employeeid = @id " +
                  "AND date BETWEEN @lower AND @upper ";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@lower", lower);
            command.Parameters.AddWithValue("@upper", upper);
            command.Parameters.AddWithValue("@id", employeeId);
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                double FTE = Convert.ToDouble(command.ExecuteScalar()) / 10;
                return FTE;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        //Editing employee data---------------------------------------------------------------------------------------------------------------------------------------------------
        public bool UpdateEmployeePosition(Employee employee, Position position)
        {
            //Set up command and add parameters
            string sql = "UPDATE employee SET currentposition = @currentposition WHERE id = @id";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", employee.id);
            command.Parameters.AddWithValue("@currentposition", position);
            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        public bool UpdateEmployeeDetails(Employee employee, Store store)
        {
            //Set up command and add parameters
            string sql = "UPDATE employee SET id=@id,store =@store,username=@username,firstname=@firstname,lastname=@lastname" +
                ",birthdate=@birthdate,emailaddress=@emailaddress,nationality=@nationality,region=@region,zipcode=@zipcode" +
                ",streetname=@streetname,phonenumber=@phonenumber,emergencyphonenumber=@emergencyphonenumber WHERE id =@id";
            MySqlCommand command = new MySqlCommand(sql, connection);
            string spokenLanguages = employee.spokenLanguagesAsString();
            command.Parameters.AddWithValue("@id", employee.id);
            command.Parameters.AddWithValue("@store", store.name);
            command.Parameters.AddWithValue("@username", employee.username);

            command.Parameters.AddWithValue("@firstname", employee.firstName);
            command.Parameters.AddWithValue("@lastname", employee.lastName);
            command.Parameters.AddWithValue("@birthdate", employee.birthDate);
            command.Parameters.AddWithValue("@emailaddress", employee.emailAddress);
            command.Parameters.AddWithValue("@nationality", employee.nationality);
            command.Parameters.AddWithValue("@region", employee.region);
            command.Parameters.AddWithValue("@zipcode", employee.zipcode);
            command.Parameters.AddWithValue("@streetname", employee.streetName);
            command.Parameters.AddWithValue("@phonenumber", employee.phonenumber);
            command.Parameters.AddWithValue("@emergencyphonenumber", employee.emergencyPhonenumber);
            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        #endregion

        #region Contracts

        //Adding contracts------------------------------------------------------------------------------------------------------------------------------------
        public void AddContract(Contract contract)
        {
            //Set up command and add parameters
            string sql;
            sql = "INSERT INTO contract (employeeid, position, hourlysalary, startdate, enddate ,FTE,active) VALUES (@employeeid, @position, @hourlysalary, @startdate, @enddate, @fte, @active)";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@employeeid", contract.employeeId);
            command.Parameters.AddWithValue("@position", contract.position);
            command.Parameters.AddWithValue("@hourlysalary", contract.hourlySalary);
            command.Parameters.AddWithValue("@startdate", contract.startDate);
            command.Parameters.AddWithValue("@enddate", contract.endDate);
            command.Parameters.AddWithValue("@fte", contract.fte);
            command.Parameters.AddWithValue("@active", 1);

            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        //Getting contract data-------------------------------------------------------------------------------------------------------------------------------
        public Contract GetContractById(int contractId)
        {
            //Set up command and add parameters
            string sql;
            sql = "SELECT * FROM contract WHERE id = @contractid";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@contractid", contractId);
            Contract contract = null;
            try
            {
                //Open the connection and execute command
                OpenConnection();
                MySqlDataReader reader = command.ExecuteReader();
                int id;
                int employeeId;
                Position position;
                double hourlySalary;
                DateTime startDate, endDate;
                bool active;
                string departureReason;
                DepartureType departureType;
                double fte;

                //Get values from the database and store them as a contract
                while (reader.Read())
                {
                    id = Convert.ToInt32(reader["id"]);
                    employeeId = Convert.ToInt32(reader["employeeid"]);
                    position = (Position)Enum.Parse(typeof(Position), reader["position"].ToString());
                    hourlySalary = Convert.ToDouble(reader["hourlysalary"]);
                    startDate = Convert.ToDateTime(reader["startdate"]);
                    endDate = Convert.ToDateTime(reader["enddate"]);
                    active = Convert.ToBoolean(reader["active"]);
                    fte = Convert.ToDouble(reader["FTE"]);

                    if (active == true)
                    {
                        contract = new Contract(true, true, id, employeeId, position, hourlySalary, startDate, endDate, fte, active);
                    }
                    else
                    {
                        departureReason = reader["departurereason"].ToString();
                        departureType = (DepartureType)Enum.Parse(typeof(DepartureType), reader["departuretype"].ToString());
                        endDate = Convert.ToDateTime(reader["enddate"]);
                        contract = new Contract(true, true, id, employeeId, position, hourlySalary, startDate, endDate, fte, active, departureReason, departureType);
                    }

                }

                //return the retrieved contract
                return contract;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        public List<Contract> GetAllContracts(int employeeId)
        {
            //Set up command and add parameters, initialize list of contracts
            string sql;
            sql = "SELECT * FROM contract WHERE employeeid = @employeeid";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@employeeid", employeeId);
            List<Contract> contracts = new List<Contract>();
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                MySqlDataReader reader = command.ExecuteReader();
                int id;
                Position position;
                double hourlySalary;
                DateTime startDate, endDate;
                bool active;
                string departureReason;
                DepartureType departureType;
                double fte;
                //Get values from the database and save them as instances of the contract class
                while (reader.Read())
                {
                    id = Convert.ToInt32(reader["id"]);
                    position = (Position)Enum.Parse(typeof(Position), reader["position"].ToString());
                    hourlySalary = Convert.ToDouble(reader["hourlysalary"]);
                    startDate = Convert.ToDateTime(reader["startdate"]);
                    endDate = Convert.ToDateTime(reader["enddate"]);
                    active = Convert.ToBoolean(reader["active"]);
                    fte = Convert.ToDouble(reader["FTE"]);

                    if (active == true)
                    {
                        contracts.Add(new Contract(true, false, id, 0, position, hourlySalary, startDate, endDate, fte, active));
                    }
                    else
                    {
                        departureReason = reader["departurereason"].ToString();
                        departureType = (DepartureType)Enum.Parse(typeof(DepartureType), reader["departuretype"].ToString());
                        endDate = Convert.ToDateTime(reader["enddate"]);
                        contracts.Add(new Contract(true, false, id, 0, position, hourlySalary, startDate, endDate, fte, active, departureReason, departureType));
                    }

                }

                //return all retrieved contracts
                return contracts;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the cnnection
                CloseConnection();
            }
        }

        //Editing contract data---------------------------------------------------------------------------------------------------------------------------------------------------
        public void EditContract(int currentContractId, Contract contract)
        {
            //Initialize variables to use in ending the active contract
            DepartureType departureType = DepartureType.ContractChanged;
            string departureReason = "Contract changed";
            DateTime endDate = DateTime.Now;

            //End the current contract
            EndContract(currentContractId, departureType, departureReason, endDate);

            //Create a new contract with the new salary and position
            AddContract(contract);
        }
        public void RenewContract(int currentContractId, Contract contract, DateTime enddate)
        {
            //Initialize variables to use in renewing the active contract
            DateTime endDate = DateTime.Now;
            DepartureType departureType = DepartureType.ContractChanged;
            contract.endDate = enddate;
            string departureReason = "Contract renewed";
            //End the current contract
            EndContract(currentContractId, departureType, departureReason, endDate);

            //Create a new contract with the new salary and position
            AddContract(contract);
        }
        public void EndContract(int id, DepartureType departureType, string departureReason, DateTime endDate)
        {
            //Set up command and add parameters
            string sql;
            sql = "UPDATE contract SET enddate = @enddate, active = FALSE, departurereason = @departurereason, departuretype = @departuretype WHERE id = @id";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@enddate", endDate);
            command.Parameters.AddWithValue("@departurereason", departureReason);
            command.Parameters.AddWithValue("@departuretype", departureType);
            command.Parameters.AddWithValue("@id", id);

            try
            {
                //Open connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        #endregion

        #region Shifts
        //Adding shifts---------------------------------------------------------------------------------------------------------------------------------------------------
        public bool AddShift(Shift scheduleDay)
        {
            //Set up command and add parameters
            string sql = "INSERT INTO shift (id, employeeid, date, daysegment, floor) " +
                         "VALUES (@id, @employeeid, @date, @daysegment, @floor)";

            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", scheduleDay.id);
            command.Parameters.AddWithValue("@employeeid", scheduleDay.employeeId);
            command.Parameters.AddWithValue("@date", scheduleDay.date);
            command.Parameters.AddWithValue("@daysegment", scheduleDay.daySegment);
            command.Parameters.AddWithValue("@floor", scheduleDay.floor);

            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException)
            {

                return false;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }

        }
        public bool AddShift(int id, DateTime date, string daysegment, string department)
        {
            //Set up command and add parameters
            string sql = "INSERT INTO shift (employeeid, date, daysegment, floor) " +
                         "VALUES (@employeeid, @date, @daysegment, @floor)";

            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@employeeid", id);
            command.Parameters.AddWithValue("@date", date);
            command.Parameters.AddWithValue("@daysegment", daysegment);
            command.Parameters.AddWithValue("@floor", department);

            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException)
            {

                return false;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }

        }

        //Getting shift data---------------------------------------------------------------------------------------------------------------------------------------------------
        public List<Shift> GetAllShifts()
        {
            string sql = "SELECT * FROM shift";
            MySqlCommand command = new MySqlCommand(sql, connection);
            List<Shift> shifts = new List<Shift>();

            try
            {
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int id = Convert.ToInt32(reader["id"]);
                    int employeeId = Convert.ToInt32(reader["employeeid"]);
                    DateTime date = reader.GetDateTime(reader.GetOrdinal("date"));
                    string daySegment = Convert.ToString(reader["daysegment"]);
                    string floor = Convert.ToString(reader["floor"]);
                    shifts.Add(new Shift(id, employeeId, date, daySegment, floor));
                }
                return shifts;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public DataTable GetShiftsBetweenForPosition(string floor, DateTime lowerBound, DateTime upperBound)
        {
            //Set up command and select a query based upon the criteria given, initialize list of employees
            DataTable dt = new DataTable();
            string sql;

            sql = "SELECT employee.id, employee.firstname, employee.lastname, shift.date, shift.daysegment, shift.floor, employee.currentposition " +
                  "FROM employee " +
                  "INNER JOIN shift ON employee.id=shift.employeeid " +
                  "WHERE shift.floor=@floor AND shift.date BETWEEN @lowerBound AND @upperBound " +
                  "ORDER BY shift.date";

            //Unsure how to properly check for the date here? Would place it after the position.
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@floor", floor);
            command.Parameters.AddWithValue("@lowerBound", lowerBound);
            command.Parameters.AddWithValue("@upperBound", upperBound);

            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dt);
                return dt;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        public DataTable GetAllShifts(DateTime lowerbound, DateTime upperbound)
        {
            DataTable dtShifts = new DataTable();
            string sql;

            sql = "SELECT employee.id, employee.firstname, employee.lastname, shift.date, shift.daysegment, shift.floor, employee.currentposition " +
                  "FROM employee " +
                  "INNER JOIN shift ON employee.id=shift.employeeid " +
                  "WHERE shift.date BETWEEN @lowerBound AND @upperBound " +
                  "ORDER BY shift.date";

            //Unsure how to properly check for the date here? Would place it after the position.
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@lowerBound", lowerbound);
            command.Parameters.AddWithValue("@upperBound", upperbound);

            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dtShifts);
                return dtShifts;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        public List<Shift> GetShiftsBetween(DateTime lowerBound, DateTime upperBound)
        {
            string sql = "SELECT * FROM shift";
            MySqlCommand command = new MySqlCommand(sql, connection);
            List<Shift> shifts = new List<Shift>();

            try
            {
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    int id = Convert.ToInt32(reader["id"]);
                    int employeeId = Convert.ToInt32(reader["employeeid"]);
                    DateTime date = reader.GetDateTime(reader.GetOrdinal("date"));
                    string daySegment = Convert.ToString(reader["daysegment"]);
                    string floor = Convert.ToString(reader["floor"]);
                    if (date >= lowerBound || date <= upperBound)
                    {
                        shifts.Add(new Shift(id, employeeId, date, daySegment, floor));
                    }
                }
                return shifts;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public bool SearchForShift(int employeeId, string floor, string daySegment, DateTime date)
        {
            string sql;
            sql = "SELECT * FROM shift WHERE employeeid = @employeeId AND floor = @floor AND daysegment = @daySegment AND date = @date";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@employeeId", employeeId);
            command.Parameters.AddWithValue("@floor", floor);
            command.Parameters.AddWithValue("@daySegment", daySegment);
            command.Parameters.AddWithValue("@date", date.Date);
            try
            {
                OpenConnection();
                if (command.ExecuteScalar() != null)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                return false;
            }
            finally
            {
                CloseConnection();
            }
        }

        //Editing shift data---------------------------------------------------------------------------------------------------------------------------------------------------
        public void RemoveEmployeeFromShift(int id, DateTime date, string daysegment)
        {
            string sql;
            sql = "DELETE FROM shift WHERE employeeid = @id AND date = @date AND daysegment = @daysegment";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@date", date);
            command.Parameters.AddWithValue("@daysegment", daysegment);
            try
            {
                OpenConnection();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        public void PublishSchedule()
        {
            string sql;
            sql = "INSERT INTO shift (employeeid, date, daysegment, floor) SELECT employeeid, date, separateSegment, floor FROM temporaryshiftholder";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                OpenConnection();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        #endregion

        #region Stock
        //Adding stock---------------------------------------------------------------------------------------------------------------------------------------------------------

        //Getting stock data---------------------------------------------------------------------------------------------------------------------------------------------------
        public List<Product> SearchForProducts(string input)
        {
            string sql;

            //Select the correct query for the criteria given
            if (input == "")
            {
                sql = "SELECT * FROM product";
            }
            else
            {
                sql = "SELECT * FROM product WHERE productname = @input OR barcode = @input OR manufacturerbrand = @input OR category = @input";
            }

            //set up command
            MySqlCommand cmd = new MySqlCommand(sql, connection);
            cmd.Parameters.AddWithValue("@input", input);

            try
            {
                //Open the connection and execute command, initialize necessary variables
                OpenConnection();
                MySqlDataReader reader = cmd.ExecuteReader();
                List<Product> products = new List<Product>();
                int barcode, modelNumber, stockInDepot, stockOnShelves, amountSold;
                double normalPrice, promotionPrice;
                string store, productName, manufacturerBrand, category;

                //Read all the values needed and create a Product with the values retrieved
                while (reader.Read())
                {
                    barcode = Convert.ToInt32(reader["barcode"]);
                    store = Convert.ToString(reader["store"]);
                    productName = Convert.ToString(reader["productname"]);
                    modelNumber = Convert.ToInt32(reader["modelnumber"]);
                    manufacturerBrand = Convert.ToString(reader["manufacturerbrand"]);
                    category = Convert.ToString(reader["category"]);
                    normalPrice = Convert.ToDouble(reader["normalprice"]);
                    promotionPrice = Convert.ToDouble(reader["promotionprice"]);
                    stockInDepot = Convert.ToInt32(reader["stockindepot"]);
                    stockOnShelves = Convert.ToInt32(reader["stockonshelves"]);
                    amountSold = Convert.ToInt32(reader["amountsold"]);

                    products.Add(new Product(barcode, productName, modelNumber, manufacturerBrand, category, normalPrice, promotionPrice, stockInDepot, stockOnShelves, amountSold));
                }

                return products;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }

        }

        //Editing Stock data---------------------------------------------------------------------------------------------------------------------------------------------------
        public void ModifyStockAmount(int barcode, int amount)
        {
            //Set up command and add parameters
            string sql = "UPDATE product SET stockindepot=@amount WHERE barcode = @barcode";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@barcode", barcode);
            command.Parameters.AddWithValue("@amount", amount);
            try
            {
                //Open the connection and execute command
                OpenConnection();
                int effectedRows = command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        public bool LogStockUpdate(StockChange stockchange)
        {
            //Set up command and add parameters
            string sql = "INSERT INTO stockupdate (productbarcode, updatereason) VALUES (@productbarcode, @updatereason)";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@productbarcode", stockchange.productBarcode);
            command.Parameters.AddWithValue("@updatereason", stockchange.reason);
            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();
                return true;

            }
            catch (MySqlException)
            {
                return false;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        #endregion

        #region RestockRequest
        //Adding restock request
        public bool PlaceRestockRequest(RestockRequest restockRequest, Store store)
        {
            //Set up command and add parameters
            string sql = "INSERT INTO restockrequest (store, productbarcode, productamount, floor, date, delivered) VALUES " +
                "(@store, @productbarcode, @productamount, @floor, @date, @delivered)";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@store", store.name);
            command.Parameters.AddWithValue("@productbarcode", restockRequest.productBarcode);
            command.Parameters.AddWithValue("@productamount", restockRequest.productAmount);
            command.Parameters.AddWithValue("@floor", restockRequest.floor);
            command.Parameters.AddWithValue("@date", restockRequest.date);
            command.Parameters.AddWithValue("@delivered", restockRequest.delivered);

            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        //Getting restock request data---------------------------------------------------------------------------------------------------------------------------------------------------
        public List<RestockRequest> GetRestockRequests()
        {
            //Set up command and initialize list
            string sql = "SELECT * FROM restockrequest WHERE delivered = 0";
            MySqlCommand command = new MySqlCommand(sql, connection);
            List<RestockRequest> restockRequests = new List<RestockRequest>();
            try
            {
                //Open the connection and initialize variables
                OpenConnection();
                MySqlDataReader reader = command.ExecuteReader();
                int id, productBarcode, productAmount;
                string floor, store;
                DateTime date;
                int delivered;

                //Read values and create restockrequests with the data
                while (reader.Read())
                {
                    id = Convert.ToInt32(reader["id"]);
                    store = Convert.ToString(reader["store"]);
                    date = reader.GetDateTime(reader.GetOrdinal("date"));
                    productBarcode = Convert.ToInt32(reader["productbarcode"]);
                    productAmount = Convert.ToInt32(reader["productamount"]);
                    floor = Convert.ToString(reader["floor"]);
                    delivered = Convert.ToInt32(reader["delivered"]);


                    restockRequests.Add(new RestockRequest(id, productBarcode, productAmount, floor, date));
                }
                return restockRequests;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }

        }

        //Editing restock request data---------------------------------------------------------------------------------------------------------------------------------------------------
        public void ConfirmRestockRequest(int id)
        {
            //Set up command and add parameters
            string sql = "UPDATE restockrequest SET delivered = 1 WHERE id = @id";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", id);

            try
            {
                //Open the connection and execute the command
                OpenConnection();
                int effectedRows = command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        #endregion

        #region Departments

        //Adding department---------------------------------------------------------------------------------------------------------------------------------------------------

        public void AddDepartment(Department department)
        {
            //Set up command and add parameters
            string sql = "INSERT INTO departments (department, requiredfte, position) VALUES (@name, @expectedrequiredshiftsperweek, @position)";


            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@name", department.Name);
            command.Parameters.AddWithValue("@expectedrequiredshiftsperweek", department.ExpectedRequiredShiftsPerWeek);
            command.Parameters.AddWithValue("@position", department.position);

            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();


            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        //Getting department data---------------------------------------------------------------------------------------------------------------------------------------------------
        public List<Department> GetAllDepartments()
        {
            //Set up command and add parameters
            string sql = "SELECT * FROM departments";


            MySqlCommand command = new MySqlCommand(sql, connection);

            try
            {
                //Open the connection and execute command
                OpenConnection();
                MySqlDataReader reader = command.ExecuteReader();
                List<Department> departments = new List<Department>();
                while (reader.Read())
                {
                    int id = Convert.ToInt32(reader["id"]);
                    string name = reader["department"].ToString();
                    int expectedRequiredShiftsPerWeek = Convert.ToInt32(reader["requiredfte"]);
                    Position position = (Position)Enum.Parse(typeof(Position), reader["position"].ToString());
                    Department department = new Department(id, name, position, expectedRequiredShiftsPerWeek);
                    departments.Add(department);
                }
                return departments;

            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        public List<string> GetAllDepartmentNames()
        {
            string sql;
            sql = "SELECT * FROM departments";
            MySqlCommand command = new MySqlCommand(sql, connection);
            List<string> departments = new List<string>();
            try
            {
                OpenConnection();
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    departments.Add(dr["department"].ToString());

                }
                return departments;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                CloseConnection();
            }
        }

        //Editing department data---------------------------------------------------------------------------------------------------------------------------------------------------

        public void EditDepartment(Department department)
        {
            //Set up command and add parameters
            string sql = "UPDATE departments SET department = @name, requiredfte = @expectedrequiredshiftsperweek, position = @position WHERE id = @id";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", department.Id);
            command.Parameters.AddWithValue("@name", department.Name);
            command.Parameters.AddWithValue("@expectedrequiredshiftsperweek", department.ExpectedRequiredShiftsPerWeek);
            command.Parameters.AddWithValue("@position", department.position);
            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        public void DeleteDepartment(int id)
        {
            //Set up command and add parameters
            string sql = "DELETE FROM departments WHERE id = @id";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", id);
            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        #endregion

        #region Statistics department
        //Getting department statistic data------------------------------------------------------------------------------------------------------------------------------------------
        public double GetRequiredFteForDepartment(string department)
        {
            string sql;
            sql = "SELECT * FROM departments WHERE department = @department";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@department", department);

            try
            {
                connection.Open();
                MySqlDataReader dr = command.ExecuteReader();
                double fte = 0;

                while (dr.Read())
                {
                    fte = Convert.ToDouble(dr["requiredfte"]);

                }
                return fte;
            }

            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public Position GetPositionForDepartment(string department)
        {
            string sql;
            sql = "SELECT position FROM departments WHERE department = @department";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@department", department);
            try
            {
                connection.Open();
                MySqlDataReader dr = command.ExecuteReader();
                Position position = Position.None;
                while (dr.Read())
                {
                    position = (Position)Enum.Parse(typeof(Position), dr["position"].ToString());
                }
                return position;
            }

            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }
        public List<int> GetActiveEmployeesIdsForDepartment(string department)
        {
            Position position = GetPositionForDepartment(department);
            string sql;
            sql = "SELECT * FROM contract WHERE position = @position AND active = 1";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@position", position);
            List<int> employeeIds = new List<int>();
            try
            {
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    employeeIds.Add(Convert.ToInt32(reader["employeeId"]));
                }
                return employeeIds;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }

        }
        public List<Employee> GetActiveEmployeesForDepartment(string department)
        {
            List<int> employeeIds = GetActiveEmployeesIdsForDepartment(department);

            string sql;
            sql = "SELECT * FROM employee";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader();
                List<Employee> employees = new List<Employee>();
                while (reader.Read())
                {
                    //Read values and store them in the list as instances of the employee class
                    int id = Convert.ToInt32(reader["id"]);
                    string username = Convert.ToString(reader["username"]);
                    string firstName = Convert.ToString(reader["firstname"]);
                    string lastName = Convert.ToString(reader["lastname"]);
                    string emailAddress = Convert.ToString(reader["emailaddress"]);
                    Gender gender = (Gender)Enum.Parse(typeof(Gender), reader["gender"].ToString());
                    string bsn = Convert.ToString(reader["bsn"]);
                    DateTime birthDate = reader.GetDateTime(reader.GetOrdinal("birthdate"));
                    string nationality = Convert.ToString(reader["nationality"]);
                    string spokenLanguagesString = (reader["spokenlanguages"].ToString());
                    string region = Convert.ToString(reader["region"]);
                    string zipCode = Convert.ToString(reader["zipcode"]);
                    string streetName = Convert.ToString(reader["streetname"]);
                    string phoneNumber = Convert.ToString(reader["phonenumber"]);
                    string emergencyPhoneNumber = Convert.ToString(reader["emergencyphonenumber"]);

                    bool spouse = Convert.ToBoolean(reader["spouse"]);
                    List<string> spokenLanguages = spokenLanguagesString.Split(',').ToList<string>();
                    if (employeeIds.Contains(id))
                    {
                        employees.Add(new Employee(id, username, firstName, lastName, gender, birthDate, nationality,
                    emailAddress, bsn, region, zipCode, streetName, phoneNumber, emergencyPhoneNumber, spokenLanguages, spouse));
                    }


                }
                return employees;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        #endregion

        #region Statistics stock
        //Getting stock statistic data------------------------------------------------------------------------------------------------------------------------------------------

        #endregion

        #region Templates

        //Adding templates-------------------------------------------------------------------------------------------------------------------------------------------------------
        public void AddTemplate(Template template)
        {
            //Set up command and add parameters
            string sql = "INSERT INTO template (name, description) " +
                         "VALUES (@name, @description)";

            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@name", template.Name);
            command.Parameters.AddWithValue("@description", template.Description);


            try
            {
                //Open the connection and execute command
                OpenConnection();
                command.ExecuteNonQuery();
            }
            catch (MySqlException)
            {

            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        //Getting template data---------------------------------------------------------------------------------------------------------------------------------------------------
        public List<TemplateShift> GetTemplateShifts(int templateId)
        {
            List<TemplateShift> templateShifts = new List<TemplateShift>();
            string sql;
            sql = "SELECT * FROM templateshift WHERE templateid = @templateid";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@templateid", templateId);
            try
            {
                connection.Open();
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    templateShifts.Add(new TemplateShift(Convert.ToInt32(dr["id"]), dr["department"].ToString(), Convert.ToDateTime(dr["date"])
                        , dr["daysegment"].ToString(), Convert.ToInt32(dr["requiredemployees"]), Convert.ToInt32(dr["templateid"])));
                }
                return templateShifts;
            }

            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public DataTable GetTemplates()
        {
            DataTable dtTemplate = new DataTable();
            string sql;

            sql = "SELECT * FROM template";

            //Unsure how to properly check for the date here? Would place it after the position.
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dtTemplate);
                return dtTemplate;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        public DataTable GetAllTemplateShifts()
        {
            DataTable dtTemplateShifts = new DataTable();
            string sql;

            sql = "SELECT * FROM templateshift";

            //Unsure how to properly check for the date here? Would place it after the position.
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dtTemplateShifts);
                return dtTemplateShifts;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        public DataTable GetTemplateShiftsPerDepartment(string department)
        {
            DataTable dtTemplateShifts = new DataTable();
            string sql;

            sql = "SELECT requiredemployees, employeeid, date, daysegment, floor, employee.firstname, employee.lastname" +
                " FROM temporaryshiftholder INNER JOIN employee ON employeeid = employee.id WHERE floor = @department";

            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@department", department);
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dtTemplateShifts);
                return dtTemplateShifts;
            }
            catch (MySqlException ex)
            {
                return null;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        //Editing department data---------------------------------------------------------------------------------------------------------------------------------------------------

        #endregion

        #region Temporary templateShift table
        public bool CreateTempraryTableInDB()
        {

            string sql;

            sql = "CREATE TABLE temporaryshiftholder (id INT NOT NULL AUTO_INCREMENT , PRIMARY KEY (id) , requiredemployees int(11), employeeid int(11), date date, daysegment varchar(50), separateSegment varchar (50), floor varchar(50))";

            //Unsure how to properly check for the date here? Would place it after the position.
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                command.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException ex)
            {
                return false;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        public void deleteTempraryTableInDB()
        {

            string sql;

            sql = "DROP TABLE temporaryshiftholder; ";

            //Unsure how to properly check for the date here? Would place it after the position.
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {

            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        public DataTable InsertTempraryTemplateShifts(int requiredEmployees, int id, DateTime date, string daysegment, string separatedDaySegment, string floor)
        {
            DataTable dtTemplateShifts = new DataTable();
            string sql;

            sql = "INSERT INTO temporaryshiftholder (requiredemployees, employeeid, date, daysegment, separateSegment, floor) VALUES(@required, @id, @date, @daysegment, @separate, @floor)";

            //Unsure how to properly check for the date here? Would place it after the position.
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@required", requiredEmployees);
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@date", date);
            command.Parameters.AddWithValue("@daysegment", daysegment);
            command.Parameters.AddWithValue("@separate", separatedDaySegment);
            command.Parameters.AddWithValue("@floor", floor);
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                MySqlDataAdapter da = new MySqlDataAdapter(command);
                da.Fill(dtTemplateShifts);
                return dtTemplateShifts;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        public void RemoveEmployeeFromTemporaryShift(int id, string daySegment)
        {

            string sql;

            sql = "DELETE FROM temporaryshiftholder WHERE employeeid = @id AND daysegment = @daysegment; ";

            //Unsure how to properly check for the date here? Would place it after the position.
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@daysegment", daySegment);
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                command.ExecuteNonQuery();
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }

        public bool InsertEmployeeIntoTemporaryScheduler(int id, string floor, string daysegment, string separateDaySegment, DateTime date)
        {
            string sql;

            sql = "INSERT INTO temporaryshiftholder (requiredemployees, employeeid, date, daysegment, separateSegment ,floor) VALUES(@required, @id, @date, @daysegment, @separateSegment, @floor)";

            //Unsure how to properly check for the date here? Would place it after the position.
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@required", 0);
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@date", date);
            command.Parameters.AddWithValue("@daysegment", daysegment);
            command.Parameters.AddWithValue("@separateSegment", separateDaySegment);
            command.Parameters.AddWithValue("@floor", floor);
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                command.ExecuteNonQuery();
                return true;
            }
            catch (MySqlException)
            {
                return false;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        
        }
        public int FindEmployeeInTemporaryShift(int id, string daySegment)
        {
            string sql;

            sql = "SELECT employeeid FROM temporaryshiftholder WHERE employeeid =@id AND daysegment = @daysegment";

            //Unsure how to properly check for the date here? Would place it after the position.
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", id);
            command.Parameters.AddWithValue("@daysegment", daySegment);
            int employeeid = 0;
            try
            {
                connection.Open();
                MySqlDataReader reader = command.ExecuteReader();
                while (reader.Read())
                {
                    employeeid = Convert.ToInt32(reader["employeeid"]);
                }
                return employeeid;
            }
            catch (MySqlException)
            {
                return -1;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        } 
        public double GetAssignedEmployeeFTEInTemporaryShift(int id)
        {
            string sql;
            sql = "SELECT COUNT(*) FROM temporaryshiftholder " +
                  "WHERE employeeid = @id ";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", id);
            try
            {
                //Open the connection, execute command and initialize variables
                OpenConnection();
                double FTE = Convert.ToDouble(command.ExecuteScalar()) / 10;
                return FTE;
            }
            catch (MySqlException ex)
            {
                return -1;
            }
            finally
            {
                connection.Close();
            }
        }
        #endregion

        #region Availability
        //Getting availability------------------------------------------------------------------------------------------------------------------------------------------
        public List<Availability> GetAllAvailability()
        {
            List<Availability> availabilities = new List<Availability>();
            string sql;
            sql = "SELECT * FROM availability";
            MySqlCommand command = new MySqlCommand(sql, connection);
            try
            {
                connection.Open();
                MySqlDataReader dr = command.ExecuteReader();
                while (dr.Read())
                {
                    availabilities.Add(new Availability(Convert.ToInt32(dr["employeeid"]),
                        Convert.ToBoolean(dr["mondaymorning"]), Convert.ToBoolean(dr["mondayafternoon"]), Convert.ToBoolean(dr["mondayevening"]),
                        Convert.ToBoolean(dr["tuesdaymorning"]), Convert.ToBoolean(dr["tuesdayafternoon"]), Convert.ToBoolean(dr["tuesdayevening"]),
                        Convert.ToBoolean(dr["wednesdaymorning"]), Convert.ToBoolean(dr["wednesdayafternoon"]), Convert.ToBoolean(dr["wednesdayevening"]),
                        Convert.ToBoolean(dr["thursdaymorning"]), Convert.ToBoolean(dr["thursdayafternoon"]), Convert.ToBoolean(dr["thursdayevening"]),
                        Convert.ToBoolean(dr["fridaymorning"]), Convert.ToBoolean(dr["fridayafternoon"]), Convert.ToBoolean(dr["fridayevening"]),
                        Convert.ToBoolean(dr["saturdaymorning"]), Convert.ToBoolean(dr["saturdayafternoon"]), Convert.ToBoolean(dr["saturdayevening"]),
                        Convert.ToBoolean(dr["sundaymorning"]), Convert.ToBoolean(dr["sundayafternoon"]), Convert.ToBoolean(dr["sundayevening"])));
                }
                return availabilities;
            }

            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                connection.Close();
            }
        }

        public bool SearchForVacationDay(int employeeId, DateTime date)
        {
            //Set up command and add parameter, initialize an employee
            string sql = "SELECT * FROM vacationday WHERE employeeid= @id AND vacationdate=@date";
            MySqlCommand command = new MySqlCommand(sql, connection);
            command.Parameters.AddWithValue("@id", employeeId);
            command.Parameters.AddWithValue("@date", date.Date);

            try
            {
                //Open the connection, execute the command and initialize variables
                OpenConnection();
                MySqlDataReader reader = command.ExecuteReader();

                while (reader.Read())
                {
                    return true;
                }
                return false;
            }
            catch (MySqlException ex)
            {
                throw ex;
            }
            finally
            {
                //Close the connection
                CloseConnection();
            }
        }
        #endregion
    }
}