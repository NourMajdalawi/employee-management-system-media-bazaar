﻿using GroupProject_MediaBazzar;
using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{

    public class TemplateShift
    {
        public int Id { get; private set; }
        public string Department { get; private set; }
        public DateTime Date { get;  set; }
        public string DaySegment { get; set; }
        public int RequiredEmployees { get; private set; }
        public int TemplateId { get; private set; }
        private List<Employee> employees;
        public List<Employee> Employees
        {
            get { return this.employees; }
            private set
            {
                this.employees = value;
            }
        }

        public TemplateShift(int id, string department, DateTime date, string daySegment, int requiredEmployees, int templateId)
        {
            Id = id;
            Department = department;
            Date = date;
            DaySegment = daySegment;
            RequiredEmployees = requiredEmployees;
            TemplateId = templateId;
            employees = new List<Employee>();
        }

        public void AddEmployee(Employee employee)
        {
            Employees.Add(employee);
        }

    }


}
