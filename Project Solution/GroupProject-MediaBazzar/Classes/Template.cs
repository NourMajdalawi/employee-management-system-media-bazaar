﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public class Template
    {
       public int Id { get; private set; }
       public string Name { get; private set; }
       public string Description { get; private set; }
       public List<TemplateShift> templateShifts { get; private set; }

        public Template(int id, string name, string description)
        {
            Id = id;
            Name = name;
            Description = description;
            templateShifts = new List<TemplateShift>();
        }

        


    }
}
