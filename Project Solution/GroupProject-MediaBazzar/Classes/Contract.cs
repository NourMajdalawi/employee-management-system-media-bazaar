﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GroupProject_MediaBazzar
{
	public class Contract
	{
		public int id { get; }
		public int employeeId { get; }
		public Position position { get; private set; }
		public double hourlySalary { get; private set; }
		public string departureReason { get; private set; }
		public DateTime startDate { get; set; }
		public DateTime endDate { get; set; }
		public bool active { get; set; }
		public double fte { get; private set; }

		public DepartureType departureType { get; private set; }

		//Constructor without enddate
		public Contract(bool useContractId, bool useEmployeeId, int id, int employeeId, Position position, double hourlySalary, DateTime startDate, double fte, bool active)
		{
			if (useContractId)
			{
				this.id = id;
			}
			if (useEmployeeId)
			{
				this.employeeId = employeeId;
			}
			this.hourlySalary = hourlySalary;
			this.position = position;
			this.startDate = startDate;
			this.active = active;
			this.fte = fte;
		}

		//Constructor with enddate but no departuretype or departurereason
		public Contract(bool useContractId, bool useEmployeeId, int id, int employeeId, Position position, double hourlySalary, DateTime startDate, DateTime endDate, double fte, bool active)
		{
			if (useContractId)
			{
				this.id = id;
			}
			if (useEmployeeId)
			{
				this.employeeId = employeeId;
			}
			this.position = position;
			this.hourlySalary = hourlySalary;
			this.startDate = startDate;
			this.endDate = endDate;
			this.fte = fte;
			this.active = active;
		}

		//Constructor with enddate, departuretype and departurereason
		public Contract(bool useContractId, bool useEmployeeId, int id, int employeeId, Position position, double hourlySalary, DateTime startDate, DateTime endDate, double fte, bool active, string departureReason, DepartureType departureType)
		{
			if (useContractId)
			{
				this.id = id;
			}
			if (useEmployeeId)
			{
				this.employeeId = employeeId;
			}
			this.position = position;
			this.hourlySalary = hourlySalary;
			this.startDate = startDate;
			this.endDate = endDate;
			this.fte = fte;
			this.active = active;
			this.departureReason = departureReason;
			this.departureType = departureType;
		}

		public override string ToString()
		{
			return base.ToString();
		}

		public void EditContract(double hourlySalary, Position position, double fte)
		{
			this.hourlySalary = hourlySalary;
			this.position = position;
			this.fte = fte;
		}
		public void RenewContract()
		{
			this.startDate = DateTime.Now;
		}
	}
}