<?php
class Shift extends DataHelper{

    public  function  GetShifts(){
        $pdo = $this->createConnection();

        $employeeid = $_SESSION['userid'];
        $sql = 'SELECT * FROM shift WHERE employeeid=:employeeid';
        $sth = $pdo->prepare($sql);
        $sth->execute([':employeeid' => $employeeid]);
        $result = $sth->fetchAll();
        $shifts = array();


        foreach ($result as $shift ){

            //if ($result.count()-1)
            if($shift[3] == 'morning'){
                $shift[3] = '8.00-12.30';
            }
            elseif ($shift[3] == 'afternoon'){
                $shift[3] = '12.30-17.00';
            }
            elseif ($shift[3] == 'evening'){
                $shift[3] = '17.00-21.30';
            }
            $shift = <<<EOT
        {
        title :'$shift[3]'+ '    ' +'$shift[4]',
        start : '$shift[2]'
        },
        
EOT;
            array_push($shifts, $shift);
        }

return $shifts;
    }
}
