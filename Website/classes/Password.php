<?php
class Password extends DataHelper {

    public  function  UpdatePassword(){
        try{
            $pdo = $this->createConnection();
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                // The request is using the POST method

                if (!empty($_POST['password'])) {
                    $password = $_POST['password'];
                }if(!empty($_POST['repeatpassword'])){
                    $repeatpassword = $_POST['repeatpassword'];
                }
                $password = $_POST['password'];
                $repeatpassword = $_POST['repeatpassword'];

                if($_POST['password'] && $_POST['repeatpassword']) {
                    if ($_POST['password'] == $_POST['repeatpassword']) {
                        $employeeid = $_SESSION['userid'];
                        $sql = 'UPDATE employee SET password=:password WHERE id=:employeeid';
                        $sth = $pdo->prepare($sql);
                        $sth->execute([':employeeid' => $employeeid, 'password' => $password]);
                        $sth->fetchAll();
                    }
                }

                $pdo = null;
            }
        }
        catch (PDOException $ex){
            echo $ex->getMessage();
        }

    }

}