<?php
class Availability extends DataHelper {

    public  function InsetAvailability(){
        $pdo =  $this->createConnection();

        $employeeid = $_SESSION['userid'];

        $sql = 'SELECT * FROM availability WHERE employeeid=:employeeid';
        $sth = $pdo->prepare($sql);
        $sth->execute([':employeeid' => $employeeid]);
        $result = $sth->fetchAll();
        foreach ($result as $availability){
            $_POST['monMorning']   = $availability[2];
            $_POST['monAfternoon'] = $availability[3];
            $_POST['monEvening']   = $availability[4];

            $_POST['tueMorning']   = $availability[5];
            $_POST['tueAfternoon'] = $availability[6];
            $_POST['tueEvening']   = $availability[7];

            $_POST['wedMorning']   = $availability[8];
            $_POST['wedAfternoon'] = $availability[9];
            $_POST['wedEvening']   = $availability[10];

            $_POST['thuMorning']   = $availability[11];
            $_POST['thuAfternoon'] = $availability[12];
            $_POST['thuEvening']   = $availability[13];

            $_POST['friMorning']   = $availability[14];
            $_POST['friAfternoon'] = $availability[15];
            $_POST['friEvening']   = $availability[16];

            $_POST['satMorning']   = $availability[17];
            $_POST['satAfternoon'] = $availability[18];
            $_POST['satEvening']   = $availability[19];

            $_POST['sunMorning']   = $availability[20];
            $_POST['sunAfternoon'] = $availability[21];
            $_POST['sunEvening']   = $availability[22];
        }


        if ( isset($_REQUEST['submit']) ) {
            $mondayMorning = 0;
            $mondayAfternoon = 0;
            $mondayEvening = 0;
            $tuesdayMorning = 0;
            $tuesdayAfternoon = 0;
            $tuesdayEvening = 0;
            $wednesdayMorning = 0;
            $wednesdayAfternoon = 0;
            $wednesdayEvening = 0;
            $thursdayMorning = 0;
            $thursdayAfternoon = 0;
            $thursdayEvening = 0;
            $fridayMorning = 0;
            $fridayAfternoon = 0;
            $fridayEvening = 0;
            $saturdayMorning = 0;
            $saturdayAfternoon = 0;
            $saturdayEvening = 0;
            $sundayMorning = 0;
            $sundayAfternoon = 0;
            $sundayEvening = 0;
            if(!empty($_POST['mondayMorning'])){
                $mondayMorning = 1;
            }
            if(!empty($_POST['mondayAfternoon'])){
                $mondayAfternoon = 1;
            }
            if(!empty($_POST['mondayEvening'])){
                $mondayEvening = 1;
            }
            if(!empty($_POST['tuesdayMorning'])){
                $tuesdayMorning = 1;
            }
            if(!empty($_POST['tuesdayAfternoon'])){
                $tuesdayAfternoon = 1;
            }
            if(!empty($_POST['tuesdayEvening'])){
                $tuesdayEvening = 1;
            }
            if(!empty($_POST['wednesdayMorning'])){
                $wednesdayMorning = 1;
            }
            if(!empty($_POST['wednesdayAfternoon'])){
                $wednesdayAfternoon = 1;
            }
            if(!empty($_POST['wednesdayEvening'])){
                $wednesdayEvening = 1;
            }
            if(!empty($_POST['thursdayMorning'])){
                $thursdayMorning = 1;
            }
            if(!empty($_POST['thursdayAfternoon'])){
                $thursdayAfternoon = 1;
            }
            if(!empty($_POST['thursdayEvening'])){
                $thursdayEvening = 1;
            }
            if(!empty($_POST['fridayMorning'])){
                $fridayMorning = 1;
            }
            if(!empty($_POST['fridayAfternoon'])){
                $fridayAfternoon = 1;
            }
            if(!empty($_POST['fridayEvening'])){
                $fridayEvening = 1;
            }
            if(!empty($_POST['saturdayMorning'])){
                $saturdayMorning = 1;
            }
            if(!empty($_POST['saturdayAfternoon'])){
                $saturdayAfternoon = 1;
            }
            if(!empty($_POST['saturdayEvening'])){
                $saturdayEvening = 1;
            }
            if(!empty($_POST['sundayMorning'])){
                $sundayMorning = 1;
            }
            if(!empty($_POST['sundayAfternoon'])){
                $sundayAfternoon = 1;
            }
            if(!empty($_POST['sundayEvening'])){
                $sundayEvening = 1;
            }

            if($mondayMorning != 0 || $mondayAfternoon!=0 ||$mondayEvening !=0||$tuesdayMorning!=0||$tuesdayAfternoon!=0||
                $tuesdayEvening!=0||$wednesdayMorning!=0||$wednesdayAfternoon!=0||$wednesdayEvening !=0||$thursdayMorning!=0||
                $thursdayAfternoon!=0||$thursdayEvening!=0||$fridayMorning!=0||$fridayAfternoon!=0||$fridayEvening!=0||
                $saturdayMorning!=0||$saturdayAfternoon!=0||$saturdayEvening!=0||$sundayMorning!=0||$sundayAfternoon!=0||$sundayEvening!=0) {


                $sql   = "INSERT INTO availability (employeeid,mondaymorning,mondayafternoon,mondayevening,tuesdaymorning,tuesdayafternoon,tuesdayevening,
                      wednesdaymorning,wednesdayafternoon,wednesdayevening,thursdaymorning,thursdayafternoon,thursdayevening,
                      fridaymorning,fridayafternoon,fridayevening,saturdaymorning,saturdayafternoon,saturdayevening,
                      sundaymorning,sundayafternoon,sundayevening) VALUES ('$employeeid','$mondayMorning','$mondayAfternoon','$mondayEvening','$tuesdayMorning','$tuesdayAfternoon','$tuesdayEvening',
                      '$wednesdayMorning','$wednesdayAfternoon','$wednesdayEvening','$thursdayMorning','$thursdayAfternoon','$thursdayEvening','$fridayMorning',
					  '$fridayAfternoon','$fridayEvening','$saturdayMorning','$saturdayAfternoon','$saturdayEvening','$sundayMorning','$sundayAfternoon','$sundayEvening')
					  ON DUPLICATE KEY UPDATE 
					  mondaymorning = $mondayMorning,
					  mondayafternoon = $mondayAfternoon,
					  mondayevening = $mondayEvening,
					  tuesdaymorning = $tuesdayMorning,
					  tuesdayafternoon = $tuesdayAfternoon,
					  tuesdayevening = $tuesdayEvening,
                      wednesdaymorning = $wednesdayMorning,
					  wednesdayafternoon = $wednesdayAfternoon,
					  wednesdayevening = $wednesdayEvening,
					  thursdaymorning = $thursdayMorning,
					  thursdayafternoon = $thursdayAfternoon,
					  thursdayevening = $thursdayEvening,
                      fridaymorning = $fridayMorning,
					  fridayafternoon = $fridayAfternoon,
					  fridayevening = $fridayEvening,
					  saturdaymorning = $saturdayMorning,
					  saturdayafternoon = $saturdayAfternoon,
					  saturdayevening = $saturdayEvening,
                      sundaymorning = $sundayMorning,
					  sundayafternoon = $sundayAfternoon,
					  sundayevening = $sundayEvening
					  ";
                $nrows = $pdo->exec( $sql );
                if($nrows ==1 || $nrows ==2){
                    $_POST['monMorning']   = $mondayMorning;
                    $_POST['monAfternoon'] = $mondayAfternoon;
                    $_POST['monEvening']   = $mondayEvening;

                    $_POST['tueMorning']   = $tuesdayMorning;
                    $_POST['tueAfternoon'] = $tuesdayAfternoon;
                    $_POST['tueEvening']   = $tuesdayEvening;

                    $_POST['wedMorning']   = $wednesdayMorning;
                    $_POST['wedAfternoon'] = $wednesdayAfternoon;
                    $_POST['wedEvening']   = $wednesdayEvening;

                    $_POST['thuMorning']   = $thursdayMorning;
                    $_POST['thuAfternoon'] = $thursdayAfternoon;
                    $_POST['thuEvening']   = $thursdayEvening;

                    $_POST['friMorning']   = $fridayMorning;
                    $_POST['friAfternoon'] = $fridayAfternoon;
                    $_POST['friEvening']   = $fridayEvening;

                    $_POST['satMorning']   = $saturdayMorning;
                    $_POST['satAfternoon'] = $saturdayAfternoon;
                    $_POST['satEvening']   = $saturdayEvening;

                    $_POST['sunMorning']   = $sundayMorning;
                    $_POST['sunAfternoon'] = $sundayAfternoon;
                    $_POST['sunEvening']   = $sundayEvening;
                }
            }
        }
    }
}