<?php
class Loaddata extends DataHelper{
    public  function  LoadEmployeeDate(){
        $pdo = $this->createConnection();

        $username = $_SESSION['username'];
        $sql = 'SELECT * FROM employee WHERE username=:username';
        $sth = $pdo->prepare($sql);
        $sth->execute([':username' => $username]);
        $result = $sth->fetchAll();
        foreach ( $result as $employee ) {
//Sets the session username into the name of the current user
            $_POST['firstname']            = $employee[5];
            $_POST['lastname']             = $employee[6];
            $_POST['gender']               = $employee[7];
            $_POST['bsn']                  = $employee[8];
            $_POST['emailAddress']         = $employee[10];
            $_POST['nationality']          = $employee[11];
            $_POST['spokenlanguages']      = $employee[12];
            $_POST['region']               = $employee[13];
            $_POST['zipcode']              = $employee[14];
            $_POST['streetname']           = $employee[15];
            $_POST['phonenumber']          = $employee[16];
            $_POST['emergencyphonenumber'] = $employee[17];
            $_POST['spouse']               = $employee[18];
        }
    }
}