<!DOCTYPE html>
<html lang="en">
<head>
<?php include "classes/Login.php";
$login = new Login();
$login->Run();
?>
</head>

<body>

        <form id="form" action="" method="POST">
            <fieldset>
                <legend><h1>Log in</h1></legend>
                <div class="logininfo">
                    <p>Please fill in this form to log in.</p>
                    <div id ="error"></div>
                    <span style="color: darkred;"><?php if(isset($err)){echo $err;} ?></span><br>
                    <div class="grid-login">
                        <label class="item1" for="username">Username: </label>
                        <input class="item2" placeholder="Enter username" name="username" id="username" autofocus> 
                        <br>
                        <label class="item3" for="password">Password: </label>
                        <input class="item4" type="password" placeholder="Enter Password" class="form-control" name="password" id="password">
                    </div>
                    <br><br>
                    <button type="submit" class="button">Submit</button>
                </div>
            </fieldset>
        </form>
</body>
<script defer src="Scripts/loginscript.js"></script>
</html> 