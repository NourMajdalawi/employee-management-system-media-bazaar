<!DOCTYPE html>
<html>
<head>

    <meta charset='utf-8' />
    <link href='Calendar/mainCore.css' rel='stylesheet' />
    <link href='Calendar/mainDaygrid.css' rel='stylesheet' />
    <link href='Calendar/mainList.css' rel='stylesheet' />
    <link href="Calendar/CalendarStyle.css" rel="stylesheet"/>
    <script src='Calendar/mainCore.js'></script>
    <script src='Calendar/mainInteraction.js'></script>
    <script src='Calendar/mainDaygrid.js'></script>
    <script src='Calendar/mainList.js'></script>
    <script src='Calendar/mainGoogleCalendar.js'></script>

    <?php
    include "Calendar/calendarScript.php";
    ?>

</head>
<body>

<div id='loading'>loading...</div>

<div id='calendar'></div>

</body>
</html>