function validatepassword(){
    if($('#psw').val() !== $('#pswcon').val()){
        document.changePasswordForm.password.style.border="3px dashed red";
        document.changePasswordForm.repeatpassword.style.border="3px dashed red";

    } else {
        document.changePasswordForm.password.style.border="3px dashed green";
        document.changePasswordForm.repeatpassword.style.border="3px dashed green";

    }
}
$("#changePasswordForm").submit(function() {
    if($('#psw').val()!== '' && $('#pswcon').val()!== ''){
        if ($('#psw').val() === $('#pswcon').val()) {
            $.ajax({
                url: 'classes/Password.php',
                method: 'POST',
                success: function () {
                    alert("password has been changed successfully!");
                }
            });

        }
        else {
            alert("The password you entered does not match!");
        }
    }
    else {
        alert("Make sure all fields are filled before submitting!");
    }


});