const name = document.getElementById('username')
const password = document.getElementById('password')
const form = document.getElementById('form')
const errorElement = document.getElementById('error')

form.addEventListener('submit', (e) => {

    let messages = []
    if (name.value === '' || name.value == null){
        messages.push('Name is required')
    }

    if(password.value === '' || password.value == null){
        messages.push('Password is required')
    }

    if(messages.length > 0){
        errorElement.innerText = messages.join(', ')
        e.preventDefault()
    }
})

